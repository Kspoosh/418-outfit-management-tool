﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Net.Sockets;
using System.Xml;
using System.Xml.Linq;



namespace WindowsFormsApplication5
{
    
    public partial class Form1 : Form
    {

        //some globals
        DataTable table = new DataTable();
        DataTable Killboard = new DataTable();
        public delegate void InvokeDelegate();
        bool sizeChanged = false;
        bool showWorldData = false;

        // initalizes Form and sets the Traceroute ComboboxIndex to 1
        public Form1()
        {
            var splashScreen = new Form4();
            splashScreen.Show();

            Cursor.Current = Cursors.WaitCursor;
            CheckForInternetConnection();
            if (CheckForInternetConnection() == false)
            {
                //t.Abort();
                splashScreen.Close();
                string message = "Please connect your device to the internet and restart the application!";
                string caption = "No Internet connection detected";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, caption, buttons);
                this.Close();
            }
            if (CheckIfServersAreOnline() == false)
            {
                splashScreen.Close();
                string message = "Please try again later and in the mean time check the offical forums for news regarding the outage";
                string caption = "The Planetside Servers seems to be in trouble!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, caption, buttons);
                this.Close();
            }

            InitializeComponent();
            Thread.Sleep(30);
            toolsToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.T;
            Thread.Sleep(100);
            setupDataTable();
            setupKillBoardTable();
            loadDataIntoDataGridView();

            dataGridView1.AutoGenerateColumns = false;
            //t.Abort();
            splashScreen.Close();
           
            Cursor.Current = Cursors.Default;
        }

        //Creating the background workers on Form load
        private void Form1_Load(object sender, EventArgs e)
        {}

        public void changeLabel72Text(int action)
        {
            if(action == 1)
            {
                label72.Text = "Loading Character";
            }
            if(action == 2)
            {
                label72.Text = "Loading Killboard";
            }
            
        }
        
        //calls form4
        public void splashStart()
        {
            Application.Run(new Form4());
        }

        // Starts form splashscreen
        public static void ThreadProc()
        {
            Application.Run(new Form4());
        }

        // Starts tools
        public static void ToolsForm()
        {
            Application.Run(new ping_form());
        }
        public static void splash()
        {
            Application.Run(new Form6());
        }

        // Calls form4 (splashscreen)
        private void button2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(ThreadProc));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
        
        // Test closing event
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        // About ... Calls Form3 and centers it with it's parent i.e. Form1
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.Parent = Parent;
            form3.StartPosition = FormStartPosition.CenterParent;
            form3.ShowDialog();
        }

        // pull territory control from the API and calculate it to percentages
        private void getTerritoryControl()
        {
            if (showWorldData == false)
            {
                Cursor.Current = Cursors.WaitCursor;
                var apiURL = "https://census.daybreakgames.com/xml/get/ps2:v2/map/?world_id=13&zone_ids=2,4,6,8";
                try
                {
                    XElement doc = XElement.Load(apiURL);

                    //Indar Hexownership
                    IEnumerable<XElement> indarTR =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "3" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 3000
                    select item;

                    IEnumerable<XElement> indarNC =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "2" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 3000
                    select item;

                    IEnumerable<XElement> indarVS =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "1" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 3000
                    select item;


                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //Amerish Hexownership
                    IEnumerable<XElement> amerishTR =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "3" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 6400 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 6000
                    select item;

                    IEnumerable<XElement> amerishNC =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "2" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 6400 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 6000
                    select item;

                    IEnumerable<XElement> amerishVS =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "1" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 6400 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 6000
                    select item;



                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //Esamir Hexownership
                    IEnumerable<XElement> esamirTR =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "3" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 18100 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 18000
                    select item;

                    IEnumerable<XElement> esamirNC =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "2" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 18100 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 18000
                    select item;

                    IEnumerable<XElement> esamirVS =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "1" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 18100 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 18000
                    select item;



                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //Hossin Hexownership
                    IEnumerable<XElement> hossinTR =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "3" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 5000 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 4000
                    select item;

                    IEnumerable<XElement> hossinNC =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "2" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 5000 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 4000
                    select item;

                    IEnumerable<XElement> hossinVS =
                    from item in doc.Descendants("Row")
                    where item.Element("RowData").Attribute("FactionId").Value == "1" && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) < 5000 && Convert.ToInt32(item.Element("RowData").Attribute("RegionId").Value) > 4000
                    select item;

                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //put stuff in variables
                    double indarTRhexes = indarTR.Count();
                    double indarNChexes = indarNC.Count();
                    double indarVShexes = indarVS.Count();


                    //get total hexes on indar             
                    double indarhexes = Convert.ToInt32(indarTR.Count()) + Convert.ToInt32(indarNC.Count()) + Convert.ToInt32(indarVS.Count());
                    //calculate precentages
                    double indartrpercent = (indarTRhexes * 100) / indarhexes;
                    double indarncpercent = (indarNChexes * 100) / indarhexes;
                    double indarvspercent = (indarVShexes * 100) / indarhexes;


                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //put stuff in variables
                    double amerishTRhexes = amerishTR.Count();
                    double amerishNChexes = amerishNC.Count();
                    double amerishVShexes = amerishVS.Count();

                    //get total hexes on Amerish                   
                    double amerishhexes = Convert.ToInt32(amerishTR.Count()) + Convert.ToInt32(amerishNC.Count()) + Convert.ToInt32(amerishVS.Count());
                    //calculate precentages
                    double amerishtrpercent = (amerishTRhexes * 100) / amerishhexes;
                    double amerishncpercent = (amerishNChexes * 100) / amerishhexes;
                    double amerishvspercent = (amerishVShexes * 100) / amerishhexes;


                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //put stuff in variables
                    double esamirTRhexes = esamirTR.Count();
                    double esamirNChexes = esamirNC.Count();
                    double esamirVShexes = esamirVS.Count();

                    //get total hexes on Esamir      
                    double esamirhexes = Convert.ToInt32(esamirTR.Count()) + Convert.ToInt32(esamirNC.Count()) + Convert.ToInt32(esamirVS.Count());
                    //calculate precentages
                    double esamirtrpercent = (esamirTRhexes * 100) / esamirhexes;
                    double esamirncpercent = (esamirNChexes * 100) / esamirhexes;
                    double esamirvspercent = (esamirVShexes * 100) / esamirhexes;


                    //----------------------------------------------------------------------------------------------------------------------------------------------
                    //put stuff in variables
                    double hossinTRhexes = hossinTR.Count();
                    double hossinNChexes = hossinNC.Count();
                    double hossinVShexes = hossinVS.Count();
                    //get total hexes on Esamir      
                    double hossinhexes = Convert.ToInt32(hossinTR.Count()) + Convert.ToInt32(hossinNC.Count()) + Convert.ToInt32(hossinVS.Count());
                    //calculate precentages
                    double hossintrpercent = (hossinTRhexes * 100) / hossinhexes;
                    double hossinncpercent = (hossinNChexes * 100) / hossinhexes;
                    double hossinvspercent = (hossinVShexes * 100) / hossinhexes;


                    //put it into labels and add the Locked string

                    //Math.Round(3.44, 1); //Returns 3.4. 
                    label1.Text = Math.Round(indartrpercent,1).ToString() + " %";
                    label2.Text = Math.Round(indarncpercent,1).ToString() + " %";
                    label3.Text = Math.Round(indarvspercent,1).ToString() + " %";

                    if (indartrpercent == 100)
                    {
                        label206.Parent = groupBox2;
                        label206.BackColor = Color.Transparent;
                        label206.Visible = true;
                        label206.ForeColor = Color.Red;
                    }
                    else if (indarncpercent == 100)
                    {
                        label206.Parent = groupBox2;
                        label206.BackColor = Color.Transparent;
                        label206.Visible = true;
                        label206.ForeColor = System.Drawing.ColorTranslator.FromHtml("#008cba");

                    }
                    else if (indarvspercent == 100)
                    {
                        label206.Parent = groupBox2;
                        label206.BackColor = Color.Transparent;
                        label206.Visible = true;
                        label206.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC33FF");
                    }
                    else
                    {
                        label206.Visible = false;
                    }


                    label190.Text = Math.Round(amerishtrpercent,1).ToString() + " %";
                    label189.Text = Math.Round(amerishncpercent,1).ToString() + " %";
                    label188.Text = Math.Round(amerishvspercent,1).ToString() + " %";

                    if (amerishtrpercent == 100)
                    {
                        label207.Parent = groupBox2;
                        label207.BackColor = Color.Transparent;
                        label207.Visible = true;
                        label207.ForeColor = Color.Red;
                    }
                    else if (amerishncpercent == 100)
                    {
                        label207.Parent = groupBox2;
                        label207.BackColor = Color.Transparent;
                        label207.Visible = true;
                        label207.ForeColor = System.Drawing.ColorTranslator.FromHtml("#008cba");

                    }
                    else if (amerishvspercent == 100)
                    {
                        label207.Parent = groupBox2;
                        label207.BackColor = Color.Transparent;
                        label207.Visible = true;
                        label207.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC33FF");
                    }
                    else
                    {
                        label207.Visible = false;
                    }

                    label197.Text = Math.Round(esamirtrpercent,1).ToString() + " %";
                    label196.Text = Math.Round(esamirncpercent,1).ToString() + " %";
                    label195.Text = Math.Round(esamirvspercent,1).ToString() + " %";

                    if (esamirtrpercent == 100)
                    {
                        label208.Parent = groupBox2;
                        label208.BackColor = Color.Transparent;
                        label208.Visible = true;
                        label208.ForeColor = Color.Red;
                    }
                    else if (esamirncpercent == 100)
                    {
                        label208.Parent = groupBox2;
                        label208.BackColor = Color.Transparent;
                        label208.Visible = true;
                        label208.ForeColor = System.Drawing.ColorTranslator.FromHtml("#008cba");
                    }
                    else if (esamirvspercent == 100)
                    {
                        label208.Parent = groupBox2;
                        label208.BackColor = Color.Transparent;
                        label208.Visible = true;
                        label208.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC33FF");
                    }
                    else
                    {
                        label208.Visible = false;
                    }


                    label204.Text = Math.Round(hossintrpercent,1).ToString() + " %";
                    label203.Text = Math.Round(hossinncpercent,1).ToString() + " %";
                    label202.Text = Math.Round(hossinvspercent,1).ToString() + " %";

                    if (hossintrpercent == 100)
                    {
                        label209.Parent = groupBox2;
                        label209.BackColor = Color.Transparent;
                        label209.Visible = true;
                        label209.ForeColor = Color.Red;
                    }
                    else if (hossinncpercent == 100)
                    {
                        label209.Parent = groupBox2;
                        label209.BackColor = Color.Transparent;
                        label209.Visible = true;
                        label209.ForeColor = System.Drawing.ColorTranslator.FromHtml("#008cba");
                    }
                    else if (hossinvspercent == 100)
                    {
                        label209.Parent = groupBox2;
                        label209.BackColor = Color.Transparent;
                        label209.Visible = true;
                        label209.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC33FF");
                    }
                    else
                    {
                        label209.Visible = false;
                    }

                }
                catch (ArgumentNullException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                catch (NullReferenceException ex)
                {
                    MessageBox.Show("error, " + ex);
                }
                showWorldData = true;
                dataGridView1.Size = new Size(496, 334);
                Cursor.Current = Cursors.Default;
            }
            else
            {
                dataGridView1.Size = new Size(496, 623);
                showWorldData = false;
            }
        }

        // setup datatable table and bind it to datagridview1
        public void setupDataTable()
        {
            DataGridViewButtonColumn btnColumn = new DataGridViewButtonColumn();
            btnColumn.HeaderText = "Link";
            btnColumn.Text = "Link";
            btnColumn.UseColumnTextForButtonValue = true;

            table.Columns.Add("IGN", typeof(String));
            table.Columns.Add("BR", typeof(int));
            table.Columns.Add("Last played", typeof(string));            
            table.Columns.Add("player_id", typeof(string));
            table.Columns.Add("days", typeof(double));
            table.Columns.Add("online_status", typeof(string));
            table.Columns.Add("Rank", typeof(string));
            
            dataGridView1.DataSource = table;
            dataGridView1.Columns["player_id"].Visible = false;
            //dataGridView1.Columns["days"].Visible = false;
            dataGridView1.Columns["online_status"].Visible = false;
            //dataGridView1.Columns["Rank"].Visible = false;
            dataGridView1.Columns["days"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["days"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["BR"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["BR"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns["IGN"].FillWeight = 210;
            dataGridView1.Columns["Last played"].FillWeight = 150;
            dataGridView1.Columns["BR"].FillWeight = 50;
            dataGridView1.Columns["days"].FillWeight = 50;
            //dataGridView1.Columns.Add(btnColumn);
            table.DefaultView.RowFilter = "online_status = 13";
        }
        public void setupKillBoardTable()
        {
            //add columns
            Killboard.Columns.Add("type", typeof(string));
            Killboard.Columns.Add("Attacker", typeof(string));
            //Killboard.Columns.Add("Weapon", typeof(string));
            Killboard.Columns.Add("Victim", typeof(string));
            Killboard.Columns.Add("note", typeof(string));
            Killboard.Columns.Add("Timestamp", typeof(string));

            //column settings
            dataGridView2.DataSource = Killboard;

            dataGridView2.Columns["type"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["type"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridView2.Columns["Attacker"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["Attacker"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["Victim"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["Victim"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["note"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["Timestamp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["Timestamp"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns["Attacker"].FillWeight = 210;
            dataGridView2.Columns["Victim"].FillWeight = 210;
            dataGridView2.Columns["note"].FillWeight = 80;
            dataGridView2.Columns["Timestamp"].FillWeight = 210;

        }

        // Gets XML from census.daybreakgames.com, parses it and writes it into the Datagridview1 and Datagridview2
        public void loadDataIntoDataGridView()
        {
                table.Clear();
                label14.Text = "Members online";
                var apiURL = "http://census.daybreakgames.com/s:soe/xml/get/ps2:v2/outfit_member?c:limit=1000&c:resolve=online_status,character(name,battle_rank,profile_id,times)&c:join=type:profile^list:0^inject_at:profile^show:name.en^on:character.profile_id^to:profile_id&outfit_id=37510026483741634";
                XElement doc = XElement.Load(apiURL);

                IEnumerable<XElement> list1 =
                    from el in doc.DescendantsAndSelf("outfit_member")
                    //where (string)el.Attribute("online_status") == "11"
                    orderby (string)el.Element("character").Element("name").Attribute("first")
                    select el;
                try
                {
                    foreach (XElement el in list1)
                    {
                        //get Name
                        string nameResult = el.Element("character").Element("name").Attribute("first").Value.ToString();

                        //get BR
                        string brResult = el.Element("character").Element("battle_rank").Attribute("value").Value.ToString();
                        int brResultInt = Convert.ToInt32(brResult);

                        //get played class
                        string classResult = el.Element("character").Element("profile").Element("name").Attribute("en").Value.ToString();

                        //get character id
                        string playerIDResult = el.Attribute("character_id").Value.ToString();

                        //datagrid stuff
                        //var playersLink = "https://players.planetside2.com/#!/" + playerIDResult;

                        //get last login then display the color
                        var llDateResult = el.Element("character").Element("times").Attribute("last_login_date").Value.ToString();
                        DateTime llDate = Convert.ToDateTime(llDateResult);
                        double diff2 = DateTime.Now.Subtract(llDate).TotalDays;
                        int last = Convert.ToInt32(diff2);

                        //get online status
                        string online = el.Attribute("online_status").Value.ToString();
                        if(online == "service_unavailable")
                        {
                            online = "0";
                        }

                        //get outfit rank
                        string outfitRank = el.Attribute("rank").Value.ToString();



                        DataRow _player = table.NewRow();
                        _player["IGN"] = nameResult;
                        _player["BR"] = brResultInt;
                        _player["Last played"] = classResult;
                        _player["player_id"] = playerIDResult;
                        _player["days"] = last;
                        _player["online_status"] = online;
                        _player["rank"] = outfitRank;

                        table.Rows.Add(_player);
                        dataGridView1.DataSource = table;

                    }
                }
                catch (ArgumentNullException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
        }
        void killboard(int dataindex)
        {
            //toolStripProgressBar1.Value = 0;
            //progressBar2.Value = 0;
            
            label72.Text = "loading Killboard";
            changeLabel72Text(2);
            Thread.Sleep(100);
            Cursor.Current = Cursors.WaitCursor;
            var charID = dataGridView1.Rows[dataindex].Cells[3].Value.ToString();
            var apiURL = "http://census.daybreakgames.com/xml/get/ps2:v2/characters_event/?character_id=" + charID + "&c:limit=50&type=KILL,DEATH&c:hide=world_id,zone_id&c:resolve=attacker(faction_id,name.first)&c:resolve=character(faction_id,name.first)";
            string attackerResult = "";
            //string attackerFactionID = "";
            //string weaponUSED = string.Empty;
            string victimResult = "";
            //string victimFactionID = "";

            XElement doc2 = XElement.Load(apiURL);

            IEnumerable<XElement> list2 =
            from el in doc2.DescendantsAndSelf("characters_event")
            select el;
            try
            {
                foreach (XElement el in list2)
                {
                    progressBar2.Value++;
                    try 
                    { 
                        //get Attacker
                        //attackerResult = el.Attribute("attacker_character_id").Value.ToString();
                        attackerResult = el.Element("attacker").Element("name").Attribute("first").Value.ToString();
                    }
                    catch(NullReferenceException)
                    {
                        var attackerID = el.Attribute("attacker_character_id").Value.ToString();
                        attackerResult = killerLookup(attackerID);
                        
                        //attackerResult = "...";
                    }
                    
                    try
                    {
                        //get Victim
                        //victimResult = el.Attribute("character_id").Value.ToString();
                        victimResult = el.Element("character").Element("name").Attribute("first").Value.ToString();
                    }
                    catch (NullReferenceException)
                    {
                        var victimID = el.Attribute("character_id").Value.ToString();
                        victimResult = killerLookup(victimID);
                        //victimResult = "...";
                    }
                   
                    //get headshot
                    var headshot = el.Attribute("is_headshot").Value.ToString();

                    //get event type (kill or death)
                    string eventType = el.Attribute("table_type").Value.ToString();
                    
                    
                    //get Weapon (extremly slow)
                    ////get item_id
                    //string weaponID = el.Attribute("attacker_weapon_id").Value.ToString();

                    //get Timestamp
                    string timestamp = el.Attribute("timestamp").Value.ToString();
                    //convert to local time
                    string time = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(Convert.ToDouble(timestamp)).ToLocalTime().ToString();
                    
                    //get faction ids (1 =VS, 2 = NC, 3 = TR)
                    //try
                    //{
                    //    string victimfactionID = el.Element("character").Attribute("faction_id").Value.ToString();
                    //}
                    //catch(NullReferenceException)
                    //{
                    //
                    //}
                    //try
                    //{
                    //    string attackerfactionID = el.Element("attacker").Attribute("faction_id").Value.ToString();
                    //}
                    //catch(NullReferenceException)
                    //{
                    //
                    //}


                    DataRow _killboard = Killboard.NewRow();
                    if(eventType == "kills")
                    {
                        //if (victimfactionID == "2")
                        //{
                        //    _killboard["type"] = "Teamkill";
                        //}
                        //else
                        //{
                            _killboard["type"] = "Kill";
                        //}
                    }
                    else
                    {
                        //if (attackerfactionID == "2")
                        //{
                        //    _killboard["type"] = "Teamkill";
                        //}
                        //else
                        //{
                            _killboard["type"] = "Death";
                        //}
                    }

                    _killboard["Attacker"] = attackerResult;
                    //_killboard["Weapon"] = weaponUsed(weaponID);//weaponUsed(weaponID);
                    _killboard["Victim"] = victimResult;
                    if (headshot == "1")
                    {
                        _killboard["note"] = "headshot";
                    }
                    _killboard["Timestamp"] = time;
                                        
                    Killboard.Rows.Add(_killboard);
                    dataGridView2.DataSource = Killboard;
                    

                }
            }
            catch (ArgumentNullException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            Cursor.Current = Cursors.Default;
            label72.Visible = false;
        }

        //lookup indidvidual chars and weapons
        static string killerLookup(string attackerID)
        {
            string attackerResult = string.Empty;
            var attackerAPI = "http://census.daybreakgames.com/xml/get/ps2:v2/character/?character_id=" + attackerID + "&c:show=faction_id,name.first";
            XElement doc3 = XElement.Load(attackerAPI);
        
            IEnumerable<XElement> list3 =
            from el in doc3.DescendantsAndSelf("character")
            select el;
        
            foreach (XElement el in list3)
            {
                attackerResult = el.Element("name").Attribute("first").Value.ToString();  
            }
        
            return attackerResult;
        }
        static string victimLookup(string victimID)
        {
            string victimResult = string.Empty;
            var victimAPI = "http://census.daybreakgames.com/xml/get/ps2:v2/character/?character_id=" + victimID + "&c:show=faction_id,name.first";
            XElement doc3 = XElement.Load(victimAPI);
        
            IEnumerable<XElement> list3 =
            from el in doc3.DescendantsAndSelf("character")
            select el;
        
            foreach (XElement el in list3)
            {
                victimResult = el.Element("name").Attribute("first").Value.ToString();
            }
        
            return victimResult;
        }
        static string weaponUsed(string weaponID)
        {
            string weaponResult = string.Empty;
            var itemAPI = "http://census.daybreakgames.com/xml/get/ps2/item/?item_id=" + weaponID + "&c:show=name.en,description.en";
            XElement doc3 = XElement.Load(itemAPI);
        
            IEnumerable<XElement> list3 =
            from el in doc3.DescendantsAndSelf("item")
            select el;
        
            foreach (XElement el in list3)
            {
                weaponResult = el.Element("name").Attribute("en").Value.ToString();  
            }
        
            return weaponResult;
        }
        
        //quickstats
        void quickstats(int dataindex)
        {
            progressBar2.Value = 0;
            label72.Visible = true;
            label72.Text = "loading Character";
            changeLabel72Text(1);
            Thread.Sleep(100);
            Cursor.Current = Cursors.WaitCursor;
            var charID = dataGridView1.Rows[dataindex].Cells[3].Value.ToString();
            var apiURL = "http://census.daybreakgames.com/xml/get/ps2/character/?character_id=" + charID + "&c:resolve=item_full(image_path,max_stack_size,name.en,description.en)&c:resolve=outfit_member,currency&c:resolve=profile(name.en,image_path)&c:join=type:characters_stat_history^terms:stat_name=deaths^inject_at:deaths^show:all_time&c:join=type:characters_stat_history^terms:stat_name=kills^inject_at:kills^show:all_time&c:join=type:characters_stat_history^terms:stat_name=facility_capture^inject_at:facility_capture^show:all_time&c:join=type:characters_stat_history^terms:stat_name=facility_defend^inject_at:facility_defend^show:all_time&c:join=type:characters_stat_history^terms:stat_name=medals^inject_at:medals^show:all_time&c:join=type:characters_stat_history^terms:stat_name=ribbons^inject_at:ribbons^show:all_time&c:join=type:characters_stat_history^terms:stat_name=time^inject_at:timesday^show:day&c:join=type:characters_stat_history^terms:stat_name=score^inject_at:score^show:all_time";
            try { 
            XElement doc = XElement.Load(apiURL);
            IEnumerable<XElement> list1 =
                from el in doc.DescendantsAndSelf("character")
                select el;

             var medicalapplicatorCertline = from item in doc.Descendants("items").Elements("name")
                                 where item.Attribute("en").Value.Contains("Medical Applicator")
                                 select new
                                 {
                                     MedicalApplicator = item.Attribute("en").Value.Contains("Medical Applicator")
                                 };


             var repairtoolCertline = from item in doc.Descendants("items").Elements("name")
                                 where item.Attribute("en").Value.Contains("Nano-Armor Kit")
                                 select new
                                 {
                                     repairtool = item.Attribute("en").Value.Contains("Nano-Armor Kit")
                                 };

             var nanoRegen = from item in doc.Descendants("items").Elements("name")
                                where item.Attribute("en").Value.Contains("Nano-Regen Device")
                                select new
                                {
                                    nanoregen = item.Attribute("en").Value.Contains("Nano-Regen Device")
                                };
             var ammoPack = from item in doc.Descendants("items").Elements("name")
                                where item.Attribute("en").Value.Contains("Ammunition Package")
                                select new
                                {
                                    ammoPack = item.Attribute("en").Value.Contains("Ammunition Package")
                                };

             var jumpJets = from item in doc.Descendants("items").Elements("name")
                                where item.Attribute("en").Value.Contains("Jump Jets")
                                select new
                                {
                                    jumpJets = item.Attribute("en").Value.Contains("Jump Jets")
                                };

             var drifterJumpJets = from item in doc.Descendants("items").Elements("name")
                                where item.Attribute("en").Value.Contains("Drifter")
                                select new
                                {
                                    drifterJumpJets = item.Attribute("en").Value.Contains("Drifter")
                                };
            

             var reconDetect = from item in doc.Descendants("items").Elements("name")
                                where item.Attribute("en").Value.Contains("Recon")
                                select new
                                {
                                    reconDetect = item.Attribute("en").Value.Contains("Recon")
                                };

             var hunterCloak = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Hunter")
                               select new
                               {
                                   hunterCloak = item.Attribute("en").Value.Contains("Hunter")
                               };

             var nanoCloak = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Nano-Armor Cloaking")
                               select new
                               {
                                   nanoCloak = item.Attribute("en").Value.Contains("Nano-Armor Cloaking")
                               };

             var nanitemeshGenerator = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Nanite Mesh Generator")
                               select new
                               {
                                   nanitemeshGenerator = item.Attribute("en").Value.Contains("Nanite Mesh Generator")
                               };
 
             var adrenalineShield = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Adrenaline Shield")
                               select new
                               {
                                   adrenalineShield = item.Attribute("en").Value.Contains("Adrenaline Shield")
                               };

             var resistShield = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Resist Shield")
                               select new
                               {
                                   resistShield = item.Attribute("en").Value.Contains("Resist Shield")
                               };
 
             var aegisShield = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Aegis Shield")
                               select new
                               {
                                   aegisShield = item.Attribute("en").Value.Contains("Aegis Shield")
                               };

             var ammoStorage = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Ammo Storage Canister")
                               select new
                               {
                                   reconDetect = item.Attribute("en").Value.Contains("Ammo Storage Canister")
                               };
             var charge = from item in doc.Descendants("items").Elements("name")
                               where item.Attribute("en").Value.Contains("Charge")
                               select new
                               {
                                   charge = item.Attribute("en").Value.Contains("Charge")
                               };

            var medicalapplicator = medicalapplicatorCertline.Count().ToString();
            var repairTool = repairtoolCertline.Count().ToString();
            var nanoRegencount = nanoRegen.Count().ToString();
            var ammoPackcount = ammoPack.Count().ToString();
            var jumpjetCount = jumpJets.Count().ToString();
            var drifterCount = drifterJumpJets.Count().ToString();
            var reconDetectCount = reconDetect.Count().ToString();
            var hunterCount = hunterCloak.Count().ToString();
            var nanocloakCount = nanoCloak.Count().ToString();
            var nanitemeshCount = nanitemeshGenerator.Count().ToString();
            var adrenalineshieldCount = adrenalineShield.Count().ToString();
            var resistshieldCount = resistShield.Count().ToString();
            var aegisshieldCount = aegisShield.Count().ToString();
            var chargeCount = charge.Count().ToString();
            var ammocanisterCount = ammoStorage.Count().ToString();

            //process Ammo Storage
            if (ammocanisterCount == "5")
            {
                label167.Visible = true;
                pictureBox144.Visible = true;
                pictureBox145.Visible = true;
                pictureBox146.Visible = true;
                pictureBox147.Visible = true;
                pictureBox148.Visible = true;
            }
            if (ammocanisterCount == "4")
            {
                label174.Visible = false;
                pictureBox144.Visible = true;
                pictureBox145.Visible = true;
                pictureBox146.Visible = true;
                pictureBox147.Visible = true;
                pictureBox148.Visible = false;
            }
            if (ammocanisterCount == "3")
            {
                label174.Visible = false;
                pictureBox144.Visible = true;
                pictureBox145.Visible = true;
                pictureBox146.Visible = true;
                pictureBox147.Visible = false;
                pictureBox148.Visible = false;
            }
            if (ammocanisterCount == "2")
            {
                label174.Visible = false;
                pictureBox144.Visible = true;
                pictureBox145.Visible = true;
                pictureBox146.Visible = false;
                pictureBox147.Visible = false;
                pictureBox148.Visible = false;
            }
            if (ammocanisterCount == "1")
            {
                label174.Visible = false;
                pictureBox144.Visible = true;
                pictureBox145.Visible = false;
                pictureBox146.Visible = false;
                pictureBox147.Visible = false;
                pictureBox148.Visible = false;
            }

            //process Aegisshield
            if (aegisshieldCount == "5")
            {
                label174.Visible = true;
                pictureBox154.Visible = true;
                pictureBox155.Visible = true;
                pictureBox156.Visible = true;
                pictureBox157.Visible = true;
                pictureBox158.Visible = true;
            }
            if (aegisshieldCount == "4")
            {
                label174.Visible = false;
                pictureBox154.Visible = true;
                pictureBox155.Visible = true;
                pictureBox156.Visible = true;
                pictureBox157.Visible = true;
                pictureBox158.Visible = false;
            }
            if (aegisshieldCount == "3")
            {
                label174.Visible = false;
                pictureBox154.Visible = true;
                pictureBox155.Visible = true;
                pictureBox156.Visible = true;
                pictureBox157.Visible = false;
                pictureBox158.Visible = false;
            }
            if (aegisshieldCount == "2")
            {
                label174.Visible = false;
                pictureBox154.Visible = true;
                pictureBox155.Visible = true;
                pictureBox156.Visible = false;
                pictureBox157.Visible = false;
                pictureBox158.Visible = false;
            }
            if (aegisshieldCount == "1")
            {
                label174.Visible = false;
                pictureBox154.Visible = true;
                pictureBox155.Visible = false;
                pictureBox156.Visible = false;
                pictureBox157.Visible = false;
                pictureBox158.Visible = false;
            }

            //process Charge
            if (chargeCount == "6")
            {
                label182.Visible = true;
                pictureBox164.Visible = true;
                pictureBox165.Visible = true;
                pictureBox166.Visible = true;
                pictureBox167.Visible = true;
                pictureBox168.Visible = true;
                pictureBox169.Visible = true;
            }
            if (chargeCount == "5")
            {
                label182.Visible = false;
                pictureBox164.Visible = true;
                pictureBox165.Visible = true;
                pictureBox166.Visible = true;
                pictureBox167.Visible = true;
                pictureBox168.Visible = true;
                pictureBox169.Visible = false;
            }
            if (chargeCount == "4")
            {
                label182.Visible = false;
                pictureBox164.Visible = true;
                pictureBox165.Visible = true;
                pictureBox166.Visible = true;
                pictureBox167.Visible = true;
                pictureBox168.Visible = false;
                pictureBox169.Visible = false;
            }
            if (chargeCount == "3")
            {
                label182.Visible = false;
                pictureBox164.Visible = true;
                pictureBox165.Visible = true;
                pictureBox166.Visible = true;
                pictureBox167.Visible = false;
                pictureBox168.Visible = false;
                pictureBox169.Visible = false;
            }
            if (chargeCount == "2")
            {
                label182.Visible = false;
                pictureBox164.Visible = true;
                pictureBox165.Visible = true;
                pictureBox166.Visible = false;
                pictureBox167.Visible = false;
                pictureBox168.Visible = false;
                pictureBox169.Visible = false;
            }
            if (chargeCount == "1")
            {
                label182.Visible = false;
                pictureBox164.Visible = true;
                pictureBox165.Visible = false;
                pictureBox166.Visible = false;
                pictureBox167.Visible = false;
                pictureBox168.Visible = false;
                pictureBox169.Visible = false;
            }

            //process resistShield
            if (resistshieldCount == "5")
            {
                label160.Visible = true;
                pictureBox134.Visible = true;
                pictureBox135.Visible = true;
                pictureBox136.Visible = true;
                pictureBox137.Visible = true;
                pictureBox138.Visible = true;
            }
            if (resistshieldCount == "4")
            {
                label160.Visible = false;
                pictureBox134.Visible = true;
                pictureBox135.Visible = true;
                pictureBox136.Visible = true;
                pictureBox137.Visible = true;
                pictureBox138.Visible = false;
            }
            if (resistshieldCount == "3")
            {
                label160.Visible = false;
                pictureBox134.Visible = true;
                pictureBox135.Visible = true;
                pictureBox136.Visible = true;
                pictureBox137.Visible = false;
                pictureBox138.Visible = false;
            }
            if (resistshieldCount == "2")
            {
                label160.Visible = false;
                pictureBox134.Visible = true;
                pictureBox135.Visible = true;
                pictureBox136.Visible = false;
                pictureBox137.Visible = false;
                pictureBox138.Visible = false;
            }
            if (resistshieldCount == "1")
            {
                label160.Visible = false;
                pictureBox134.Visible = true;
                pictureBox135.Visible = false;
                pictureBox136.Visible = false;
                pictureBox137.Visible = false;
                pictureBox138.Visible = false;
            }

            //process adrenalineShield
            if (adrenalineshieldCount == "5")
            {
                label145.Visible = true;
                pictureBox112.Visible = true;
                pictureBox113.Visible = true;
                pictureBox114.Visible = true;
                pictureBox115.Visible = true;
                pictureBox116.Visible = true;
            }
            if (adrenalineshieldCount == "4")
            {
                label145.Visible = false;
                pictureBox112.Visible = true;
                pictureBox113.Visible = true;
                pictureBox114.Visible = true;
                pictureBox115.Visible = true;
                pictureBox116.Visible = false;
            }
            if (adrenalineshieldCount == "3")
            {
                label145.Visible = false;
                pictureBox112.Visible = true;
                pictureBox113.Visible = true;
                pictureBox114.Visible = true;
                pictureBox115.Visible = false;
                pictureBox116.Visible = false;
            }
            if (adrenalineshieldCount == "2")
            {
                label145.Visible = false;
                pictureBox112.Visible = true;
                pictureBox113.Visible = true;
                pictureBox114.Visible = false;
                pictureBox115.Visible = false;
                pictureBox116.Visible = false;
            }
            if (adrenalineshieldCount == "1")
            {
                label145.Visible = false;
                pictureBox112.Visible = true;
                pictureBox113.Visible = false;
                pictureBox114.Visible = false;
                pictureBox115.Visible = false;
                pictureBox116.Visible = false;
            }

            //process Nanite Mesh Generator
            if (nanitemeshCount == "6")
            {
                label153.Visible = true;
                pictureBox122.Visible = true;
                pictureBox123.Visible = true;
                pictureBox124.Visible = true;
                pictureBox125.Visible = true;
                pictureBox126.Visible = true;
                pictureBox127.Visible = true;
            }
            if (nanitemeshCount == "5")
            {
                label153.Visible = false;
                pictureBox122.Visible = true;
                pictureBox123.Visible = true;
                pictureBox124.Visible = true;
                pictureBox125.Visible = true;
                pictureBox126.Visible = true;
                pictureBox127.Visible = false;
            }
            if (nanitemeshCount == "4")
            {
                label153.Visible = false;
                pictureBox122.Visible = true;
                pictureBox123.Visible = true;
                pictureBox124.Visible = true;
                pictureBox125.Visible = true;
                pictureBox126.Visible = false;
                pictureBox127.Visible = false;
            }
            if (nanitemeshCount == "3")
            {
                label153.Visible = false;
                pictureBox122.Visible = true;
                pictureBox123.Visible = true;
                pictureBox124.Visible = true;
                pictureBox125.Visible = false;
                pictureBox126.Visible = false;
                pictureBox127.Visible = false;
            }
            if (nanitemeshCount == "2")
            {
                label153.Visible = false;
                pictureBox122.Visible = true;
                pictureBox123.Visible = true;
                pictureBox124.Visible = false;
                pictureBox125.Visible = false;
                pictureBox126.Visible = false;
                pictureBox127.Visible = false;
            }
            if (nanitemeshCount == "1")
            {
                label153.Visible = false;
                pictureBox122.Visible = true;
                pictureBox123.Visible = false;
                pictureBox124.Visible = false;
                pictureBox125.Visible = false;
                pictureBox126.Visible = false;
                pictureBox127.Visible = false;
            }

            //process nano cloak
            if (nanocloakCount == "5")
            {
                label139.Visible = true;
                pictureBox102.Visible = true;
                pictureBox103.Visible = true;
                pictureBox104.Visible = true;
                pictureBox105.Visible = true;
                pictureBox106.Visible = true;
            }
            if (nanocloakCount == "4")
            {
                label139.Visible = false;
                pictureBox102.Visible = true;
                pictureBox103.Visible = true;
                pictureBox104.Visible = true;
                pictureBox105.Visible = true;
                pictureBox106.Visible = false;
            }
            if (nanocloakCount == "3")
            {
                label139.Visible = false;
                pictureBox102.Visible = true;
                pictureBox103.Visible = true;
                pictureBox104.Visible = true;
                pictureBox105.Visible = false;
                pictureBox106.Visible = false;
            }
            if (nanocloakCount == "2")
            {
                label139.Visible = false;
                pictureBox102.Visible = true;
                pictureBox103.Visible = true;
                pictureBox104.Visible = false;
                pictureBox105.Visible = false;
                pictureBox106.Visible = false;
            }
            if (nanocloakCount == "1")
            {
                label139.Visible = false;
                pictureBox102.Visible = true;
                pictureBox103.Visible = false;
                pictureBox104.Visible = false;
                pictureBox105.Visible = false;
                pictureBox106.Visible = false;
            }

            //process hunter cloaking device
            if (hunterCount == "6")
            {
                label129.Visible = true;
                pictureBox61.Visible = true;
                pictureBox63.Visible = true;
                pictureBox80.Visible = true;
                pictureBox81.Visible = true;
                pictureBox82.Visible = true;
                pictureBox89.Visible = true;
            }
            if (hunterCount == "5")
            {
                label129.Visible = false;
                pictureBox61.Visible = true;
                pictureBox63.Visible = true;
                pictureBox80.Visible = true;
                pictureBox81.Visible = true;
                pictureBox82.Visible = true;
                pictureBox89.Visible = false;
            }
            if (hunterCount == "4")
            {
                label129.Visible = false;
                pictureBox61.Visible = true;
                pictureBox63.Visible = true;
                pictureBox80.Visible = true;
                pictureBox81.Visible = true;
                pictureBox82.Visible = false;
                pictureBox89.Visible = false;
            }
            if (hunterCount == "3")
            {
                label129.Visible = false;
                pictureBox61.Visible = true;
                pictureBox63.Visible = true;
                pictureBox80.Visible = true;
                pictureBox81.Visible = false;
                pictureBox82.Visible = false;
                pictureBox89.Visible = false;
            }
            if (hunterCount == "2")
            {
                label129.Visible = false;
                pictureBox61.Visible = true;
                pictureBox63.Visible = true;
                pictureBox80.Visible = false;
                pictureBox81.Visible = false;
                pictureBox82.Visible = false;
                pictureBox89.Visible = false;
            }
            if (hunterCount == "1")
            {
                label129.Visible = false;
                pictureBox61.Visible = true;
                pictureBox63.Visible = false;
                pictureBox80.Visible = false;
                pictureBox81.Visible = false;
                pictureBox82.Visible = false;
                pictureBox89.Visible = false;
            }

            //process reconDevice
            if (reconDetectCount == "6")
            {
                label130.Visible = true;
                pictureBox83.Visible = true;
                pictureBox84.Visible = true;
                pictureBox85.Visible = true;
                pictureBox86.Visible = true;
                pictureBox87.Visible = true;
                pictureBox88.Visible = true;
            }
            if (reconDetectCount == "5")
            {
                label130.Visible = false;
                pictureBox83.Visible = true;
                pictureBox84.Visible = true;
                pictureBox85.Visible = true;
                pictureBox86.Visible = true;
                pictureBox87.Visible = true;
                pictureBox88.Visible = false;
            }
            if (reconDetectCount == "4")
            {
                label130.Visible = false;
                pictureBox83.Visible = true;
                pictureBox84.Visible = true;
                pictureBox85.Visible = true;
                pictureBox86.Visible = true;
                pictureBox87.Visible = false;
                pictureBox88.Visible = false;
            }
            if (reconDetectCount == "3")
            {
                label130.Visible = false;
                pictureBox83.Visible = true;
                pictureBox84.Visible = true;
                pictureBox85.Visible = true;
                pictureBox86.Visible = false;
                pictureBox87.Visible = false;
                pictureBox88.Visible = false;
            }
            if (reconDetectCount == "2")
            {
                label130.Visible = false;
                pictureBox83.Visible = true;
                pictureBox84.Visible = true;
                pictureBox85.Visible = false;
                pictureBox86.Visible = false;
                pictureBox87.Visible = false;
                pictureBox88.Visible = false;
            }
            if (reconDetectCount == "1")
            {
                label130.Visible = false;
                pictureBox83.Visible = true;
                pictureBox84.Visible = false;
                pictureBox85.Visible = false;
                pictureBox86.Visible = false;
                pictureBox87.Visible = false;
                pictureBox88.Visible = false;
            }
            
            //process Jump Jets
            if (jumpjetCount == "6")
            {
                label80.Visible = true;
                pictureBox68.Visible = true;
                pictureBox69.Visible = true;
                pictureBox70.Visible = true;
                pictureBox71.Visible = true;
                pictureBox72.Visible = true;
                pictureBox73.Visible = true;
            }
            if (jumpjetCount == "5")
            {
                label80.Visible = false;
                pictureBox68.Visible = true;
                pictureBox69.Visible = true;
                pictureBox70.Visible = true;
                pictureBox71.Visible = true;
                pictureBox72.Visible = true;
                pictureBox73.Visible = false;
            }
            if (jumpjetCount == "4")
            {
                label80.Visible = false;
                pictureBox68.Visible = true;
                pictureBox69.Visible = true;
                pictureBox70.Visible = true;
                pictureBox71.Visible = true;
                pictureBox72.Visible = false;
                pictureBox73.Visible = false;
            }
            if (jumpjetCount == "3")
            {
                label80.Visible = false;
                pictureBox68.Visible = true;
                pictureBox69.Visible = true;
                pictureBox70.Visible = true;
                pictureBox71.Visible = false;
                pictureBox72.Visible = false;
                pictureBox73.Visible = false;
            }
            if (jumpjetCount == "2")
            {
                label80.Visible = false;
                pictureBox68.Visible = true;
                pictureBox69.Visible = true;
                pictureBox70.Visible = false;
                pictureBox71.Visible = false;
                pictureBox72.Visible = false;
                pictureBox73.Visible = false;
            }
            if (jumpjetCount == "1")
            {
                label80.Visible = false;
                pictureBox68.Visible = true;
                pictureBox69.Visible = false;
                pictureBox70.Visible = false;
                pictureBox71.Visible = false;
                pictureBox72.Visible = false;
                pictureBox73.Visible = false;
            }

            //process Drifter Jump Jets
            if (drifterCount == "5")
            {
                label79.Visible = true;
                pictureBox56.Visible = true;
                pictureBox57.Visible = true;
                pictureBox58.Visible = true;
                pictureBox59.Visible = true;
                pictureBox60.Visible = true;
            }
            if (drifterCount == "4")
            {
                label79.Visible = false;
                pictureBox56.Visible = true;
                pictureBox57.Visible = true;
                pictureBox58.Visible = true;
                pictureBox59.Visible = true;
                pictureBox60.Visible = false;
            }
            if (drifterCount == "3")
            {
                label79.Visible = false;
                pictureBox56.Visible = true;
                pictureBox57.Visible = true;
                pictureBox58.Visible = true;
                pictureBox59.Visible = false;
                pictureBox60.Visible = false;
            }
            if (drifterCount == "2")
            {
                label79.Visible = false;
                pictureBox56.Visible = true;
                pictureBox57.Visible = true;
                pictureBox58.Visible = false;
                pictureBox59.Visible = false;
                pictureBox60.Visible = false;
            }
            if (drifterCount == "1")
            {
                label79.Visible = false;
                pictureBox56.Visible = true;
                pictureBox57.Visible = false;
                pictureBox58.Visible = false;
                pictureBox59.Visible = false;
                pictureBox60.Visible = false;
            }

            //process Medical Applicator
            if(medicalapplicator == "6")
            {
                label78.Visible = true;
                pictureBox7.Visible = true;
                pictureBox14.Visible = true;
                pictureBox15.Visible = true;
                pictureBox16.Visible = true;
                pictureBox17.Visible = true;
                pictureBox18.Visible = true;
            }
            if (medicalapplicator == "5")
            {
                label78.Visible = false;
                pictureBox7.Visible = true;
                pictureBox14.Visible = true;
                pictureBox15.Visible = true;
                pictureBox16.Visible = true;
                pictureBox17.Visible = true;
                pictureBox18.Visible = false;
            }
            if (medicalapplicator == "4")
            {
                label78.Visible = false;
                pictureBox7.Visible = true;
                pictureBox14.Visible = true;
                pictureBox15.Visible = true;
                pictureBox16.Visible = true;
                pictureBox17.Visible = false;
                pictureBox18.Visible = false;
            }
            if (medicalapplicator == "3")
            {
                label78.Visible = false;
                pictureBox7.Visible = true;
                pictureBox14.Visible = true;
                pictureBox15.Visible = true;
                pictureBox16.Visible = false;
                pictureBox17.Visible = false;
                pictureBox18.Visible = false;
            }
            if (medicalapplicator == "2")
            {
                label78.Visible = false;
                pictureBox7.Visible = true;
                pictureBox14.Visible = true;
                pictureBox15.Visible = false;
                pictureBox16.Visible = false;
                pictureBox17.Visible = false;
                pictureBox18.Visible = false;
            }
            if (medicalapplicator == "1")
            {
                label78.Visible = false;
                pictureBox7.Visible = true;
                pictureBox14.Visible = false;
                pictureBox15.Visible = false;
                pictureBox16.Visible = false;
                pictureBox17.Visible = false;
                pictureBox18.Visible = false;
            }

            //process Nano-Regen Device
            if (nanoRegencount == "6")
            {
                label77.Visible = true;
                pictureBox32.Visible = true;
                pictureBox33.Visible = true;
                pictureBox34.Visible = true;
                pictureBox35.Visible = true;
                pictureBox36.Visible = true;
                pictureBox37.Visible = true;
            }
            if (nanoRegencount == "5")
            {
                label77.Visible = false;
                pictureBox32.Visible = true;
                pictureBox33.Visible = true;
                pictureBox34.Visible = true;
                pictureBox35.Visible = true;
                pictureBox36.Visible = true;
                pictureBox37.Visible = false;
            }
            if (nanoRegencount == "4")
            {
                label77.Visible = false;
                pictureBox32.Visible = true;
                pictureBox33.Visible = true;
                pictureBox34.Visible = true;
                pictureBox35.Visible = true;
                pictureBox36.Visible = false;
                pictureBox37.Visible = false;
            }
            if (nanoRegencount == "3")
            {
                label77.Visible = false;
                pictureBox32.Visible = true;
                pictureBox33.Visible = true;
                pictureBox34.Visible = true;
                pictureBox35.Visible = false;
                pictureBox36.Visible = false;
                pictureBox37.Visible = false;
            }
            if (nanoRegencount == "2")
            {
                label77.Visible = false;
                pictureBox32.Visible = true;
                pictureBox33.Visible = true;
                pictureBox34.Visible = false;
                pictureBox35.Visible = false;
                pictureBox36.Visible = false;
                pictureBox37.Visible = false;
            }
            if (nanoRegencount == "1")
            {
                label77.Visible = false;
                pictureBox32.Visible = true;
                pictureBox33.Visible = false;
                pictureBox34.Visible = false;
                pictureBox35.Visible = false;
                pictureBox36.Visible = false;
                pictureBox37.Visible = false;
            }

            //process repairtool
            if (repairTool == "6")
            {
                label76.Visible = true;
                pictureBox19.Visible = true;
                pictureBox20.Visible = true;
                pictureBox21.Visible = true;
                pictureBox22.Visible = true;
                pictureBox23.Visible = true;
                pictureBox24.Visible = true;
            }
            if (repairTool == "5")
            {
                label76.Visible = false;
                pictureBox19.Visible = true;
                pictureBox20.Visible = true;
                pictureBox21.Visible = true;
                pictureBox22.Visible = true;
                pictureBox23.Visible = true;
                pictureBox24.Visible = false;
            }
            if (repairTool == "4")
            {
                label76.Visible = false;
                pictureBox19.Visible = true;
                pictureBox20.Visible = true;
                pictureBox21.Visible = true;
                pictureBox22.Visible = true;
                pictureBox23.Visible = false;
                pictureBox24.Visible = false;
            }
            if (repairTool == "3")
            {
                label76.Visible = false;
                pictureBox19.Visible = true;
                pictureBox20.Visible = true;
                pictureBox21.Visible = true;
                pictureBox22.Visible = false;
                pictureBox23.Visible = false;
                pictureBox24.Visible = false;
            }
            if (repairTool == "2")
            {
                label76.Visible = false;
                pictureBox19.Visible = true;
                pictureBox20.Visible = true;
                pictureBox21.Visible = false;
                pictureBox22.Visible = false;
                pictureBox23.Visible = false;
                pictureBox24.Visible = false;
            }
            if (repairTool == "1")
            {
                label76.Visible = false;
                pictureBox19.Visible = true;
                pictureBox20.Visible = false;
                pictureBox21.Visible = false;
                pictureBox22.Visible = false;
                pictureBox23.Visible = false;
                pictureBox24.Visible = false;
            }
            
            //process Ammopack
            if (ammoPackcount == "6")
            {
                label75.Visible = true;
                pictureBox44.Visible = true;
                pictureBox45.Visible = true;
                pictureBox46.Visible = true;
                pictureBox47.Visible = true;
                pictureBox48.Visible = true;
                pictureBox49.Visible = true;
            }
            if (ammoPackcount == "5")
            {
                label75.Visible = false;
                pictureBox44.Visible = true;
                pictureBox45.Visible = true;
                pictureBox46.Visible = true;
                pictureBox47.Visible = true;
                pictureBox48.Visible = true;
                pictureBox49.Visible = false;
            }
            if (ammoPackcount == "4")
            {
                label75.Visible = false;
                pictureBox44.Visible = true;
                pictureBox45.Visible = true;
                pictureBox46.Visible = true;
                pictureBox47.Visible = true;
                pictureBox48.Visible = false;
                pictureBox49.Visible = false;
            }
            if (ammoPackcount == "3")
            {
                label75.Visible = false;
                pictureBox44.Visible = true;
                pictureBox45.Visible = true;
                pictureBox46.Visible = true;
                pictureBox47.Visible = false;
                pictureBox48.Visible = false;
                pictureBox49.Visible = false;
            }
            if (ammoPackcount == "2")
            {
                label75.Visible = false;
                pictureBox44.Visible = true;
                pictureBox45.Visible = true;
                pictureBox46.Visible = false;
                pictureBox47.Visible = false;
                pictureBox48.Visible = false;
                pictureBox49.Visible = false;
            }
            if (ammoPackcount == "1")
            {
                label75.Visible = false;
                pictureBox44.Visible = true;
                pictureBox45.Visible = false;
                pictureBox46.Visible = false;
                pictureBox47.Visible = false;
                pictureBox48.Visible = false;
                pictureBox49.Visible = false;
            }

            try
            {
                foreach (XElement el in list1)
                {
                    progressBar2.Value = 1;
                    //get Name
                    string nameResult = el.Element("name").Attribute("first").Value.ToString();
                    label24.Text = "[418] " + nameResult;
                    label210.Text = nameResult;
                    progressBar2.Value++;
            
                    //get BR
                    int brResult = Convert.ToInt32(el.Element("battle_rank").Attribute("value").Value);
                    label15.Text = brResult.ToString();
                    progressBar2.Value = 5;

                    // get and set BR image and title
                    var brAPI = "http://census.daybreakgames.com/xml/get/ps2/experience_rank/?rank="+ brResult +"&c:show=nc_image_path,nc.title.en";
                    XElement bRank = XElement.Load(brAPI);
                    IEnumerable<XElement> battleRank =
                        from br in bRank.DescendantsAndSelf("experience_rank")
                        select br;
                    foreach (XElement BR in battleRank)
                    {
                        label10.Text = BR.Element("nc").Element("title").Attribute("en").Value.ToString();
                        var brPicture = "http://census.daybreakgames.com" + BR.Attribute("nc_image_path").Value.ToString();
                        pictureBox31.Load(brPicture);
                        pictureBox31.SizeMode = PictureBoxSizeMode.StretchImage;
                    }
            
                    //get BR percent
                    string brPercentResult = el.Element("battle_rank").Attribute("percent_to_next").Value.ToString();
                    int brpercenintResult = Convert.ToInt32(brPercentResult);
                    progressBar3.Value = brpercenintResult;
                    label32.Text = brPercentResult + " %";
                    progressBar2.Value++;
            
                    //get Available Certs
                    string avCertsResult = el.Element("certs").Attribute("available_points").Value.ToString();
                    label36.Text = avCertsResult;
                    progressBar2.Value++;
                    
                                            
                    //get passive_certs
                    string passiveCerts = el.Element("certs").Attribute("gifted_points").Value.ToString();
                    label63.Text = passiveCerts;
                    progressBar2.Value++;


                    //get Certs earned
                    string eaCertsResult = el.Element("certs").Attribute("earned_points").Value.ToString();                    
                    label37.Text = eaCertsResult;
                    progressBar2.Value++;

                    // //get Certs spend
                    string spCertsResult = el.Element("certs").Attribute("spent_points").Value.ToString();
                    
                    label38.Text = spCertsResult;
                    
                    //get Nanites ressources
                    string nanitesResult = el.Element("currency").Attribute("quantity").Value.ToString();
                    label51.Text = nanitesResult;
                    progressBar2.Value = 25;

                    //get outfit_member_since_date
                    string omemResult = el.Element("outfit_member").Attribute("member_since_date").Value.ToString();
                    DateTime omemd = Convert.ToDateTime(omemResult);
                    label54.Text = omemd.ToString();
                    
                    //get creation_date
                    string charcrResult = el.Element("times").Attribute("creation_date").Value.ToString();
                    DateTime charc = Convert.ToDateTime(charcrResult);
                    label9.Text = charc.ToString();
                    
                    //get outfitrank
                    string orankResult = el.Element("outfit_member").Attribute("rank").Value.ToString();
                    label27.Text = orankResult;
                    
                    //get Kills
                    string killResult = el.Element("kills").Attribute("all_time").Value.ToString();
                    
                    label18.Text = killResult;
                    
                    //get player_id
                    string playerID = el.Attribute("character_id").Value.ToString();
                    label4.Text = playerID;
                    
                    //get class image
                    string classImage = el.Element("profile").Attribute("image_path").Value.ToString();
                    string classImageURL = "http://census.daybreakgames.com" + classImage;
                    pictureBox176.Load(classImageURL);
                    pictureBox176.SizeMode = PictureBoxSizeMode.StretchImage;

                    
                    //get Deaths
                    string deathResult = el.Element("deaths").Attribute("all_time").Value.ToString();
                    
                     label39.Text = deathResult;
                    //calc K/D Kills divided by Deaths
                    
                    decimal kdkillResult = Convert.ToDecimal(killResult);
                    decimal kddeathResult = Convert.ToDecimal(deathResult);
                    decimal dkdResult = kdkillResult / kddeathResult;
                    dkdResult = decimal.Round(dkdResult, 2);
                    string kdResult = Convert.ToString(dkdResult);
                    
                    label40.Text = kdResult;
                    
                    // 
                    //
                    // //get Facilitys defended
                    string facdefResult = el.Element("facility_defend").Attribute("all_time").Value.ToString();
                    label45.Text = facdefResult;
                    
                    //
                    // //get Facilitys captured
                    string faccapResult = el.Element("facility_capture").Attribute("all_time").Value.ToString();
                    label44.Text = faccapResult;
                    //
                    // //get ribbons
                    string ribbonsResult = el.Element("ribbons").Attribute("all_time").Value.ToString();
                    label49.Text = ribbonsResult;
                    //
                    // //get medals
                    string medalsResult = el.Element("medals").Attribute("all_time").Value.ToString();
                    label48.Text = medalsResult;
                    //
                    // //get login_count
                     string logincResult = el.Element("times").Attribute("login_count").Value.ToString();
                     label34.Text = logincResult;
                    
                    // //get last_login_date
                    string lldResult = el.Element("times").Attribute("last_login_date").Value.ToString();
                    
                    DateTime lld = Convert.ToDateTime(lldResult);    
                    double diff2 = DateTime.Now.Subtract(lld).TotalDays;
                    label33.Text = lld.ToString();
                    //
                    // //get minutes_played
                    string minutesResult = el.Element("times").Attribute("minutes_played").Value.ToString();
                    var mp = Convert.ToInt32(minutesResult);
                    var timePlayed = TimeSpan.FromMinutes(mp);
                    progressBar2.Value = 45;
                    
                    var daysPlayed = (int)timePlayed.TotalDays;
                    var hoursPlayed = (int)timePlayed.Hours;
                    var minutesPlayed = (int)timePlayed.Minutes;
                    label35.Text = daysPlayed + " days " + hoursPlayed + " hours " + minutesPlayed + " minutes ";




                    //get Score/Minute
                    int br100score = 18842977;
                    int score = Convert.ToInt32(el.Element("score").Attribute("all_time").Value);
                    label71.Text = score.ToString("N0");
                    int SPM = score / mp;

                    label13.Text = SPM.ToString();

                    decimal percentT100 = ((decimal)score / br100score) * 100;
                    label69.Text = percentT100.ToString("N0") + " %";
                    if(percentT100 <= 100)
                    { 
                        progressBar1.Value = Convert.ToInt32(percentT100);
                    }
                    else
                    {
                        progressBar1.Value = 100;
                    }

                    // calculate score to BR100 = 18842977 and time to BR100
                    int SCL = br100score - score;
                    int H2BR100 = SCL / SPM / 60;

                    if (brResult >= 100)
                    {
                        label64.Text = "Score since BR100";
                        label65.Text = "Time since BR100";

                        SCL = SCL * (-1);
                        H2BR100 = H2BR100 * (-1);
                        label66.Text = SCL.ToString("N0");
                        label67.Text = H2BR100.ToString() + " H";
                    }
                    else
                    {
                        label64.Text = "Score to BR100";
                        label65.Text = "Time to BR100";
                        
                        label66.Text = SCL.ToString("N0");
                        label67.Text = Convert.ToString(H2BR100) + " H";
                    }

                    progressBar2.Value = 50;


                    //get daily playtime


                    // determins the gender and display the right class into PictureBox1
                    var headID = Convert.ToInt32(el.Attribute("head_id").Value);
                    var charClass = el.Element("profile").Element("name").Attribute("en").Value.ToString();
                    if (headID >= 6)
                    {                     
                        switch (charClass)
                        {
                            default:// "Infiltrator" :
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-2-female.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;

                                   
                                }
                            case "Light Assault":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-4-female.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Combat Medic":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-5-female.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Engineer":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-6-female.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Heavy Assault":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-7-female.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "MAX":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-8-female.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                        }
                    }
                    else
                    {
                        switch (charClass)
                        {
                            default:// "Infiltrator" :
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-2-male.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Light Assault":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-4-male.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Combat Medic":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-5-male.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Engineer":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-6-male.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "Heavy Assault":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-7-male.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                            case "MAX":
                                {
                                    pictureBox1.Load("https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-8-male.png");
                                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                                    break;
                                }
                        }
                    }          
                }
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            Cursor.Current = Cursors.Default;
            return ;
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("error, " + ex);
            }
        }
        
        //controls clicking in the datagrid (Players Site Link and Quic Stats)
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6 && e.RowIndex != -1)
            {
                // Open the link in the default browser
                System.Diagnostics.Process.Start(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex - 4].Value.ToString());
            }

            else if (e.ColumnIndex == 0 && e.RowIndex != -1 || e.ColumnIndex == 1 && e.RowIndex != -1 || e.ColumnIndex == 2 && e.RowIndex != -1 || e.ColumnIndex == 5 && e.RowIndex != -1 || e.ColumnIndex == 7 && e.RowIndex != -1)
            {        
                Killboard.Clear();
                quickstats(e.RowIndex);
                killboard(e.RowIndex);
                button7.Enabled = true;
                pictureBox4.Visible = true;
            }
        }

        //refresh
        private void button6_Click(object sender, EventArgs e)
        {
            table.Clear();
            Thread.Sleep(100);
            loadDataIntoDataGridView();
        }

        // calls Form5
        private void allTheStatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(ThreadProc));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        // Extends and shrinks form1 (720px; 749px to 1300px; 749px)
        private void button7_Click(object sender, EventArgs e)
        {
            if (sizeChanged == false)
            {
                if (Screen.PrimaryScreen.Bounds.Width <= 1300)
                {
                    this.Size = new Size(1300, 739);
                    this.AutoScroll = true;

                }
                else
                {
                    this.Size = new Size(1300, 739);
                    this.AutoScroll = false;
                }
                button7.Text = "<<";
                sizeChanged = true;
            }
            else
            {
                this.AutoScroll = false;
                this.Size = new Size(720, 739);
                button7.Text = ">>";                
                sizeChanged = false;
            }
        }
        
        // search textBox12
        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            table.DefaultView.RowFilter = string.Format("IGN LIKE '%{0}%'", textBox12.Text);
            if(textBox12.Text == string.Empty)
            {
                if(checkBox2.Checked == true)
                {
                    table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
                }
                else
                {
                    table.DefaultView.RowFilter = "online_status = 13";
                }
            }
        }

        // change row color according to inactivity
        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                checkBox1.Enabled = false;
                linkLabel6.Visible = true;
                dataGridView1.Columns["days"].Visible = true;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int days = Convert.ToInt32(row.Cells[5].Value);
                    int br = Convert.ToInt32(row.Cells[1].Value);
                    string outfitRank = row.Cells[6].Value.ToString();
            
                    if(outfitRank ==  "Recruit")
                    {
                        if(br <= 10 && days >= 4)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                        if (br > 10 && days >= 7 && days <= 13)
            		    {
            		       row.DefaultCellStyle.BackColor = Color.Yellow;
            		    }
                        if (br > 10 && days > 13)
            		    {
            		       row.DefaultCellStyle.BackColor = Color.Red;
            		    }
                    }
                    if(outfitRank == "Grunt")
                    {
                        if (days >= 14 && days <= 20)
                        {
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                        }
                        if (days > 20)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                    }
                    if(outfitRank == "Sentinel")
                    {
                        if (days >= 15 && days <= 29)
                        {
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                        }
                        if (days > 29)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                    }
                }
            }
            else if (checkBox1.Checked == true)
            {
                checkBox3.Enabled = false;
                linkLabel6.Visible = true;
                dataGridView1.Columns["days"].Visible = true;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int days = Convert.ToInt32(row.Cells[5].Value);
                    if (days >= 15 && days <= 29)
                    {
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                    if (days > 29)
                    {
                        row.DefaultCellStyle.BackColor = Color.Red;
                    }
                }
            }
            else
            {
                checkBox1.Enabled = true;
                linkLabel6.Visible = false;
                dataGridView1.Columns["days"].Visible = false;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    row.DefaultCellStyle.BackColor = Color.White;
                }
            }    
        }
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                this.dataGridView1.Sort(this.dataGridView1.Columns["days"], ListSortDirection.Descending);
                checkBox1.Enabled = false;
                linkLabel6.Visible = true;
                dataGridView1.Columns["days"].Visible = true;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int days = Convert.ToInt32(row.Cells[4].Value);
                    int br = Convert.ToInt32(row.Cells[1].Value);
                    string outfitRank = row.Cells[6].Value.ToString();

                    if(outfitRank ==  "Grunt")
                    {
                        if(br <= 10 && days >= 4)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                        if (br > 10 && days >= 7 && days <= 13)
					    {
					       row.DefaultCellStyle.BackColor = Color.Yellow;
					    }
                        if (br > 10 && days > 13)
					    {
					       row.DefaultCellStyle.BackColor = Color.Red;
					    }
                    }
                    if(outfitRank == "Marauder")
                    {
                        if (days >= 14 && days <= 20)
                        {
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                        }
                        if (days > 20)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                    }
                    if(outfitRank == "Sentinel")
                    {
                        if (days >= 15 && days <= 29)
                        {
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                        }
                        if (days > 29)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                    }
                }
            }


            else
            {
                checkBox1.Enabled = true;
                linkLabel6.Visible = false;
                dataGridView1.Columns["days"].Visible = false;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    row.DefaultCellStyle.BackColor = Color.White;
                }
            }
            
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                this.dataGridView1.Sort(this.dataGridView1.Columns["days"], ListSortDirection.Descending);
                checkBox3.Enabled = false;
                linkLabel6.Visible = true;
                dataGridView1.Columns["days"].Visible = true;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int days = Convert.ToInt32(row.Cells[4].Value);
                    if (days >= 15 && days <= 29)
                    {
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                    if (days > 29)
                    {
                        row.DefaultCellStyle.BackColor = Color.Red;
                    }
                }
            }

            else
            {
                checkBox3.Enabled = true;
                linkLabel6.Visible = false;
                dataGridView1.Columns["days"].Visible = false;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int days = Convert.ToInt32(row.Cells[5].Value);
                    if (days >= 15 && days <= 29)
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                    }
                    if (days > 29)
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                    }
                }
            }
        }

        //filter online and offline Checkbox show all members
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                label14.Text = "All Outfit Members";
                dataGridView1.Columns["Rank"].Visible = true;
                //enable checkbox 4 - 8
                checkBox4.Enabled = true;
                checkBox5.Enabled = true;
                checkBox6.Enabled = true;
                checkBox7.Enabled = true;
                checkBox8.Enabled = true;
                table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
            }
            else
            {
                label14.Text = "Members online";
                dataGridView1.Columns["Rank"].Visible = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                checkBox7.Checked = false;
                checkBox8.Checked = false;

                checkBox4.Enabled = false;
                checkBox5.Enabled = false;
                checkBox6.Enabled = false;
                checkBox7.Enabled = false;
                checkBox8.Enabled = false;
                table.DefaultView.RowFilter = "online_status = 13";
            }
        }

        //check for internet connection
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                        return true;                 
                }
                
            }
            catch
            {
                return false;
            }
            
        }
        public static bool CheckIfServersAreOnline()
        {
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();
            options.DontFragment = true;
            var ipAddress = "69.174.194.165";
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 1024;

            PingReply reply = pingSender.Send(ipAddress, timeout, buffer, options);
            if(reply.Status == IPStatus.TimedOut)
            {
                return false;
            }
            else 
            {
                return true;
            }
        }

        private void pingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        //calls the tools_form
        private void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(ToolsForm));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        //quickstat links
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://stats.dasanfall.com/ps2/player/" + label210.Text);
            Process.Start(sInfo);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://www.planetside2.com/players/#!/" + label4.Text);
            Process.Start(sInfo);
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://planetside-intel.com/character/name/" + label210.Text + "/view/summary");
            Process.Start(sInfo);
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://www.planetside-universe.com/character-" + label4.Text + ".php");
            Process.Start(sInfo);
        }

        //leave of absence link
        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://www.outfit418.com/forum/29/leave-of-absence/");
            Process.Start(sInfo);
        }

        //Filter
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked == true)
            {
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                checkBox7.Checked = false;
                checkBox8.Checked = false;
                table.DefaultView.RowFilter = "Rank = 'Grunt'";
            }
            else
            {
                table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
            }

        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked == true)
            {
                checkBox4.Checked = false;
                checkBox6.Checked = false;
                checkBox7.Checked = false;
                checkBox8.Checked = false;
                table.DefaultView.RowFilter = "Rank = 'Marauder'";
            }
            else
            {
                table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked == true)
            {
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox7.Checked = false;
                checkBox8.Checked = false;
                table.DefaultView.RowFilter = "Rank = 'Sentinel'";
            }
            else
            {
                table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
            }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox7.Checked == true)
            {
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                checkBox8.Checked = false;
                table.DefaultView.RowFilter = "Rank = 'Officer'";
            }
            else
            {
                table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox8.Checked == true)
            {
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                checkBox7.Checked = false;
                table.DefaultView.RowFilter = "Rank = 'War Lord'";
            }
            else
            {
                table.DefaultView.RowFilter = "online_status = 0 OR online_status = 13";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        //show continent control
        private void button1_Click_1(object sender, EventArgs e)
        {
            getTerritoryControl();
        }
    }
}



// Planeside 2 IPs
// Briggs:  69.174.220.23
// 
// Ceres:   69.174.194.168
// Cobalt:  69.174.194.165
// Miller:  69.174.194.165
// Woodman: 69.174.194.167
// 
// Connery: 64.37.174.140
// Helios:  64.37.174.140
// 
// Test:        199.108.194.36
// Mattherson:  199.108.194.38
// Waterson:    199.108.194.41


// Male
//***URL Changed to https://www-cdn.planetside2.com/images/players/player/profile/char-default-nc-7-male.png etc.

// Infiltrator: https://players.planetside2.com/images/player/profile/char-default-nc-2-male.png
// Light Assault: https://players.planetside2.com/images/player/profile/char-default-nc-4-male.png
// Medic: https://players.planetside2.com/images/player/profile/char-default-nc-5-male.png
// Engineer: https://players.planetside2.com/images/player/profile/char-default-nc-6-male.png
// Heavy Assault: https://players.planetside2.com/images/player/profile/char-default-nc-7-male.png
// Max: https://players.planetside2.com/images/player/profile/char-default-nc-8-male.png
// 
// Female
// Infiltrator: https://players.planetside2.com/images/player/profile/char-default-nc-2-female.png
// Light Assault: https://players.planetside2.com/images/player/profile/char-default-nc-4-female.png
// Medic: https://players.planetside2.com/images/player/profile/char-default-nc-5-female.png
// Engineer: https://players.planetside2.com/images/player/profile/char-default-nc-6-female.png
// Heavy Assault: https://players.planetside2.com/images/player/profile/char-default-nc-7-female.png
// Max: https://players.planetside2.com/images/player/profile/char-default-nc-8-female.png
