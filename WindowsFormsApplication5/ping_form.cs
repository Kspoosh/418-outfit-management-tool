﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Net.Sockets;

namespace WindowsFormsApplication5
{
    public partial class ping_form : Form
    {
        BackgroundWorker workerPing;
        BackgroundWorker workerTrace;
        public ping_form()
        {

            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void ping_form_Load(object sender, EventArgs e)
        {
            workerPing = new BackgroundWorker();
            workerPing.WorkerReportsProgress = true;
            workerPing.WorkerSupportsCancellation = true;

            workerPing.DoWork += new DoWorkEventHandler(worker_Ping);
            workerPing.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            workerPing.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);


            workerTrace = new BackgroundWorker();
            workerTrace.WorkerReportsProgress = true;
            workerTrace.WorkerSupportsCancellation = true;

            workerTrace.DoWork += new DoWorkEventHandler(worker_Traceroute);
            workerTrace.ProgressChanged += new ProgressChangedEventHandler(workerTrace_ProgressChanged);
            workerTrace.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workerTrace_RunWorkerCompleted);
        }

        //multithreaded Ping
        void worker_Ping(object sender, DoWorkEventArgs e)
        {
            int percentFinished = (int)e.Argument;
            while (!workerPing.CancellationPending && percentFinished < 100)
            {
                //percentFinished = 100;
                //workerPing.ReportProgress(percentFinished);
                Cursor.Current = Cursors.WaitCursor;
                TextBox[] textBoxes = new TextBox[] { textBox1, textBox2, textBox3, textBox4, textBox5, textBox6, textBox7, textBox8, textBox9, textBox10 };
                string[] ip = new string[] { "69.174.220.23", "69.174.194.168", "69.174.194.165", "69.174.194.165", "69.174.194.167", "64.37.174.140", "64.37.174.140", "199.108.194.36", "199.108.194.38", "199.108.194.41" };
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();
                options.DontFragment = true;

                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 1024;

                for (int i = 0; i < 10; i++)
                {
                    percentFinished = (i + 1) * 10;
                    workerPing.ReportProgress(percentFinished);
                    PingReply reply = pingSender.Send(ip[i], timeout, buffer, options);

                    if (reply.Status == IPStatus.Success)
                    {
                        string answer = (reply.RoundtripTime.ToString() + " ms");
                        textBoxes[i].Text = answer;
                    }
                    else
                    {
                        textBoxes[i].Text = ("Error");
                    }
                }
                button1.Enabled = true;
            }
            e.Result = percentFinished;
        }

        //multithreaded Traceroute
        void worker_Traceroute(object sender, DoWorkEventArgs e)
        {
            int percentFinished = (int)e.Argument;
            while (!workerTrace.CancellationPending && percentFinished < 100) //
            {
                //percentFinished++;


                listBox1.Items.Clear();
                Stopwatch SW = new Stopwatch();
                Cursor.Current = Cursors.WaitCursor;
                string[] iplist = new string[] { "69.174.220.23", "69.174.194.168", "69.174.194.165", "69.174.194.165", "69.174.194.167", "64.37.174.140", "64.37.174.140", "199.108.194.36", "199.108.194.38", "199.108.194.41" };
                Ping pingSender = new Ping();
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                PingOptions options = new PingOptions();
                int indNr = comboBox1.SelectedIndex;
                options.DontFragment = true;
                int timeout = 10;

                listBox1.Items.Add("Hop \t Ping \t IP Address \t Hostname");
                listBox1.Items.Add("");

                for (int i = 1; i < 30; i++)
                {
                    percentFinished = i;
                    workerTrace.ReportProgress(percentFinished);
                    progressBar2.Value = i;
                    SW.Reset();
                    SW.Start();
                    try
                    {
                        options.Ttl = i;
                        SW.Reset();
                        SW.Start();
                        PingReply reply = pingSender.Send(iplist[indNr], timeout, buffer, options);
                        SW.Stop();
                        string replyaddress = ReplyAddressParse(reply.Address.ToString());
                        string host = lookup(replyaddress);
                        try
                        {
                            if (!(reply == null))
                            {
                                switch (reply.Status)
                                {
                                    case IPStatus.TtlExpired:
                                        listBox1.Items.Add(i + "\t" + SW.ElapsedMilliseconds + " ms" + "\t" + reply.Address + "\t" + host + "\r");
                                        listBox1.Refresh();
                                        break;
                                    case IPStatus.TimedOut:
                                        listBox1.Items.Add(i + "\t\t" + "\t" + "***" + "\t\t" + "Request timed out." + "\r");
                                        listBox1.Refresh();
                                        break;
                                    case IPStatus.Success:
                                        listBox1.Items.Add(i + "\t" + reply.RoundtripTime + " ms" + "\t" + reply.Address + "\t" + host + "\r");
                                        listBox1.Refresh();
                                        listBox1.Items.Add("\r");
                                        listBox1.Refresh();
                                        listBox1.Items.Add("Traceroute complete");
                                        listBox1.Refresh();
                                        i = 30;
                                        progressBar2.Value = 30;
                                        percentFinished = 100;
                                        button4.Enabled = true;
                                        break;
                                }
                            }
                            else
                            {
                                listBox1.Items.Add(i + "\t" + "Request timed out." + "\r");
                                listBox1.Refresh();
                            }
                        }
                        catch (SocketException SE)
                        {
                            listBox1.Items.Add(i + "\t" + SE.ToString() + "\r");
                            listBox1.Refresh();
                        }
                        catch (PingException PE)
                        {
                            listBox1.Items.Add(i + "\t" + PE.ToString() + "\r");
                            listBox1.Refresh();
                        }
                    }
                    catch (NullReferenceException)
                    {
                        listBox1.Items.Add(i + "\t" + "*" + "\t" + "***.***.***.***" + "\t" + "Request timed out" + "\r");
                        listBox1.Refresh();
                    }
                    //progressBar2.Value++;
                }
                button1.Enabled = true;
            }
            e.Result = percentFinished;
        }

        //reports percentage of the backgroundworker thread
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        void workerTrace_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar2.Value = e.ProgressPercentage;
        }

        //is called when Backgroundworker worker_Ping is done
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.Text = "Start Ping";
        }

        //is called when Backgroundworker worker_Traceroute is done
        void workerTrace_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button4.Text = "Start Trace";
        }

        // Ping Button
        private void button1_Click(object sender, EventArgs e)
        {
            if (workerPing.IsBusy)
            {
                workerPing.CancelAsync();
                button1.Text = "Start Ping";
                button1.Enabled = true;
            }
            else
            {
                if (progressBar1.Value == progressBar1.Maximum)
                {
                    progressBar1.Value = progressBar1.Minimum;
                }
                workerPing.RunWorkerAsync(progressBar1.Value);
                button1.Text = "Start Ping";
                button1.Enabled = false;
            }
        }

        // Obsolete NSlookup 
        private static string GetNameFromIPAddress(string ipAddress)
        {
            string machineName = string.Empty;
            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(ipAddress);
                machineName = hostEntry.HostName;
            }
            catch (Exception)
            {
                machineName = "***";
            }
            return machineName;
        }

        //NSlookup with checkbox control
        public string lookup(string ipAddress)
        {
            string machineName = string.Empty;
            if (checkBox1.Checked)
            {
                try
                {
                    IPHostEntry hostEntry = Dns.GetHostEntry(ipAddress);
                    machineName = hostEntry.HostName;
                }
                catch (Exception)
                {
                    machineName = "***";
                }
                return machineName;
            }
            else
            {
                machineName = "***";
                return machineName;
            }
        }

        // Checks if Replyaddress isn't empty
        private static string ReplyAddressParse(string replyaddress)
        {
            string replyipaddress = string.Empty;
            if (!(replyaddress == null))
            {
                replyipaddress = replyaddress;
            }
            else
            {
                replyipaddress = "Unknown";
            }

            return replyipaddress;
        }

        // Start Trace Button
        private void button4_Click(object sender, EventArgs e)
        {
            if (workerTrace.IsBusy)
            {
                workerTrace.CancelAsync();
                //listBox1.Text = null;
                button4.Text = "Start Trace";
                button4.Enabled = true;
            }
            else
            {
                if (progressBar2.Value == progressBar2.Maximum)
                {
                    progressBar2.Value = progressBar2.Minimum;
                }
                workerTrace.RunWorkerAsync(progressBar2.Value);
                button4.Text = "Start Trace";
                button4.Enabled = false;
            }
        }


        // Copy to Clipboard button
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string s1 = "";
                foreach (object item in listBox1.Items) s1 += item.ToString() + "\n\r";
                Clipboard.SetText(s1);
            }
            catch (ArgumentNullException)
            {
                string message = "Please start a trace first";
                string caption = "Attention";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, caption, buttons);
            }
        }

        // Teamspeak connection
        private void button5_Click(object sender, EventArgs e)
        {
            string username = textBox11.Text;
            if (!(textBox11.Text.Length == 0))
            {
                Process.Start("ts3server://ts.outfit418.com/?nickname=" + username);
            }
            else
            {
                string message = "You did not enter a Username";
                string caption = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);
            }

        }

        // Teamspeak downloadlink
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://teamspeak.com/?page=downloads/");
            Process.Start(sInfo);
        }
    }
        
}
