﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Net.Sockets;

namespace WindowsFormsApplication5
{
    
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private static string GetNameFromIPAddress(string ipAddress)
        {
            string machineName = string.Empty;
            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(ipAddress);
                machineName = hostEntry.HostName;
            }
            catch (Exception)
            {
                machineName = "***";
            }
            return machineName;

        }

        private static string ReplyAddressParse(string replyaddress)
        {
            string replyipaddress = string.Empty;
            if (!(replyaddress == null))
            {
                replyipaddress = replyaddress;
            }
            else
            {
                replyipaddress = "Unknown";
            }

            return replyipaddress;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Stopwatch SW = new Stopwatch();
            Cursor.Current = Cursors.WaitCursor;
            string[] iplist = new string[] { "69.174.220.23", "69.174.194.168", "69.174.194.165", "69.174.194.165", "69.174.194.167", "64.37.174.140", "64.37.174.140", "199.108.194.36", "199.108.194.38", "199.108.194.41" };
            Ping pingSender = new Ping();
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            PingOptions options = new PingOptions(); 
            int indNr = comboBox1.SelectedIndex;
            options.DontFragment = true;                     
            int timeout = 10;

            listBox1.Items.Add("Hop \t Ping \t IP Address \t Hostname");
            listBox1.Items.Add("");

            for (int i = 1; i < 30; i++)
            {
                SW.Reset();
                SW.Start();
                try
                {
                    options.Ttl = i;
                    SW.Reset();
                    SW.Start();                    
                    PingReply reply = pingSender.Send(iplist[indNr], timeout, buffer, options);
                    SW.Stop();
                    //int hops = i;                
                    string replyaddress = ReplyAddressParse(reply.Address.ToString());
                    string host = GetNameFromIPAddress(replyaddress);
                    try
                    {
                        if (!(reply == null))
                        {
                            switch (reply.Status)
                            {
                                case IPStatus.TtlExpired:
                                    listBox1.Items.Add(i + "\t" + SW.ElapsedMilliseconds + " ms" + "\t" + reply.Address + "\t" + host + "\r");
                                    listBox1.Refresh();
                                    break;
                                case IPStatus.TimedOut:
                                    listBox1.Items.Add(i + "\t\t" + "\t" + "***" + "\t\t" + "Request timed out." + "\r");
                                    listBox1.Refresh();
                                    break;
                                case IPStatus.Success:
                                    listBox1.Items.Add(i + "\t" + reply.RoundtripTime + " ms" + "\t" + reply.Address + "\t" + host + "\r");
                                    listBox1.Refresh();
                                    listBox1.Items.Add("\r");
                                    listBox1.Refresh();
                                    listBox1.Items.Add("Traceroute complete");
                                    listBox1.Refresh();
                                    i = 30;
                                    break;
                            }
                        }
                        else
                        {
                            listBox1.Items.Add(i + "\t" + "Request timed out." + "\r");
                            listBox1.Refresh();
                        }
                    }
                    catch (SocketException SE)
                    {
                        listBox1.Items.Add(i + "\t" + SE.ToString() + "\r");
                        listBox1.Refresh();
                    }
                    catch (PingException PE)
                    {
                        listBox1.Items.Add(i + "\t" + PE.ToString() + "\r");
                        listBox1.Refresh();
                    }
                }
                catch (NullReferenceException)
                {
                    listBox1.Items.Add(i + "\t" + "*" + "\t" + "***.***.***.***" + "\t" + "Request timed out" + "\r");
                    listBox1.Refresh();
                }
                progressBar1.Value = i;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s1 = "";
            foreach (object item in listBox1.Items) s1 += item.ToString() + "\n\r";
            Clipboard.SetText(s1);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string s1 = "";
            foreach (object item in listBox1.Items) s1 += item.ToString() + "\n\r";
            Clipboard.SetText(s1);
        }
    }
}