﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.Net.Sockets;
using System.Xml;
using System.Xml.Linq;

namespace WindowsFormsApplication5
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
            setupDataGridView();
            loadDataIntoDataGridView();
        }

        //setting up the Datagidview1 
        void setupDataGridView()
        {
            this.Controls.Add(dataGridView1);

            DataGridViewTextBoxColumn col1 = new DataGridViewTextBoxColumn();
            col1.Name = "column1";
            col1.HeaderText = "IGN";
            col1.Width = 100;
            dataGridView1.Columns.Add(col1);

            DataGridViewTextBoxColumn col2 = new DataGridViewTextBoxColumn();
            col2.Name = "column2";
            col2.HeaderText = "BR";
            col2.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col2.Width = 100;
            dataGridView1.Columns.Add(col2);

            DataGridViewTextBoxColumn col3 = new DataGridViewTextBoxColumn();
            col3.Name = "column3";
            col3.HeaderText = "Outfit rank";
            col3.Width = 100;
            dataGridView1.Columns.Add(col3);
            
            //DataGridViewTextBoxColumn col4 = new DataGridViewTextBoxColumn();
            //col4.Name = "column4";
            //col4.HeaderText = "Last Login";
            //col4.Width = 100;
            //dataGridView1.Columns.Add(col4);



            //DataGridViewButtonColumn col4 = new DataGridViewButtonColumn();
            //col4.Name = "Link";
            //col4.Text = "Link";
            //col4.HeaderText = "Players Site";
            ////col4.Index = 4;
            //col4.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //col4.SortMode = DataGridViewColumnSortMode.Automatic;
            //col4.Width = 200;
            //dataGridView1.Columns.Add(col4);
            //
            //DataGridViewTextBoxColumn col5 = new DataGridViewTextBoxColumn();
            //col5.Name = "column5";
            //col5.HeaderText = "Link";
            //col5.Width = 100;
            //col5.Visible = false;
            //dataGridView1.Columns.Add(col5);
        }

        // Gets XML from census.soe.com, parses it and writes it into the Datagridview1
        void loadDataIntoDataGridView()
        {


            var apiURL = "http://census.soe.com/s:soe/xml/get/ps2:v2/outfit_member?c:limit=1000&c:resolve=online_status,character&c:join=type:profile^list:0^inject_at:profile^show:name.en^on:character.profile_id^to:profile_id&outfit_id=37510026483741634";
            XElement doc = XElement.Load(apiURL);

            IEnumerable<XElement> list1 =
                from el in doc.DescendantsAndSelf("outfit_member")
                //where (string)el.Attribute("online_status") == "11"
                orderby (string)el.Element("character").Element("name").Attribute("first")
                select el;
            try
            {
                foreach (XElement el in list1)
                {
                    //get Name
                    string nameResult = el.Element("character").Element("name").Attribute("first").ToString();
                    string nameParse = nameResult.Replace("first=", "");
                    string nameParse2 = nameParse.Replace("\"", "");

                    //get BR
                    string brResult = el.Element("character").Element("battle_rank").Attribute("value").ToString();
                    string brParse = brResult.Replace("value=", "");
                    string brParse2 = brParse.Replace("\"", "");

                    //get played class
                    string classResult = el.Element("character").Element("profile").Element("name").Attribute("en").ToString();
                    string classParse = classResult.Replace("en=", "");
                    string classParse2 = classParse.Replace("\"", "");

                    //get character id
                    string playerIDResult = el.Attribute("character_id").ToString();
                    string idParse = playerIDResult.Replace("character_id=", "");
                    string idParse2 = idParse.Replace("\"", "");

                    //get outfitrank
                    string orResult = el.Attribute("rank").ToString();
                    string orParse = orResult.Replace("rank=", "");
                    string orParse2 = orParse.Replace("\"", "");

                    //get last logged in
                    //string llResult = el.Element("character").Attribute("last_login_date").ToString();
                    //string llParse = llResult.Replace("last_login_date=", "");
                    //string llParse2 = llParse.Replace("\"", "");
                    //DateTime lastLogin = DateTime.Parse(llParse2);

                    //datagrid stuff
                    var playersLink = "https://players.planetside2.com/#!/" + idParse2;
                    //dataGridView1.Rows.Add(nameParse2, brParse2, classParse2, "Link", playersLink);
                    dataGridView1.Rows.Add(nameParse2, brParse2, orParse2);


                }
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }


        private void Form5_Load(object sender, EventArgs e)
        {}
    }
}
