﻿namespace WindowsFormsApplication5
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button7 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox176 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label32 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.pictureBox177 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox102 = new System.Windows.Forms.PictureBox();
            this.pictureBox103 = new System.Windows.Forms.PictureBox();
            this.pictureBox104 = new System.Windows.Forms.PictureBox();
            this.pictureBox105 = new System.Windows.Forms.PictureBox();
            this.pictureBox106 = new System.Windows.Forms.PictureBox();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.pictureBox107 = new System.Windows.Forms.PictureBox();
            this.pictureBox108 = new System.Windows.Forms.PictureBox();
            this.pictureBox109 = new System.Windows.Forms.PictureBox();
            this.pictureBox110 = new System.Windows.Forms.PictureBox();
            this.pictureBox111 = new System.Windows.Forms.PictureBox();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.pictureBox63 = new System.Windows.Forms.PictureBox();
            this.pictureBox80 = new System.Windows.Forms.PictureBox();
            this.pictureBox81 = new System.Windows.Forms.PictureBox();
            this.pictureBox82 = new System.Windows.Forms.PictureBox();
            this.pictureBox89 = new System.Windows.Forms.PictureBox();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.pictureBox90 = new System.Windows.Forms.PictureBox();
            this.pictureBox91 = new System.Windows.Forms.PictureBox();
            this.pictureBox92 = new System.Windows.Forms.PictureBox();
            this.pictureBox93 = new System.Windows.Forms.PictureBox();
            this.pictureBox100 = new System.Windows.Forms.PictureBox();
            this.pictureBox101 = new System.Windows.Forms.PictureBox();
            this.pictureBox83 = new System.Windows.Forms.PictureBox();
            this.pictureBox84 = new System.Windows.Forms.PictureBox();
            this.pictureBox85 = new System.Windows.Forms.PictureBox();
            this.pictureBox86 = new System.Windows.Forms.PictureBox();
            this.pictureBox87 = new System.Windows.Forms.PictureBox();
            this.pictureBox88 = new System.Windows.Forms.PictureBox();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.pictureBox94 = new System.Windows.Forms.PictureBox();
            this.pictureBox95 = new System.Windows.Forms.PictureBox();
            this.pictureBox96 = new System.Windows.Forms.PictureBox();
            this.pictureBox97 = new System.Windows.Forms.PictureBox();
            this.pictureBox98 = new System.Windows.Forms.PictureBox();
            this.pictureBox99 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.pictureBox68 = new System.Windows.Forms.PictureBox();
            this.pictureBox69 = new System.Windows.Forms.PictureBox();
            this.pictureBox70 = new System.Windows.Forms.PictureBox();
            this.pictureBox71 = new System.Windows.Forms.PictureBox();
            this.pictureBox72 = new System.Windows.Forms.PictureBox();
            this.pictureBox73 = new System.Windows.Forms.PictureBox();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.pictureBox64 = new System.Windows.Forms.PictureBox();
            this.pictureBox65 = new System.Windows.Forms.PictureBox();
            this.pictureBox66 = new System.Windows.Forms.PictureBox();
            this.pictureBox67 = new System.Windows.Forms.PictureBox();
            this.pictureBox74 = new System.Windows.Forms.PictureBox();
            this.pictureBox75 = new System.Windows.Forms.PictureBox();
            this.pictureBox76 = new System.Windows.Forms.PictureBox();
            this.pictureBox77 = new System.Windows.Forms.PictureBox();
            this.pictureBox78 = new System.Windows.Forms.PictureBox();
            this.pictureBox79 = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.pictureBox134 = new System.Windows.Forms.PictureBox();
            this.pictureBox135 = new System.Windows.Forms.PictureBox();
            this.pictureBox136 = new System.Windows.Forms.PictureBox();
            this.pictureBox137 = new System.Windows.Forms.PictureBox();
            this.pictureBox138 = new System.Windows.Forms.PictureBox();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.pictureBox139 = new System.Windows.Forms.PictureBox();
            this.pictureBox140 = new System.Windows.Forms.PictureBox();
            this.pictureBox141 = new System.Windows.Forms.PictureBox();
            this.pictureBox142 = new System.Windows.Forms.PictureBox();
            this.pictureBox143 = new System.Windows.Forms.PictureBox();
            this.pictureBox112 = new System.Windows.Forms.PictureBox();
            this.pictureBox113 = new System.Windows.Forms.PictureBox();
            this.pictureBox114 = new System.Windows.Forms.PictureBox();
            this.pictureBox115 = new System.Windows.Forms.PictureBox();
            this.pictureBox116 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.pictureBox117 = new System.Windows.Forms.PictureBox();
            this.pictureBox118 = new System.Windows.Forms.PictureBox();
            this.pictureBox119 = new System.Windows.Forms.PictureBox();
            this.pictureBox120 = new System.Windows.Forms.PictureBox();
            this.pictureBox121 = new System.Windows.Forms.PictureBox();
            this.pictureBox122 = new System.Windows.Forms.PictureBox();
            this.pictureBox123 = new System.Windows.Forms.PictureBox();
            this.pictureBox124 = new System.Windows.Forms.PictureBox();
            this.pictureBox125 = new System.Windows.Forms.PictureBox();
            this.pictureBox126 = new System.Windows.Forms.PictureBox();
            this.pictureBox127 = new System.Windows.Forms.PictureBox();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.pictureBox128 = new System.Windows.Forms.PictureBox();
            this.pictureBox129 = new System.Windows.Forms.PictureBox();
            this.pictureBox130 = new System.Windows.Forms.PictureBox();
            this.pictureBox131 = new System.Windows.Forms.PictureBox();
            this.pictureBox132 = new System.Windows.Forms.PictureBox();
            this.pictureBox133 = new System.Windows.Forms.PictureBox();
            this.label153 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.pictureBox144 = new System.Windows.Forms.PictureBox();
            this.pictureBox145 = new System.Windows.Forms.PictureBox();
            this.pictureBox146 = new System.Windows.Forms.PictureBox();
            this.pictureBox147 = new System.Windows.Forms.PictureBox();
            this.pictureBox148 = new System.Windows.Forms.PictureBox();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.pictureBox149 = new System.Windows.Forms.PictureBox();
            this.pictureBox150 = new System.Windows.Forms.PictureBox();
            this.pictureBox151 = new System.Windows.Forms.PictureBox();
            this.pictureBox152 = new System.Windows.Forms.PictureBox();
            this.pictureBox153 = new System.Windows.Forms.PictureBox();
            this.pictureBox154 = new System.Windows.Forms.PictureBox();
            this.pictureBox155 = new System.Windows.Forms.PictureBox();
            this.pictureBox156 = new System.Windows.Forms.PictureBox();
            this.pictureBox157 = new System.Windows.Forms.PictureBox();
            this.pictureBox158 = new System.Windows.Forms.PictureBox();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.pictureBox159 = new System.Windows.Forms.PictureBox();
            this.pictureBox160 = new System.Windows.Forms.PictureBox();
            this.pictureBox161 = new System.Windows.Forms.PictureBox();
            this.pictureBox162 = new System.Windows.Forms.PictureBox();
            this.pictureBox163 = new System.Windows.Forms.PictureBox();
            this.pictureBox164 = new System.Windows.Forms.PictureBox();
            this.pictureBox165 = new System.Windows.Forms.PictureBox();
            this.pictureBox166 = new System.Windows.Forms.PictureBox();
            this.pictureBox167 = new System.Windows.Forms.PictureBox();
            this.pictureBox168 = new System.Windows.Forms.PictureBox();
            this.pictureBox169 = new System.Windows.Forms.PictureBox();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.pictureBox170 = new System.Windows.Forms.PictureBox();
            this.pictureBox171 = new System.Windows.Forms.PictureBox();
            this.pictureBox172 = new System.Windows.Forms.PictureBox();
            this.pictureBox173 = new System.Windows.Forms.PictureBox();
            this.pictureBox174 = new System.Windows.Forms.PictureBox();
            this.pictureBox175 = new System.Windows.Forms.PictureBox();
            this.label182 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tracerouteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teamspeakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTheStatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.label72 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label210 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox176)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox177)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox99)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox79)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox133)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox175)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage9.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(663, 64);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(29, 624);
            this.button7.TabIndex = 30;
            this.button7.Text = ">>";
            this.toolTip1.SetToolTip(this.button7, "QuickStats");
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 22);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(111, 17);
            this.checkBox2.TabIndex = 71;
            this.checkBox2.Text = "Show all members";
            this.toolTip1.SetToolTip(this.checkBox2, "Show all outfit members");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(6, 178);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(111, 17);
            this.checkBox3.TabIndex = 104;
            this.checkBox3.Text = "Highlight inactivity";
            this.toolTip1.SetToolTip(this.checkBox3, "Highlights inactive members");
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(3, 658);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(146, 30);
            this.button6.TabIndex = 1;
            this.button6.Text = "Refresh Data";
            this.button6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.button6, "Get new data from SOE");
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Enabled = false;
            this.checkBox4.Location = new System.Drawing.Point(34, 45);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(87, 17);
            this.checkBox4.TabIndex = 105;
            this.checkBox4.Text = "Show Grunts";
            this.toolTip1.SetToolTip(this.checkBox4, "Show only Recuits");
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Enabled = false;
            this.checkBox5.Location = new System.Drawing.Point(34, 68);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(106, 17);
            this.checkBox5.TabIndex = 106;
            this.checkBox5.Text = "Show Marauders";
            this.toolTip1.SetToolTip(this.checkBox5, "Show only Grunts");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Enabled = false;
            this.checkBox6.Location = new System.Drawing.Point(34, 91);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(99, 17);
            this.checkBox6.TabIndex = 107;
            this.checkBox6.Text = "Show Sentinels";
            this.toolTip1.SetToolTip(this.checkBox6, "Show only Marauders");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Enabled = false;
            this.checkBox7.Location = new System.Drawing.Point(34, 114);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(92, 17);
            this.checkBox7.TabIndex = 108;
            this.checkBox7.Text = "Show Officers";
            this.toolTip1.SetToolTip(this.checkBox7, "Show only Officers");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Enabled = false;
            this.checkBox8.Location = new System.Drawing.Point(34, 137);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(98, 17);
            this.checkBox8.TabIndex = 109;
            this.checkBox8.Text = "Show Warlords";
            this.toolTip1.SetToolTip(this.checkBox8, "Show only Warlords");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 201);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(136, 17);
            this.checkBox1.TabIndex = 116;
            this.checkBox1.Text = "Highlight inactivity *old*";
            this.toolTip1.SetToolTip(this.checkBox1, "Highlight Members who bring down the activity statistic");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // linkLabel6
            // 
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.LinkColor = System.Drawing.Color.Red;
            this.linkLabel6.Location = new System.Drawing.Point(396, 39);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(255, 13);
            this.linkLabel6.TabIndex = 119;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "***Remember to check for leave of absence posts***";
            this.toolTip1.SetToolTip(this.linkLabel6, "Click here to go directly to the leave of absence forum");
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 130);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(81, 13);
            this.label65.TabIndex = 82;
            this.label65.Text = "Time to BR100:";
            this.toolTip1.SetToolTip(this.label65, "Aproximate time needed to reach BR100 ");
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 111);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(86, 13);
            this.label64.TabIndex = 81;
            this.label64.Text = "Score to BR100:";
            this.toolTip1.SetToolTip(this.label64, "Score needed to BR100");
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(32, 487);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(81, 13);
            this.label73.TabIndex = 27;
            this.label73.Text = "Nano-Armor Kit:";
            this.toolTip1.SetToolTip(this.label73, "Repair tool");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 487);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Nano-Regen Device:";
            this.toolTip1.SetToolTip(this.label11, "AOE heal");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 408);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Medical Applicator:";
            this.toolTip1.SetToolTip(this.label6, "Medical applicator");
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(32, 408);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(110, 13);
            this.label74.TabIndex = 40;
            this.label74.Text = "Ammunition Package:";
            this.toolTip1.SetToolTip(this.label74, "Ammo pack");
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(32, 487);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(88, 13);
            this.label81.TabIndex = 58;
            this.label81.Text = "Drifter Jump Jets:";
            this.toolTip1.SetToolTip(this.label81, "Nano-Regen Device");
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(32, 408);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(57, 13);
            this.label82.TabIndex = 45;
            this.label82.Text = "Jump Jets:";
            this.toolTip1.SetToolTip(this.label82, "Medical applicator");
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(32, 408);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(123, 13);
            this.label131.TabIndex = 97;
            this.label131.Text = "Hunter Cloaking Device:";
            this.toolTip1.SetToolTip(this.label131, "Nano-Regen Device");
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(32, 329);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(114, 13);
            this.label132.TabIndex = 84;
            this.label132.Text = "Recon Detect Device:";
            this.toolTip1.SetToolTip(this.label132, "Medical applicator");
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(32, 487);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(110, 13);
            this.label140.TabIndex = 134;
            this.label140.Text = "Nano-Armor Cloaking:";
            this.toolTip1.SetToolTip(this.label140, "Nano-Regen Device");
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(32, 408);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(92, 13);
            this.label146.TabIndex = 171;
            this.label146.Text = "Adrenaline Shield:";
            this.toolTip1.SetToolTip(this.label146, "Nano-Regen Device");
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(32, 329);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(120, 13);
            this.label154.TabIndex = 151;
            this.label154.Text = "Nanite Mesh Generator:";
            this.toolTip1.SetToolTip(this.label154, "Nano-Regen Device");
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(32, 487);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(71, 13);
            this.label161.TabIndex = 188;
            this.label161.Text = "Resist Shield:";
            this.toolTip1.SetToolTip(this.label161, "Nano-Regen Device");
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(32, 329);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(120, 13);
            this.label168.TabIndex = 242;
            this.label168.Text = "Ammo Storage Canister:";
            this.toolTip1.SetToolTip(this.label168, "Nano-Regen Device");
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(32, 408);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(68, 13);
            this.label175.TabIndex = 225;
            this.label175.Text = "Aegis Shield:";
            this.toolTip1.SetToolTip(this.label175, "Nano-Regen Device");
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(32, 487);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(44, 13);
            this.label183.TabIndex = 205;
            this.label183.Text = "Charge:";
            this.toolTip1.SetToolTip(this.label183, "Nano-Regen Device");
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(3, 615);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 30);
            this.button1.TabIndex = 124;
            this.button1.Text = "Territory Control";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolTip1.SetToolTip(this.button1, "Territory Control");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Location = new System.Drawing.Point(713, 65);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(568, 623);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label210);
            this.tabPage1.Controls.Add(this.pictureBox176);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label69);
            this.tabPage1.Controls.Add(this.label68);
            this.tabPage1.Controls.Add(this.progressBar1);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.progressBar3);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label55);
            this.tabPage1.Controls.Add(this.label53);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.label54);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.pictureBox31);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(560, 597);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Overview";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBox176
            // 
            this.pictureBox176.BackColor = System.Drawing.Color.DimGray;
            this.pictureBox176.Location = new System.Drawing.Point(258, 24);
            this.pictureBox176.Name = "pictureBox176";
            this.pictureBox176.Size = new System.Drawing.Size(64, 64);
            this.pictureBox176.TabIndex = 112;
            this.pictureBox176.TabStop = false;
            this.pictureBox176.Visible = false;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(152, 118);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 16);
            this.label10.TabIndex = 111;
            this.label10.Text = "label10";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(502, 66);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(55, 13);
            this.label69.TabIndex = 109;
            this.label69.Text = "br percent";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(362, 66);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(135, 13);
            this.label68.TabIndex = 108;
            this.label68.Text = "Progress to Battlerank 100:";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(365, 82);
            this.progressBar1.MarqueeAnimationSpeed = 0;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(174, 23);
            this.progressBar1.TabIndex = 107;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Cursor = System.Windows.Forms.Cursors.Default;
            this.label32.Location = new System.Drawing.Point(502, 14);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(55, 13);
            this.label32.TabIndex = 61;
            this.label32.Text = "br percent";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(517, 578);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 106;
            this.label4.Text = "label4";
            this.label4.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.pictureBox177);
            this.groupBox1.Location = new System.Drawing.Point(6, 443);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 148);
            this.groupBox1.TabIndex = 105;
            this.groupBox1.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(49, 19);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 13);
            this.label26.TabIndex = 107;
            this.label26.Text = "Nanites";
            // 
            // label51
            // 
            this.label51.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label51.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label51.Location = new System.Drawing.Point(48, 111);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(50, 24);
            this.label51.TabIndex = 105;
            this.label51.Text = "label";
            this.label51.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox177
            // 
            this.pictureBox177.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox177.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox177.BackgroundImage")));
            this.pictureBox177.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox177.Location = new System.Drawing.Point(37, 35);
            this.pictureBox177.Name = "pictureBox177";
            this.pictureBox177.Size = new System.Drawing.Size(72, 73);
            this.pictureBox177.TabIndex = 106;
            this.pictureBox177.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(362, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 13);
            this.label17.TabIndex = 52;
            this.label17.Text = "Progress to next Battlerank:";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(152, -1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 31);
            this.label15.TabIndex = 54;
            this.label15.Text = "br";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar3
            // 
            this.progressBar3.Enabled = false;
            this.progressBar3.Location = new System.Drawing.Point(365, 30);
            this.progressBar3.MarqueeAnimationSpeed = 0;
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(174, 23);
            this.progressBar3.TabIndex = 51;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label71);
            this.groupBox5.Controls.Add(this.label70);
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Controls.Add(this.label65);
            this.groupBox5.Controls.Add(this.label64);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this.label66);
            this.groupBox5.Location = new System.Drawing.Point(365, 111);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(174, 156);
            this.groupBox5.TabIndex = 91;
            this.groupBox5.TabStop = false;
            // 
            // label71
            // 
            this.label71.Location = new System.Drawing.Point(90, 73);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(74, 13);
            this.label71.TabIndex = 110;
            this.label71.Text = "score";
            this.label71.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 73);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(38, 13);
            this.label70.TabIndex = 110;
            this.label70.Text = "Score:";
            // 
            // label67
            // 
            this.label67.Location = new System.Drawing.Point(106, 130);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(58, 13);
            this.label67.TabIndex = 84;
            this.label67.Text = "t2br100";
            this.label67.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(123, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 80;
            this.label13.Text = "spm";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 79;
            this.label12.Text = "SPM:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(28, 13);
            this.label25.TabIndex = 36;
            this.label25.Text = "Kills:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 35);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 39;
            this.label23.Text = "Deaths:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 54);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 41;
            this.label22.Text = "K/D ratio:";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(123, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "kills";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(123, 35);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 13);
            this.label39.TabIndex = 77;
            this.label39.Text = "deaths";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(123, 53);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 13);
            this.label40.TabIndex = 78;
            this.label40.Text = "kd";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label66
            // 
            this.label66.Location = new System.Drawing.Point(90, 111);
            this.label66.Name = "label66";
            this.label66.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label66.Size = new System.Drawing.Size(74, 13);
            this.label66.TabIndex = 83;
            this.label66.Text = "s2br100";
            this.label66.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Controls.Add(this.label8);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this.label33);
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Location = new System.Drawing.Point(8, 272);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(159, 155);
            this.groupBox9.TabIndex = 101;
            this.groupBox9.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 92;
            this.label9.Text = "creation date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 91;
            this.label8.Text = "Creation date:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 49);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(55, 13);
            this.label50.TabIndex = 90;
            this.label50.Text = "Last login:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(13, 62);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 13);
            this.label33.TabIndex = 70;
            this.label33.Text = "last login";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 98);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 13);
            this.label34.TabIndex = 72;
            this.label34.Text = "login count";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 85);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(64, 13);
            this.label41.TabIndex = 89;
            this.label41.Text = "Total logins:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 118);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(90, 13);
            this.label31.TabIndex = 88;
            this.label31.Text = "Total time played:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(13, 131);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 13);
            this.label35.TabIndex = 73;
            this.label35.Text = "minutes played";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label63);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.label56);
            this.groupBox6.Location = new System.Drawing.Point(365, 273);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(174, 93);
            this.groupBox6.TabIndex = 92;
            this.groupBox6.TabStop = false;
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(123, 35);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 120;
            this.label63.Text = "label63";
            this.label63.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 43;
            this.label21.Text = "Available Certs:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "Certs spent:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Certs earned:";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(123, 16);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 74;
            this.label36.Text = "label36";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(123, 74);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 13);
            this.label38.TabIndex = 76;
            this.label38.Text = "label38";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(123, 54);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 13);
            this.label37.TabIndex = 75;
            this.label37.Text = "label37";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 35);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(74, 13);
            this.label56.TabIndex = 77;
            this.label56.Text = "Passive Certs:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label27.Location = new System.Drawing.Point(154, 176);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 13);
            this.label27.TabIndex = 56;
            this.label27.Text = "outfit rank";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(152, 163);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(59, 13);
            this.label55.TabIndex = 100;
            this.label55.Text = "Outfit rank:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(152, 199);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(103, 13);
            this.label53.TabIndex = 98;
            this.label53.Text = "Outfit member since:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label42);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Location = new System.Drawing.Point(365, 372);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(174, 56);
            this.groupBox7.TabIndex = 93;
            this.groupBox7.TabStop = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 16);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(95, 13);
            this.label42.TabIndex = 80;
            this.label42.Text = "Facilities captured:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 35);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(98, 13);
            this.label43.TabIndex = 81;
            this.label43.Text = "Facilities defended:";
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(123, 16);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(41, 13);
            this.label44.TabIndex = 82;
            this.label44.Text = "label44";
            this.label44.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(123, 35);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 13);
            this.label45.TabIndex = 83;
            this.label45.Text = "label45";
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(154, 212);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(98, 13);
            this.label54.TabIndex = 99;
            this.label54.Text = "outfit member since";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this.label47);
            this.groupBox8.Controls.Add(this.label48);
            this.groupBox8.Controls.Add(this.label49);
            this.groupBox8.Location = new System.Drawing.Point(365, 434);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(174, 57);
            this.groupBox8.TabIndex = 94;
            this.groupBox8.TabStop = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 16);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(69, 13);
            this.label46.TabIndex = 84;
            this.label46.Text = "Medal count:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 35);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(74, 13);
            this.label47.TabIndex = 85;
            this.label47.Text = "Ribbon count:";
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(123, 16);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 13);
            this.label48.TabIndex = 86;
            this.label48.Text = "label48";
            this.label48.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(123, 35);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 13);
            this.label49.TabIndex = 87;
            this.label49.Text = "label49";
            this.label49.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 257);
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox31.Location = new System.Drawing.Point(152, 24);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(100, 100);
            this.pictureBox31.TabIndex = 110;
            this.pictureBox31.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox102);
            this.tabPage2.Controls.Add(this.pictureBox103);
            this.tabPage2.Controls.Add(this.pictureBox104);
            this.tabPage2.Controls.Add(this.pictureBox105);
            this.tabPage2.Controls.Add(this.pictureBox106);
            this.tabPage2.Controls.Add(this.label134);
            this.tabPage2.Controls.Add(this.label135);
            this.tabPage2.Controls.Add(this.label136);
            this.tabPage2.Controls.Add(this.label137);
            this.tabPage2.Controls.Add(this.label138);
            this.tabPage2.Controls.Add(this.label139);
            this.tabPage2.Controls.Add(this.pictureBox107);
            this.tabPage2.Controls.Add(this.pictureBox108);
            this.tabPage2.Controls.Add(this.pictureBox109);
            this.tabPage2.Controls.Add(this.pictureBox110);
            this.tabPage2.Controls.Add(this.pictureBox111);
            this.tabPage2.Controls.Add(this.label140);
            this.tabPage2.Controls.Add(this.pictureBox61);
            this.tabPage2.Controls.Add(this.pictureBox63);
            this.tabPage2.Controls.Add(this.pictureBox80);
            this.tabPage2.Controls.Add(this.pictureBox81);
            this.tabPage2.Controls.Add(this.pictureBox82);
            this.tabPage2.Controls.Add(this.pictureBox89);
            this.tabPage2.Controls.Add(this.label118);
            this.tabPage2.Controls.Add(this.label119);
            this.tabPage2.Controls.Add(this.label120);
            this.tabPage2.Controls.Add(this.label121);
            this.tabPage2.Controls.Add(this.label122);
            this.tabPage2.Controls.Add(this.label133);
            this.tabPage2.Controls.Add(this.pictureBox90);
            this.tabPage2.Controls.Add(this.pictureBox91);
            this.tabPage2.Controls.Add(this.pictureBox92);
            this.tabPage2.Controls.Add(this.pictureBox93);
            this.tabPage2.Controls.Add(this.pictureBox100);
            this.tabPage2.Controls.Add(this.pictureBox101);
            this.tabPage2.Controls.Add(this.pictureBox83);
            this.tabPage2.Controls.Add(this.pictureBox84);
            this.tabPage2.Controls.Add(this.pictureBox85);
            this.tabPage2.Controls.Add(this.pictureBox86);
            this.tabPage2.Controls.Add(this.pictureBox87);
            this.tabPage2.Controls.Add(this.pictureBox88);
            this.tabPage2.Controls.Add(this.label123);
            this.tabPage2.Controls.Add(this.label124);
            this.tabPage2.Controls.Add(this.label125);
            this.tabPage2.Controls.Add(this.label126);
            this.tabPage2.Controls.Add(this.label127);
            this.tabPage2.Controls.Add(this.label128);
            this.tabPage2.Controls.Add(this.label129);
            this.tabPage2.Controls.Add(this.label130);
            this.tabPage2.Controls.Add(this.label131);
            this.tabPage2.Controls.Add(this.pictureBox94);
            this.tabPage2.Controls.Add(this.pictureBox95);
            this.tabPage2.Controls.Add(this.pictureBox96);
            this.tabPage2.Controls.Add(this.pictureBox97);
            this.tabPage2.Controls.Add(this.pictureBox98);
            this.tabPage2.Controls.Add(this.pictureBox99);
            this.tabPage2.Controls.Add(this.label132);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(560, 597);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Infiltrator";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox102
            // 
            this.pictureBox102.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox102.BackgroundImage")));
            this.pictureBox102.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox102.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox102.InitialImage")));
            this.pictureBox102.Location = new System.Drawing.Point(32, 503);
            this.pictureBox102.Name = "pictureBox102";
            this.pictureBox102.Size = new System.Drawing.Size(58, 48);
            this.pictureBox102.TabIndex = 144;
            this.pictureBox102.TabStop = false;
            this.pictureBox102.Visible = false;
            // 
            // pictureBox103
            // 
            this.pictureBox103.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox103.BackgroundImage")));
            this.pictureBox103.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox103.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox103.InitialImage")));
            this.pictureBox103.Location = new System.Drawing.Point(97, 503);
            this.pictureBox103.Name = "pictureBox103";
            this.pictureBox103.Size = new System.Drawing.Size(58, 48);
            this.pictureBox103.TabIndex = 143;
            this.pictureBox103.TabStop = false;
            this.pictureBox103.Visible = false;
            // 
            // pictureBox104
            // 
            this.pictureBox104.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox104.BackgroundImage")));
            this.pictureBox104.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox104.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox104.InitialImage")));
            this.pictureBox104.Location = new System.Drawing.Point(162, 503);
            this.pictureBox104.Name = "pictureBox104";
            this.pictureBox104.Size = new System.Drawing.Size(58, 48);
            this.pictureBox104.TabIndex = 142;
            this.pictureBox104.TabStop = false;
            this.pictureBox104.Visible = false;
            // 
            // pictureBox105
            // 
            this.pictureBox105.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox105.BackgroundImage")));
            this.pictureBox105.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox105.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox105.InitialImage")));
            this.pictureBox105.Location = new System.Drawing.Point(227, 503);
            this.pictureBox105.Name = "pictureBox105";
            this.pictureBox105.Size = new System.Drawing.Size(58, 48);
            this.pictureBox105.TabIndex = 141;
            this.pictureBox105.TabStop = false;
            this.pictureBox105.Visible = false;
            // 
            // pictureBox106
            // 
            this.pictureBox106.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox106.BackgroundImage")));
            this.pictureBox106.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox106.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox106.InitialImage")));
            this.pictureBox106.Location = new System.Drawing.Point(292, 503);
            this.pictureBox106.Name = "pictureBox106";
            this.pictureBox106.Size = new System.Drawing.Size(58, 48);
            this.pictureBox106.TabIndex = 140;
            this.pictureBox106.TabStop = false;
            this.pictureBox106.Visible = false;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label134.Location = new System.Drawing.Point(309, 521);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(25, 13);
            this.label134.TabIndex = 150;
            this.label134.Text = "500";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label135.Location = new System.Drawing.Point(243, 521);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(25, 13);
            this.label135.TabIndex = 149;
            this.label135.Text = "200";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label136.Location = new System.Drawing.Point(179, 521);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(25, 13);
            this.label136.TabIndex = 148;
            this.label136.Text = "150";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label137.Location = new System.Drawing.Point(113, 521);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(25, 13);
            this.label137.TabIndex = 147;
            this.label137.Text = "100";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label138.Location = new System.Drawing.Point(50, 521);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(19, 13);
            this.label138.TabIndex = 146;
            this.label138.Text = "50";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(293, 487);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(57, 13);
            this.label139.TabIndex = 145;
            this.label139.Text = "Completed";
            this.label139.Visible = false;
            // 
            // pictureBox107
            // 
            this.pictureBox107.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox107.BackgroundImage")));
            this.pictureBox107.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox107.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox107.InitialImage")));
            this.pictureBox107.Location = new System.Drawing.Point(32, 503);
            this.pictureBox107.Name = "pictureBox107";
            this.pictureBox107.Size = new System.Drawing.Size(58, 48);
            this.pictureBox107.TabIndex = 139;
            this.pictureBox107.TabStop = false;
            // 
            // pictureBox108
            // 
            this.pictureBox108.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox108.BackgroundImage")));
            this.pictureBox108.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox108.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox108.InitialImage")));
            this.pictureBox108.Location = new System.Drawing.Point(97, 503);
            this.pictureBox108.Name = "pictureBox108";
            this.pictureBox108.Size = new System.Drawing.Size(58, 48);
            this.pictureBox108.TabIndex = 138;
            this.pictureBox108.TabStop = false;
            // 
            // pictureBox109
            // 
            this.pictureBox109.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox109.BackgroundImage")));
            this.pictureBox109.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox109.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox109.InitialImage")));
            this.pictureBox109.Location = new System.Drawing.Point(162, 503);
            this.pictureBox109.Name = "pictureBox109";
            this.pictureBox109.Size = new System.Drawing.Size(58, 48);
            this.pictureBox109.TabIndex = 137;
            this.pictureBox109.TabStop = false;
            // 
            // pictureBox110
            // 
            this.pictureBox110.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox110.BackgroundImage")));
            this.pictureBox110.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox110.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox110.InitialImage")));
            this.pictureBox110.Location = new System.Drawing.Point(227, 503);
            this.pictureBox110.Name = "pictureBox110";
            this.pictureBox110.Size = new System.Drawing.Size(58, 48);
            this.pictureBox110.TabIndex = 136;
            this.pictureBox110.TabStop = false;
            // 
            // pictureBox111
            // 
            this.pictureBox111.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox111.BackgroundImage")));
            this.pictureBox111.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox111.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox111.InitialImage")));
            this.pictureBox111.Location = new System.Drawing.Point(292, 503);
            this.pictureBox111.Name = "pictureBox111";
            this.pictureBox111.Size = new System.Drawing.Size(58, 48);
            this.pictureBox111.TabIndex = 135;
            this.pictureBox111.TabStop = false;
            // 
            // pictureBox61
            // 
            this.pictureBox61.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox61.BackgroundImage")));
            this.pictureBox61.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox61.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox61.InitialImage")));
            this.pictureBox61.Location = new System.Drawing.Point(32, 424);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(48, 48);
            this.pictureBox61.TabIndex = 127;
            this.pictureBox61.TabStop = false;
            this.pictureBox61.Visible = false;
            // 
            // pictureBox63
            // 
            this.pictureBox63.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox63.BackgroundImage")));
            this.pictureBox63.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox63.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox63.InitialImage")));
            this.pictureBox63.Location = new System.Drawing.Point(86, 424);
            this.pictureBox63.Name = "pictureBox63";
            this.pictureBox63.Size = new System.Drawing.Size(48, 48);
            this.pictureBox63.TabIndex = 126;
            this.pictureBox63.TabStop = false;
            this.pictureBox63.Visible = false;
            // 
            // pictureBox80
            // 
            this.pictureBox80.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox80.BackgroundImage")));
            this.pictureBox80.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox80.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox80.InitialImage")));
            this.pictureBox80.Location = new System.Drawing.Point(140, 424);
            this.pictureBox80.Name = "pictureBox80";
            this.pictureBox80.Size = new System.Drawing.Size(48, 48);
            this.pictureBox80.TabIndex = 125;
            this.pictureBox80.TabStop = false;
            this.pictureBox80.Visible = false;
            // 
            // pictureBox81
            // 
            this.pictureBox81.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox81.BackgroundImage")));
            this.pictureBox81.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox81.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox81.InitialImage")));
            this.pictureBox81.Location = new System.Drawing.Point(194, 424);
            this.pictureBox81.Name = "pictureBox81";
            this.pictureBox81.Size = new System.Drawing.Size(48, 48);
            this.pictureBox81.TabIndex = 124;
            this.pictureBox81.TabStop = false;
            this.pictureBox81.Visible = false;
            // 
            // pictureBox82
            // 
            this.pictureBox82.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox82.BackgroundImage")));
            this.pictureBox82.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox82.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox82.InitialImage")));
            this.pictureBox82.Location = new System.Drawing.Point(248, 424);
            this.pictureBox82.Name = "pictureBox82";
            this.pictureBox82.Size = new System.Drawing.Size(48, 48);
            this.pictureBox82.TabIndex = 123;
            this.pictureBox82.TabStop = false;
            this.pictureBox82.Visible = false;
            // 
            // pictureBox89
            // 
            this.pictureBox89.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox89.BackgroundImage")));
            this.pictureBox89.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox89.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox89.InitialImage")));
            this.pictureBox89.Location = new System.Drawing.Point(302, 424);
            this.pictureBox89.Name = "pictureBox89";
            this.pictureBox89.Size = new System.Drawing.Size(48, 48);
            this.pictureBox89.TabIndex = 116;
            this.pictureBox89.TabStop = false;
            this.pictureBox89.Visible = false;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label118.Location = new System.Drawing.Point(314, 442);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(25, 13);
            this.label118.TabIndex = 133;
            this.label118.Text = "500";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label119.Location = new System.Drawing.Point(260, 442);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(25, 13);
            this.label119.TabIndex = 132;
            this.label119.Text = "150";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label120.Location = new System.Drawing.Point(206, 442);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(25, 13);
            this.label120.TabIndex = 131;
            this.label120.Text = "100";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label121.Location = new System.Drawing.Point(155, 442);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(19, 13);
            this.label121.TabIndex = 130;
            this.label121.Text = "50";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label122.Location = new System.Drawing.Point(101, 442);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(19, 13);
            this.label122.TabIndex = 129;
            this.label122.Text = "10";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label133.Location = new System.Drawing.Point(50, 442);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(13, 13);
            this.label133.TabIndex = 128;
            this.label133.Text = "0";
            // 
            // pictureBox90
            // 
            this.pictureBox90.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox90.BackgroundImage")));
            this.pictureBox90.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox90.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox90.InitialImage")));
            this.pictureBox90.Location = new System.Drawing.Point(32, 424);
            this.pictureBox90.Name = "pictureBox90";
            this.pictureBox90.Size = new System.Drawing.Size(48, 48);
            this.pictureBox90.TabIndex = 122;
            this.pictureBox90.TabStop = false;
            // 
            // pictureBox91
            // 
            this.pictureBox91.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox91.BackgroundImage")));
            this.pictureBox91.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox91.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox91.InitialImage")));
            this.pictureBox91.Location = new System.Drawing.Point(302, 424);
            this.pictureBox91.Name = "pictureBox91";
            this.pictureBox91.Size = new System.Drawing.Size(48, 48);
            this.pictureBox91.TabIndex = 121;
            this.pictureBox91.TabStop = false;
            // 
            // pictureBox92
            // 
            this.pictureBox92.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox92.BackgroundImage")));
            this.pictureBox92.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox92.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox92.InitialImage")));
            this.pictureBox92.Location = new System.Drawing.Point(86, 424);
            this.pictureBox92.Name = "pictureBox92";
            this.pictureBox92.Size = new System.Drawing.Size(48, 48);
            this.pictureBox92.TabIndex = 120;
            this.pictureBox92.TabStop = false;
            // 
            // pictureBox93
            // 
            this.pictureBox93.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox93.BackgroundImage")));
            this.pictureBox93.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox93.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox93.InitialImage")));
            this.pictureBox93.Location = new System.Drawing.Point(140, 424);
            this.pictureBox93.Name = "pictureBox93";
            this.pictureBox93.Size = new System.Drawing.Size(48, 48);
            this.pictureBox93.TabIndex = 119;
            this.pictureBox93.TabStop = false;
            // 
            // pictureBox100
            // 
            this.pictureBox100.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox100.BackgroundImage")));
            this.pictureBox100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox100.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox100.InitialImage")));
            this.pictureBox100.Location = new System.Drawing.Point(194, 424);
            this.pictureBox100.Name = "pictureBox100";
            this.pictureBox100.Size = new System.Drawing.Size(48, 48);
            this.pictureBox100.TabIndex = 118;
            this.pictureBox100.TabStop = false;
            // 
            // pictureBox101
            // 
            this.pictureBox101.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox101.BackgroundImage")));
            this.pictureBox101.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox101.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox101.InitialImage")));
            this.pictureBox101.Location = new System.Drawing.Point(248, 424);
            this.pictureBox101.Name = "pictureBox101";
            this.pictureBox101.Size = new System.Drawing.Size(48, 48);
            this.pictureBox101.TabIndex = 117;
            this.pictureBox101.TabStop = false;
            // 
            // pictureBox83
            // 
            this.pictureBox83.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox83.BackgroundImage")));
            this.pictureBox83.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox83.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox83.InitialImage")));
            this.pictureBox83.Location = new System.Drawing.Point(32, 345);
            this.pictureBox83.Name = "pictureBox83";
            this.pictureBox83.Size = new System.Drawing.Size(48, 48);
            this.pictureBox83.TabIndex = 96;
            this.pictureBox83.TabStop = false;
            this.pictureBox83.Visible = false;
            // 
            // pictureBox84
            // 
            this.pictureBox84.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox84.BackgroundImage")));
            this.pictureBox84.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox84.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox84.InitialImage")));
            this.pictureBox84.Location = new System.Drawing.Point(86, 345);
            this.pictureBox84.Name = "pictureBox84";
            this.pictureBox84.Size = new System.Drawing.Size(48, 48);
            this.pictureBox84.TabIndex = 95;
            this.pictureBox84.TabStop = false;
            this.pictureBox84.Visible = false;
            // 
            // pictureBox85
            // 
            this.pictureBox85.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox85.BackgroundImage")));
            this.pictureBox85.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox85.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox85.InitialImage")));
            this.pictureBox85.Location = new System.Drawing.Point(140, 345);
            this.pictureBox85.Name = "pictureBox85";
            this.pictureBox85.Size = new System.Drawing.Size(48, 48);
            this.pictureBox85.TabIndex = 94;
            this.pictureBox85.TabStop = false;
            this.pictureBox85.Visible = false;
            // 
            // pictureBox86
            // 
            this.pictureBox86.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox86.BackgroundImage")));
            this.pictureBox86.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox86.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox86.InitialImage")));
            this.pictureBox86.Location = new System.Drawing.Point(194, 345);
            this.pictureBox86.Name = "pictureBox86";
            this.pictureBox86.Size = new System.Drawing.Size(48, 48);
            this.pictureBox86.TabIndex = 93;
            this.pictureBox86.TabStop = false;
            this.pictureBox86.Visible = false;
            // 
            // pictureBox87
            // 
            this.pictureBox87.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox87.BackgroundImage")));
            this.pictureBox87.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox87.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox87.InitialImage")));
            this.pictureBox87.Location = new System.Drawing.Point(248, 345);
            this.pictureBox87.Name = "pictureBox87";
            this.pictureBox87.Size = new System.Drawing.Size(48, 48);
            this.pictureBox87.TabIndex = 92;
            this.pictureBox87.TabStop = false;
            this.pictureBox87.Visible = false;
            // 
            // pictureBox88
            // 
            this.pictureBox88.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox88.BackgroundImage")));
            this.pictureBox88.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox88.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox88.InitialImage")));
            this.pictureBox88.Location = new System.Drawing.Point(302, 345);
            this.pictureBox88.Name = "pictureBox88";
            this.pictureBox88.Size = new System.Drawing.Size(48, 48);
            this.pictureBox88.TabIndex = 85;
            this.pictureBox88.TabStop = false;
            this.pictureBox88.Visible = false;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label123.Location = new System.Drawing.Point(311, 363);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(31, 13);
            this.label123.TabIndex = 115;
            this.label123.Text = "1000";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label124.Location = new System.Drawing.Point(260, 363);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(25, 13);
            this.label124.TabIndex = 114;
            this.label124.Text = "500";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label125.Location = new System.Drawing.Point(206, 363);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(25, 13);
            this.label125.TabIndex = 113;
            this.label125.Text = "200";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label126.Location = new System.Drawing.Point(155, 363);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(19, 13);
            this.label126.TabIndex = 112;
            this.label126.Text = "50";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label127.Location = new System.Drawing.Point(101, 363);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(19, 13);
            this.label127.TabIndex = 111;
            this.label127.Text = "30";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label128.Location = new System.Drawing.Point(50, 363);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(13, 13);
            this.label128.TabIndex = 110;
            this.label128.Text = "0";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(293, 408);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(57, 13);
            this.label129.TabIndex = 109;
            this.label129.Text = "Completed";
            this.label129.Visible = false;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(293, 329);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(57, 13);
            this.label130.TabIndex = 108;
            this.label130.Text = "Completed";
            this.label130.Visible = false;
            // 
            // pictureBox94
            // 
            this.pictureBox94.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox94.BackgroundImage")));
            this.pictureBox94.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox94.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox94.InitialImage")));
            this.pictureBox94.Location = new System.Drawing.Point(32, 345);
            this.pictureBox94.Name = "pictureBox94";
            this.pictureBox94.Size = new System.Drawing.Size(48, 48);
            this.pictureBox94.TabIndex = 91;
            this.pictureBox94.TabStop = false;
            // 
            // pictureBox95
            // 
            this.pictureBox95.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox95.BackgroundImage")));
            this.pictureBox95.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox95.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox95.InitialImage")));
            this.pictureBox95.Location = new System.Drawing.Point(302, 345);
            this.pictureBox95.Name = "pictureBox95";
            this.pictureBox95.Size = new System.Drawing.Size(48, 48);
            this.pictureBox95.TabIndex = 90;
            this.pictureBox95.TabStop = false;
            // 
            // pictureBox96
            // 
            this.pictureBox96.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox96.BackgroundImage")));
            this.pictureBox96.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox96.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox96.InitialImage")));
            this.pictureBox96.Location = new System.Drawing.Point(86, 345);
            this.pictureBox96.Name = "pictureBox96";
            this.pictureBox96.Size = new System.Drawing.Size(48, 48);
            this.pictureBox96.TabIndex = 89;
            this.pictureBox96.TabStop = false;
            // 
            // pictureBox97
            // 
            this.pictureBox97.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox97.BackgroundImage")));
            this.pictureBox97.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox97.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox97.InitialImage")));
            this.pictureBox97.Location = new System.Drawing.Point(140, 345);
            this.pictureBox97.Name = "pictureBox97";
            this.pictureBox97.Size = new System.Drawing.Size(48, 48);
            this.pictureBox97.TabIndex = 88;
            this.pictureBox97.TabStop = false;
            // 
            // pictureBox98
            // 
            this.pictureBox98.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox98.BackgroundImage")));
            this.pictureBox98.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox98.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox98.InitialImage")));
            this.pictureBox98.Location = new System.Drawing.Point(194, 345);
            this.pictureBox98.Name = "pictureBox98";
            this.pictureBox98.Size = new System.Drawing.Size(48, 48);
            this.pictureBox98.TabIndex = 87;
            this.pictureBox98.TabStop = false;
            // 
            // pictureBox99
            // 
            this.pictureBox99.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox99.BackgroundImage")));
            this.pictureBox99.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox99.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox99.InitialImage")));
            this.pictureBox99.Location = new System.Drawing.Point(248, 345);
            this.pictureBox99.Name = "pictureBox99";
            this.pictureBox99.Size = new System.Drawing.Size(48, 48);
            this.pictureBox99.TabIndex = 86;
            this.pictureBox99.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pictureBox56);
            this.tabPage3.Controls.Add(this.pictureBox57);
            this.tabPage3.Controls.Add(this.pictureBox58);
            this.tabPage3.Controls.Add(this.pictureBox59);
            this.tabPage3.Controls.Add(this.pictureBox60);
            this.tabPage3.Controls.Add(this.label93);
            this.tabPage3.Controls.Add(this.label92);
            this.tabPage3.Controls.Add(this.label91);
            this.tabPage3.Controls.Add(this.label90);
            this.tabPage3.Controls.Add(this.label89);
            this.tabPage3.Controls.Add(this.pictureBox68);
            this.tabPage3.Controls.Add(this.pictureBox69);
            this.tabPage3.Controls.Add(this.pictureBox70);
            this.tabPage3.Controls.Add(this.pictureBox71);
            this.tabPage3.Controls.Add(this.pictureBox72);
            this.tabPage3.Controls.Add(this.pictureBox73);
            this.tabPage3.Controls.Add(this.label88);
            this.tabPage3.Controls.Add(this.label87);
            this.tabPage3.Controls.Add(this.label86);
            this.tabPage3.Controls.Add(this.label85);
            this.tabPage3.Controls.Add(this.label84);
            this.tabPage3.Controls.Add(this.label83);
            this.tabPage3.Controls.Add(this.label79);
            this.tabPage3.Controls.Add(this.label80);
            this.tabPage3.Controls.Add(this.pictureBox62);
            this.tabPage3.Controls.Add(this.pictureBox64);
            this.tabPage3.Controls.Add(this.pictureBox65);
            this.tabPage3.Controls.Add(this.pictureBox66);
            this.tabPage3.Controls.Add(this.pictureBox67);
            this.tabPage3.Controls.Add(this.label81);
            this.tabPage3.Controls.Add(this.pictureBox74);
            this.tabPage3.Controls.Add(this.pictureBox75);
            this.tabPage3.Controls.Add(this.pictureBox76);
            this.tabPage3.Controls.Add(this.pictureBox77);
            this.tabPage3.Controls.Add(this.pictureBox78);
            this.tabPage3.Controls.Add(this.pictureBox79);
            this.tabPage3.Controls.Add(this.label82);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(560, 597);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Light Assault";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pictureBox56
            // 
            this.pictureBox56.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox56.BackgroundImage")));
            this.pictureBox56.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox56.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox56.InitialImage")));
            this.pictureBox56.Location = new System.Drawing.Point(32, 503);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(58, 48);
            this.pictureBox56.TabIndex = 70;
            this.pictureBox56.TabStop = false;
            this.pictureBox56.Visible = false;
            // 
            // pictureBox57
            // 
            this.pictureBox57.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox57.BackgroundImage")));
            this.pictureBox57.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox57.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox57.InitialImage")));
            this.pictureBox57.Location = new System.Drawing.Point(97, 503);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(58, 48);
            this.pictureBox57.TabIndex = 69;
            this.pictureBox57.TabStop = false;
            this.pictureBox57.Visible = false;
            // 
            // pictureBox58
            // 
            this.pictureBox58.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox58.BackgroundImage")));
            this.pictureBox58.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox58.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox58.InitialImage")));
            this.pictureBox58.Location = new System.Drawing.Point(162, 503);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(58, 48);
            this.pictureBox58.TabIndex = 68;
            this.pictureBox58.TabStop = false;
            this.pictureBox58.Visible = false;
            // 
            // pictureBox59
            // 
            this.pictureBox59.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox59.BackgroundImage")));
            this.pictureBox59.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox59.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox59.InitialImage")));
            this.pictureBox59.Location = new System.Drawing.Point(227, 503);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(58, 48);
            this.pictureBox59.TabIndex = 67;
            this.pictureBox59.TabStop = false;
            this.pictureBox59.Visible = false;
            // 
            // pictureBox60
            // 
            this.pictureBox60.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox60.BackgroundImage")));
            this.pictureBox60.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox60.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox60.InitialImage")));
            this.pictureBox60.Location = new System.Drawing.Point(292, 503);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(58, 48);
            this.pictureBox60.TabIndex = 66;
            this.pictureBox60.TabStop = false;
            this.pictureBox60.Visible = false;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label93.Location = new System.Drawing.Point(309, 521);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(25, 13);
            this.label93.TabIndex = 83;
            this.label93.Text = "500";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label92.Location = new System.Drawing.Point(243, 521);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(25, 13);
            this.label92.TabIndex = 82;
            this.label92.Text = "200";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label91.Location = new System.Drawing.Point(179, 521);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(25, 13);
            this.label91.TabIndex = 81;
            this.label91.Text = "150";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label90.Location = new System.Drawing.Point(113, 521);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(25, 13);
            this.label90.TabIndex = 80;
            this.label90.Text = "100";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label89.Location = new System.Drawing.Point(50, 521);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(19, 13);
            this.label89.TabIndex = 79;
            this.label89.Text = "50";
            // 
            // pictureBox68
            // 
            this.pictureBox68.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox68.BackgroundImage")));
            this.pictureBox68.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox68.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox68.InitialImage")));
            this.pictureBox68.Location = new System.Drawing.Point(32, 424);
            this.pictureBox68.Name = "pictureBox68";
            this.pictureBox68.Size = new System.Drawing.Size(48, 48);
            this.pictureBox68.TabIndex = 57;
            this.pictureBox68.TabStop = false;
            this.pictureBox68.Visible = false;
            // 
            // pictureBox69
            // 
            this.pictureBox69.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox69.BackgroundImage")));
            this.pictureBox69.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox69.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox69.InitialImage")));
            this.pictureBox69.Location = new System.Drawing.Point(86, 424);
            this.pictureBox69.Name = "pictureBox69";
            this.pictureBox69.Size = new System.Drawing.Size(48, 48);
            this.pictureBox69.TabIndex = 56;
            this.pictureBox69.TabStop = false;
            this.pictureBox69.Visible = false;
            // 
            // pictureBox70
            // 
            this.pictureBox70.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox70.BackgroundImage")));
            this.pictureBox70.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox70.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox70.InitialImage")));
            this.pictureBox70.Location = new System.Drawing.Point(140, 424);
            this.pictureBox70.Name = "pictureBox70";
            this.pictureBox70.Size = new System.Drawing.Size(48, 48);
            this.pictureBox70.TabIndex = 55;
            this.pictureBox70.TabStop = false;
            this.pictureBox70.Visible = false;
            // 
            // pictureBox71
            // 
            this.pictureBox71.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox71.BackgroundImage")));
            this.pictureBox71.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox71.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox71.InitialImage")));
            this.pictureBox71.Location = new System.Drawing.Point(194, 424);
            this.pictureBox71.Name = "pictureBox71";
            this.pictureBox71.Size = new System.Drawing.Size(48, 48);
            this.pictureBox71.TabIndex = 54;
            this.pictureBox71.TabStop = false;
            this.pictureBox71.Visible = false;
            // 
            // pictureBox72
            // 
            this.pictureBox72.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox72.BackgroundImage")));
            this.pictureBox72.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox72.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox72.InitialImage")));
            this.pictureBox72.Location = new System.Drawing.Point(248, 424);
            this.pictureBox72.Name = "pictureBox72";
            this.pictureBox72.Size = new System.Drawing.Size(48, 48);
            this.pictureBox72.TabIndex = 53;
            this.pictureBox72.TabStop = false;
            this.pictureBox72.Visible = false;
            // 
            // pictureBox73
            // 
            this.pictureBox73.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox73.BackgroundImage")));
            this.pictureBox73.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox73.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox73.InitialImage")));
            this.pictureBox73.Location = new System.Drawing.Point(302, 424);
            this.pictureBox73.Name = "pictureBox73";
            this.pictureBox73.Size = new System.Drawing.Size(48, 48);
            this.pictureBox73.TabIndex = 46;
            this.pictureBox73.TabStop = false;
            this.pictureBox73.Visible = false;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label88.Location = new System.Drawing.Point(314, 442);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(25, 13);
            this.label88.TabIndex = 78;
            this.label88.Text = "500";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label87.Location = new System.Drawing.Point(260, 442);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(25, 13);
            this.label87.TabIndex = 77;
            this.label87.Text = "150";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label86.Location = new System.Drawing.Point(206, 442);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(25, 13);
            this.label86.TabIndex = 76;
            this.label86.Text = "100";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label85.Location = new System.Drawing.Point(155, 442);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(19, 13);
            this.label85.TabIndex = 75;
            this.label85.Text = "50";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label84.Location = new System.Drawing.Point(101, 442);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(19, 13);
            this.label84.TabIndex = 74;
            this.label84.Text = "10";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label83.Location = new System.Drawing.Point(50, 442);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(13, 13);
            this.label83.TabIndex = 73;
            this.label83.Text = "0";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(293, 487);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(57, 13);
            this.label79.TabIndex = 72;
            this.label79.Text = "Completed";
            this.label79.Visible = false;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(293, 408);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(57, 13);
            this.label80.TabIndex = 71;
            this.label80.Text = "Completed";
            this.label80.Visible = false;
            // 
            // pictureBox62
            // 
            this.pictureBox62.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox62.BackgroundImage")));
            this.pictureBox62.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox62.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox62.InitialImage")));
            this.pictureBox62.Location = new System.Drawing.Point(32, 503);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(58, 48);
            this.pictureBox62.TabIndex = 65;
            this.pictureBox62.TabStop = false;
            // 
            // pictureBox64
            // 
            this.pictureBox64.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox64.BackgroundImage")));
            this.pictureBox64.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox64.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox64.InitialImage")));
            this.pictureBox64.Location = new System.Drawing.Point(97, 503);
            this.pictureBox64.Name = "pictureBox64";
            this.pictureBox64.Size = new System.Drawing.Size(58, 48);
            this.pictureBox64.TabIndex = 63;
            this.pictureBox64.TabStop = false;
            // 
            // pictureBox65
            // 
            this.pictureBox65.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox65.BackgroundImage")));
            this.pictureBox65.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox65.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox65.InitialImage")));
            this.pictureBox65.Location = new System.Drawing.Point(162, 503);
            this.pictureBox65.Name = "pictureBox65";
            this.pictureBox65.Size = new System.Drawing.Size(58, 48);
            this.pictureBox65.TabIndex = 62;
            this.pictureBox65.TabStop = false;
            // 
            // pictureBox66
            // 
            this.pictureBox66.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox66.BackgroundImage")));
            this.pictureBox66.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox66.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox66.InitialImage")));
            this.pictureBox66.Location = new System.Drawing.Point(227, 503);
            this.pictureBox66.Name = "pictureBox66";
            this.pictureBox66.Size = new System.Drawing.Size(58, 48);
            this.pictureBox66.TabIndex = 61;
            this.pictureBox66.TabStop = false;
            // 
            // pictureBox67
            // 
            this.pictureBox67.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox67.BackgroundImage")));
            this.pictureBox67.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox67.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox67.InitialImage")));
            this.pictureBox67.Location = new System.Drawing.Point(292, 503);
            this.pictureBox67.Name = "pictureBox67";
            this.pictureBox67.Size = new System.Drawing.Size(58, 48);
            this.pictureBox67.TabIndex = 60;
            this.pictureBox67.TabStop = false;
            // 
            // pictureBox74
            // 
            this.pictureBox74.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox74.BackgroundImage")));
            this.pictureBox74.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox74.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox74.InitialImage")));
            this.pictureBox74.Location = new System.Drawing.Point(32, 424);
            this.pictureBox74.Name = "pictureBox74";
            this.pictureBox74.Size = new System.Drawing.Size(48, 48);
            this.pictureBox74.TabIndex = 52;
            this.pictureBox74.TabStop = false;
            // 
            // pictureBox75
            // 
            this.pictureBox75.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox75.BackgroundImage")));
            this.pictureBox75.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox75.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox75.InitialImage")));
            this.pictureBox75.Location = new System.Drawing.Point(302, 424);
            this.pictureBox75.Name = "pictureBox75";
            this.pictureBox75.Size = new System.Drawing.Size(48, 48);
            this.pictureBox75.TabIndex = 51;
            this.pictureBox75.TabStop = false;
            // 
            // pictureBox76
            // 
            this.pictureBox76.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox76.BackgroundImage")));
            this.pictureBox76.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox76.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox76.InitialImage")));
            this.pictureBox76.Location = new System.Drawing.Point(86, 424);
            this.pictureBox76.Name = "pictureBox76";
            this.pictureBox76.Size = new System.Drawing.Size(48, 48);
            this.pictureBox76.TabIndex = 50;
            this.pictureBox76.TabStop = false;
            // 
            // pictureBox77
            // 
            this.pictureBox77.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox77.BackgroundImage")));
            this.pictureBox77.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox77.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox77.InitialImage")));
            this.pictureBox77.Location = new System.Drawing.Point(140, 424);
            this.pictureBox77.Name = "pictureBox77";
            this.pictureBox77.Size = new System.Drawing.Size(48, 48);
            this.pictureBox77.TabIndex = 49;
            this.pictureBox77.TabStop = false;
            // 
            // pictureBox78
            // 
            this.pictureBox78.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox78.BackgroundImage")));
            this.pictureBox78.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox78.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox78.InitialImage")));
            this.pictureBox78.Location = new System.Drawing.Point(194, 424);
            this.pictureBox78.Name = "pictureBox78";
            this.pictureBox78.Size = new System.Drawing.Size(48, 48);
            this.pictureBox78.TabIndex = 48;
            this.pictureBox78.TabStop = false;
            // 
            // pictureBox79
            // 
            this.pictureBox79.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox79.BackgroundImage")));
            this.pictureBox79.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox79.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox79.InitialImage")));
            this.pictureBox79.Location = new System.Drawing.Point(248, 424);
            this.pictureBox79.Name = "pictureBox79";
            this.pictureBox79.Size = new System.Drawing.Size(48, 48);
            this.pictureBox79.TabIndex = 47;
            this.pictureBox79.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.pictureBox32);
            this.tabPage4.Controls.Add(this.pictureBox33);
            this.tabPage4.Controls.Add(this.pictureBox34);
            this.tabPage4.Controls.Add(this.pictureBox35);
            this.tabPage4.Controls.Add(this.pictureBox36);
            this.tabPage4.Controls.Add(this.pictureBox37);
            this.tabPage4.Controls.Add(this.label105);
            this.tabPage4.Controls.Add(this.label104);
            this.tabPage4.Controls.Add(this.label103);
            this.tabPage4.Controls.Add(this.label102);
            this.tabPage4.Controls.Add(this.label101);
            this.tabPage4.Controls.Add(this.label100);
            this.tabPage4.Controls.Add(this.pictureBox18);
            this.tabPage4.Controls.Add(this.pictureBox17);
            this.tabPage4.Controls.Add(this.pictureBox16);
            this.tabPage4.Controls.Add(this.pictureBox15);
            this.tabPage4.Controls.Add(this.pictureBox14);
            this.tabPage4.Controls.Add(this.pictureBox7);
            this.tabPage4.Controls.Add(this.label99);
            this.tabPage4.Controls.Add(this.label98);
            this.tabPage4.Controls.Add(this.label97);
            this.tabPage4.Controls.Add(this.label96);
            this.tabPage4.Controls.Add(this.label95);
            this.tabPage4.Controls.Add(this.label94);
            this.tabPage4.Controls.Add(this.label77);
            this.tabPage4.Controls.Add(this.label78);
            this.tabPage4.Controls.Add(this.pictureBox38);
            this.tabPage4.Controls.Add(this.pictureBox39);
            this.tabPage4.Controls.Add(this.pictureBox40);
            this.tabPage4.Controls.Add(this.pictureBox41);
            this.tabPage4.Controls.Add(this.pictureBox42);
            this.tabPage4.Controls.Add(this.pictureBox43);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.pictureBox13);
            this.tabPage4.Controls.Add(this.pictureBox12);
            this.tabPage4.Controls.Add(this.pictureBox11);
            this.tabPage4.Controls.Add(this.pictureBox10);
            this.tabPage4.Controls.Add(this.pictureBox9);
            this.tabPage4.Controls.Add(this.pictureBox8);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(560, 597);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Combat Medic";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox32.BackgroundImage")));
            this.pictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox32.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox32.InitialImage")));
            this.pictureBox32.Location = new System.Drawing.Point(32, 503);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(48, 48);
            this.pictureBox32.TabIndex = 27;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.Visible = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox33.BackgroundImage")));
            this.pictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox33.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox33.InitialImage")));
            this.pictureBox33.Location = new System.Drawing.Point(86, 503);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(48, 48);
            this.pictureBox33.TabIndex = 26;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.Visible = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox34.BackgroundImage")));
            this.pictureBox34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox34.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox34.InitialImage")));
            this.pictureBox34.Location = new System.Drawing.Point(140, 503);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(48, 48);
            this.pictureBox34.TabIndex = 25;
            this.pictureBox34.TabStop = false;
            this.pictureBox34.Visible = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox35.BackgroundImage")));
            this.pictureBox35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox35.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox35.InitialImage")));
            this.pictureBox35.Location = new System.Drawing.Point(194, 503);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(48, 48);
            this.pictureBox35.TabIndex = 24;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.Visible = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox36.BackgroundImage")));
            this.pictureBox36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox36.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox36.InitialImage")));
            this.pictureBox36.Location = new System.Drawing.Point(248, 503);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(48, 48);
            this.pictureBox36.TabIndex = 23;
            this.pictureBox36.TabStop = false;
            this.pictureBox36.Visible = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox37.BackgroundImage")));
            this.pictureBox37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox37.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox37.InitialImage")));
            this.pictureBox37.Location = new System.Drawing.Point(302, 503);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(48, 48);
            this.pictureBox37.TabIndex = 16;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.Visible = false;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label105.Location = new System.Drawing.Point(314, 521);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(25, 13);
            this.label105.TabIndex = 56;
            this.label105.Text = "200";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label104.Location = new System.Drawing.Point(262, 521);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(19, 13);
            this.label104.TabIndex = 55;
            this.label104.Text = "50";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label103.Location = new System.Drawing.Point(208, 521);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(19, 13);
            this.label103.TabIndex = 54;
            this.label103.Text = "30";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label102.Location = new System.Drawing.Point(155, 521);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(19, 13);
            this.label102.TabIndex = 53;
            this.label102.Text = "10";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label101.Location = new System.Drawing.Point(103, 521);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(13, 13);
            this.label101.TabIndex = 52;
            this.label101.Text = "1";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label100.Location = new System.Drawing.Point(50, 521);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(13, 13);
            this.label100.TabIndex = 51;
            this.label100.Text = "0";
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox18.BackgroundImage")));
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox18.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox18.InitialImage")));
            this.pictureBox18.Location = new System.Drawing.Point(302, 424);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(48, 48);
            this.pictureBox18.TabIndex = 14;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Visible = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox17.BackgroundImage")));
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox17.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox17.InitialImage")));
            this.pictureBox17.Location = new System.Drawing.Point(248, 424);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(48, 48);
            this.pictureBox17.TabIndex = 13;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Visible = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox16.BackgroundImage")));
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox16.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox16.InitialImage")));
            this.pictureBox16.Location = new System.Drawing.Point(194, 424);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(48, 48);
            this.pictureBox16.TabIndex = 12;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Visible = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox15.BackgroundImage")));
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox15.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox15.InitialImage")));
            this.pictureBox15.Location = new System.Drawing.Point(140, 424);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(48, 48);
            this.pictureBox15.TabIndex = 11;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Visible = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox14.BackgroundImage")));
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox14.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox14.InitialImage")));
            this.pictureBox14.Location = new System.Drawing.Point(86, 424);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(48, 48);
            this.pictureBox14.TabIndex = 10;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Visible = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.InitialImage")));
            this.pictureBox7.Location = new System.Drawing.Point(32, 424);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(48, 48);
            this.pictureBox7.TabIndex = 1;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Visible = false;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label99.Location = new System.Drawing.Point(314, 442);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(25, 13);
            this.label99.TabIndex = 50;
            this.label99.Text = "500";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label98.Location = new System.Drawing.Point(260, 442);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(25, 13);
            this.label98.TabIndex = 49;
            this.label98.Text = "100";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label97.Location = new System.Drawing.Point(208, 442);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(19, 13);
            this.label97.TabIndex = 48;
            this.label97.Text = "50";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label96.Location = new System.Drawing.Point(155, 442);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(19, 13);
            this.label96.TabIndex = 47;
            this.label96.Text = "30";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label95.Location = new System.Drawing.Point(101, 442);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(19, 13);
            this.label95.TabIndex = 46;
            this.label95.Text = "10";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label94.Location = new System.Drawing.Point(50, 442);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(13, 13);
            this.label94.TabIndex = 45;
            this.label94.Text = "0";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(293, 487);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(57, 13);
            this.label77.TabIndex = 44;
            this.label77.Text = "Completed";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(293, 408);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(57, 13);
            this.label78.TabIndex = 43;
            this.label78.Text = "Completed";
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox38.BackgroundImage")));
            this.pictureBox38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox38.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox38.InitialImage")));
            this.pictureBox38.Location = new System.Drawing.Point(32, 503);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(48, 48);
            this.pictureBox38.TabIndex = 22;
            this.pictureBox38.TabStop = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox39.BackgroundImage")));
            this.pictureBox39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox39.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox39.InitialImage")));
            this.pictureBox39.Location = new System.Drawing.Point(302, 503);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(48, 48);
            this.pictureBox39.TabIndex = 21;
            this.pictureBox39.TabStop = false;
            // 
            // pictureBox40
            // 
            this.pictureBox40.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox40.BackgroundImage")));
            this.pictureBox40.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox40.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox40.InitialImage")));
            this.pictureBox40.Location = new System.Drawing.Point(86, 503);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(48, 48);
            this.pictureBox40.TabIndex = 20;
            this.pictureBox40.TabStop = false;
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox41.BackgroundImage")));
            this.pictureBox41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox41.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox41.InitialImage")));
            this.pictureBox41.Location = new System.Drawing.Point(140, 503);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(48, 48);
            this.pictureBox41.TabIndex = 19;
            this.pictureBox41.TabStop = false;
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox42.BackgroundImage")));
            this.pictureBox42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox42.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox42.InitialImage")));
            this.pictureBox42.Location = new System.Drawing.Point(194, 503);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(48, 48);
            this.pictureBox42.TabIndex = 18;
            this.pictureBox42.TabStop = false;
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox43.BackgroundImage")));
            this.pictureBox43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox43.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox43.InitialImage")));
            this.pictureBox43.Location = new System.Drawing.Point(248, 503);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(48, 48);
            this.pictureBox43.TabIndex = 17;
            this.pictureBox43.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox13.BackgroundImage")));
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox13.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox13.InitialImage")));
            this.pictureBox13.Location = new System.Drawing.Point(32, 424);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(48, 48);
            this.pictureBox13.TabIndex = 9;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox12.BackgroundImage")));
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox12.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox12.InitialImage")));
            this.pictureBox12.Location = new System.Drawing.Point(302, 424);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(48, 48);
            this.pictureBox12.TabIndex = 8;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.BackgroundImage")));
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox11.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.InitialImage")));
            this.pictureBox11.Location = new System.Drawing.Point(86, 424);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(48, 48);
            this.pictureBox11.TabIndex = 5;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.InitialImage")));
            this.pictureBox10.Location = new System.Drawing.Point(140, 424);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(48, 48);
            this.pictureBox10.TabIndex = 4;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox9.BackgroundImage")));
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox9.InitialImage")));
            this.pictureBox9.Location = new System.Drawing.Point(194, 424);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(48, 48);
            this.pictureBox9.TabIndex = 3;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.InitialImage")));
            this.pictureBox8.Location = new System.Drawing.Point(248, 424);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(48, 48);
            this.pictureBox8.TabIndex = 2;
            this.pictureBox8.TabStop = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.pictureBox19);
            this.tabPage5.Controls.Add(this.pictureBox20);
            this.tabPage5.Controls.Add(this.pictureBox21);
            this.tabPage5.Controls.Add(this.pictureBox22);
            this.tabPage5.Controls.Add(this.pictureBox23);
            this.tabPage5.Controls.Add(this.pictureBox24);
            this.tabPage5.Controls.Add(this.label117);
            this.tabPage5.Controls.Add(this.label116);
            this.tabPage5.Controls.Add(this.label115);
            this.tabPage5.Controls.Add(this.label114);
            this.tabPage5.Controls.Add(this.label113);
            this.tabPage5.Controls.Add(this.pictureBox44);
            this.tabPage5.Controls.Add(this.pictureBox45);
            this.tabPage5.Controls.Add(this.pictureBox46);
            this.tabPage5.Controls.Add(this.pictureBox47);
            this.tabPage5.Controls.Add(this.pictureBox48);
            this.tabPage5.Controls.Add(this.pictureBox49);
            this.tabPage5.Controls.Add(this.label111);
            this.tabPage5.Controls.Add(this.label110);
            this.tabPage5.Controls.Add(this.label109);
            this.tabPage5.Controls.Add(this.label108);
            this.tabPage5.Controls.Add(this.label107);
            this.tabPage5.Controls.Add(this.label112);
            this.tabPage5.Controls.Add(this.label106);
            this.tabPage5.Controls.Add(this.label76);
            this.tabPage5.Controls.Add(this.label75);
            this.tabPage5.Controls.Add(this.label74);
            this.tabPage5.Controls.Add(this.pictureBox50);
            this.tabPage5.Controls.Add(this.pictureBox51);
            this.tabPage5.Controls.Add(this.pictureBox52);
            this.tabPage5.Controls.Add(this.pictureBox53);
            this.tabPage5.Controls.Add(this.pictureBox54);
            this.tabPage5.Controls.Add(this.pictureBox55);
            this.tabPage5.Controls.Add(this.label73);
            this.tabPage5.Controls.Add(this.pictureBox25);
            this.tabPage5.Controls.Add(this.pictureBox26);
            this.tabPage5.Controls.Add(this.pictureBox27);
            this.tabPage5.Controls.Add(this.pictureBox28);
            this.tabPage5.Controls.Add(this.pictureBox29);
            this.tabPage5.Controls.Add(this.pictureBox30);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(560, 597);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Engineer";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox19.BackgroundImage")));
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox19.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox19.InitialImage")));
            this.pictureBox19.Location = new System.Drawing.Point(32, 503);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(48, 48);
            this.pictureBox19.TabIndex = 26;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Visible = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox20.BackgroundImage")));
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox20.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox20.InitialImage")));
            this.pictureBox20.Location = new System.Drawing.Point(86, 503);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(48, 48);
            this.pictureBox20.TabIndex = 25;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Visible = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox21.BackgroundImage")));
            this.pictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox21.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox21.InitialImage")));
            this.pictureBox21.Location = new System.Drawing.Point(140, 503);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(48, 48);
            this.pictureBox21.TabIndex = 24;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Visible = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox22.BackgroundImage")));
            this.pictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox22.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox22.InitialImage")));
            this.pictureBox22.Location = new System.Drawing.Point(194, 503);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(48, 48);
            this.pictureBox22.TabIndex = 23;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Visible = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox23.BackgroundImage")));
            this.pictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox23.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox23.InitialImage")));
            this.pictureBox23.Location = new System.Drawing.Point(248, 503);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(48, 48);
            this.pictureBox23.TabIndex = 22;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Visible = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox24.BackgroundImage")));
            this.pictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox24.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox24.InitialImage")));
            this.pictureBox24.Location = new System.Drawing.Point(302, 503);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(48, 48);
            this.pictureBox24.TabIndex = 15;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Visible = false;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label117.Location = new System.Drawing.Point(314, 521);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(25, 13);
            this.label117.TabIndex = 53;
            this.label117.Text = "500";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label116.Location = new System.Drawing.Point(260, 521);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(25, 13);
            this.label116.TabIndex = 52;
            this.label116.Text = "150";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label115.Location = new System.Drawing.Point(209, 521);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(19, 13);
            this.label115.TabIndex = 51;
            this.label115.Text = "50";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label114.Location = new System.Drawing.Point(154, 521);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(19, 13);
            this.label114.TabIndex = 50;
            this.label114.Text = "30";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label113.Location = new System.Drawing.Point(100, 521);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(19, 13);
            this.label113.TabIndex = 49;
            this.label113.Text = "10";
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox44.BackgroundImage")));
            this.pictureBox44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox44.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox44.InitialImage")));
            this.pictureBox44.Location = new System.Drawing.Point(32, 424);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(48, 48);
            this.pictureBox44.TabIndex = 39;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.Visible = false;
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox45.BackgroundImage")));
            this.pictureBox45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox45.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox45.InitialImage")));
            this.pictureBox45.Location = new System.Drawing.Point(86, 424);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(48, 48);
            this.pictureBox45.TabIndex = 38;
            this.pictureBox45.TabStop = false;
            this.pictureBox45.Visible = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox46.BackgroundImage")));
            this.pictureBox46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox46.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox46.InitialImage")));
            this.pictureBox46.Location = new System.Drawing.Point(140, 424);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(48, 48);
            this.pictureBox46.TabIndex = 37;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.Visible = false;
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox47.BackgroundImage")));
            this.pictureBox47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox47.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox47.InitialImage")));
            this.pictureBox47.Location = new System.Drawing.Point(194, 424);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(48, 48);
            this.pictureBox47.TabIndex = 36;
            this.pictureBox47.TabStop = false;
            this.pictureBox47.Visible = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox48.BackgroundImage")));
            this.pictureBox48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox48.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox48.InitialImage")));
            this.pictureBox48.Location = new System.Drawing.Point(248, 424);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(48, 48);
            this.pictureBox48.TabIndex = 35;
            this.pictureBox48.TabStop = false;
            this.pictureBox48.Visible = false;
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox49.BackgroundImage")));
            this.pictureBox49.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox49.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox49.InitialImage")));
            this.pictureBox49.Location = new System.Drawing.Point(302, 424);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(48, 48);
            this.pictureBox49.TabIndex = 28;
            this.pictureBox49.TabStop = false;
            this.pictureBox49.Visible = false;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label111.Location = new System.Drawing.Point(314, 442);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(25, 13);
            this.label111.TabIndex = 48;
            this.label111.Text = "300";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label110.Location = new System.Drawing.Point(260, 442);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(25, 13);
            this.label110.TabIndex = 47;
            this.label110.Text = "200";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label109.Location = new System.Drawing.Point(205, 442);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(25, 13);
            this.label109.TabIndex = 46;
            this.label109.Text = "150";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label108.Location = new System.Drawing.Point(152, 442);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(25, 13);
            this.label108.TabIndex = 45;
            this.label108.Text = "100";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label107.Location = new System.Drawing.Point(101, 442);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(19, 13);
            this.label107.TabIndex = 44;
            this.label107.Text = "60";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label112.Location = new System.Drawing.Point(50, 521);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(13, 13);
            this.label112.TabIndex = 43;
            this.label112.Text = "0";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label106.Location = new System.Drawing.Point(50, 441);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(13, 13);
            this.label106.TabIndex = 43;
            this.label106.Text = "0";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(293, 487);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(57, 13);
            this.label76.TabIndex = 42;
            this.label76.Text = "Completed";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(293, 408);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(57, 13);
            this.label75.TabIndex = 41;
            this.label75.Text = "Completed";
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox50.BackgroundImage")));
            this.pictureBox50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox50.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox50.InitialImage")));
            this.pictureBox50.Location = new System.Drawing.Point(32, 424);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(48, 48);
            this.pictureBox50.TabIndex = 34;
            this.pictureBox50.TabStop = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox51.BackgroundImage")));
            this.pictureBox51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox51.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox51.InitialImage")));
            this.pictureBox51.Location = new System.Drawing.Point(302, 424);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(48, 48);
            this.pictureBox51.TabIndex = 33;
            this.pictureBox51.TabStop = false;
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox52.BackgroundImage")));
            this.pictureBox52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox52.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox52.InitialImage")));
            this.pictureBox52.Location = new System.Drawing.Point(86, 424);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(48, 48);
            this.pictureBox52.TabIndex = 32;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox53.BackgroundImage")));
            this.pictureBox53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox53.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox53.InitialImage")));
            this.pictureBox53.Location = new System.Drawing.Point(140, 424);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(48, 48);
            this.pictureBox53.TabIndex = 31;
            this.pictureBox53.TabStop = false;
            // 
            // pictureBox54
            // 
            this.pictureBox54.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox54.BackgroundImage")));
            this.pictureBox54.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox54.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox54.InitialImage")));
            this.pictureBox54.Location = new System.Drawing.Point(194, 424);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(48, 48);
            this.pictureBox54.TabIndex = 30;
            this.pictureBox54.TabStop = false;
            // 
            // pictureBox55
            // 
            this.pictureBox55.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox55.BackgroundImage")));
            this.pictureBox55.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox55.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox55.InitialImage")));
            this.pictureBox55.Location = new System.Drawing.Point(248, 424);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(48, 48);
            this.pictureBox55.TabIndex = 29;
            this.pictureBox55.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox25.BackgroundImage")));
            this.pictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox25.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox25.InitialImage")));
            this.pictureBox25.Location = new System.Drawing.Point(32, 503);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(48, 48);
            this.pictureBox25.TabIndex = 21;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox26.BackgroundImage")));
            this.pictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox26.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox26.InitialImage")));
            this.pictureBox26.Location = new System.Drawing.Point(302, 503);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(48, 48);
            this.pictureBox26.TabIndex = 20;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox27.BackgroundImage")));
            this.pictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox27.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox27.InitialImage")));
            this.pictureBox27.Location = new System.Drawing.Point(86, 503);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(48, 48);
            this.pictureBox27.TabIndex = 19;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox28.BackgroundImage")));
            this.pictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox28.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox28.InitialImage")));
            this.pictureBox28.Location = new System.Drawing.Point(140, 503);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(48, 48);
            this.pictureBox28.TabIndex = 18;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox29.BackgroundImage")));
            this.pictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox29.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox29.InitialImage")));
            this.pictureBox29.Location = new System.Drawing.Point(194, 503);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(48, 48);
            this.pictureBox29.TabIndex = 17;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox30.BackgroundImage")));
            this.pictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox30.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox30.InitialImage")));
            this.pictureBox30.Location = new System.Drawing.Point(248, 503);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(48, 48);
            this.pictureBox30.TabIndex = 16;
            this.pictureBox30.TabStop = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.pictureBox134);
            this.tabPage6.Controls.Add(this.pictureBox135);
            this.tabPage6.Controls.Add(this.pictureBox136);
            this.tabPage6.Controls.Add(this.pictureBox137);
            this.tabPage6.Controls.Add(this.pictureBox138);
            this.tabPage6.Controls.Add(this.label155);
            this.tabPage6.Controls.Add(this.label156);
            this.tabPage6.Controls.Add(this.label157);
            this.tabPage6.Controls.Add(this.label158);
            this.tabPage6.Controls.Add(this.label159);
            this.tabPage6.Controls.Add(this.label160);
            this.tabPage6.Controls.Add(this.pictureBox139);
            this.tabPage6.Controls.Add(this.pictureBox140);
            this.tabPage6.Controls.Add(this.pictureBox141);
            this.tabPage6.Controls.Add(this.pictureBox142);
            this.tabPage6.Controls.Add(this.pictureBox143);
            this.tabPage6.Controls.Add(this.label161);
            this.tabPage6.Controls.Add(this.pictureBox112);
            this.tabPage6.Controls.Add(this.pictureBox113);
            this.tabPage6.Controls.Add(this.pictureBox114);
            this.tabPage6.Controls.Add(this.pictureBox115);
            this.tabPage6.Controls.Add(this.pictureBox116);
            this.tabPage6.Controls.Add(this.label16);
            this.tabPage6.Controls.Add(this.label141);
            this.tabPage6.Controls.Add(this.label142);
            this.tabPage6.Controls.Add(this.label143);
            this.tabPage6.Controls.Add(this.label144);
            this.tabPage6.Controls.Add(this.label145);
            this.tabPage6.Controls.Add(this.pictureBox117);
            this.tabPage6.Controls.Add(this.pictureBox118);
            this.tabPage6.Controls.Add(this.pictureBox119);
            this.tabPage6.Controls.Add(this.pictureBox120);
            this.tabPage6.Controls.Add(this.pictureBox121);
            this.tabPage6.Controls.Add(this.label146);
            this.tabPage6.Controls.Add(this.pictureBox122);
            this.tabPage6.Controls.Add(this.pictureBox123);
            this.tabPage6.Controls.Add(this.pictureBox124);
            this.tabPage6.Controls.Add(this.pictureBox125);
            this.tabPage6.Controls.Add(this.pictureBox126);
            this.tabPage6.Controls.Add(this.pictureBox127);
            this.tabPage6.Controls.Add(this.label147);
            this.tabPage6.Controls.Add(this.label148);
            this.tabPage6.Controls.Add(this.label149);
            this.tabPage6.Controls.Add(this.label150);
            this.tabPage6.Controls.Add(this.label151);
            this.tabPage6.Controls.Add(this.label152);
            this.tabPage6.Controls.Add(this.pictureBox128);
            this.tabPage6.Controls.Add(this.pictureBox129);
            this.tabPage6.Controls.Add(this.pictureBox130);
            this.tabPage6.Controls.Add(this.pictureBox131);
            this.tabPage6.Controls.Add(this.pictureBox132);
            this.tabPage6.Controls.Add(this.pictureBox133);
            this.tabPage6.Controls.Add(this.label153);
            this.tabPage6.Controls.Add(this.label154);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(560, 597);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Heavy Assault";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // pictureBox134
            // 
            this.pictureBox134.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox134.BackgroundImage")));
            this.pictureBox134.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox134.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox134.InitialImage")));
            this.pictureBox134.Location = new System.Drawing.Point(32, 503);
            this.pictureBox134.Name = "pictureBox134";
            this.pictureBox134.Size = new System.Drawing.Size(58, 48);
            this.pictureBox134.TabIndex = 198;
            this.pictureBox134.TabStop = false;
            this.pictureBox134.Visible = false;
            // 
            // pictureBox135
            // 
            this.pictureBox135.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox135.BackgroundImage")));
            this.pictureBox135.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox135.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox135.InitialImage")));
            this.pictureBox135.Location = new System.Drawing.Point(97, 503);
            this.pictureBox135.Name = "pictureBox135";
            this.pictureBox135.Size = new System.Drawing.Size(58, 48);
            this.pictureBox135.TabIndex = 197;
            this.pictureBox135.TabStop = false;
            this.pictureBox135.Visible = false;
            // 
            // pictureBox136
            // 
            this.pictureBox136.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox136.BackgroundImage")));
            this.pictureBox136.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox136.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox136.InitialImage")));
            this.pictureBox136.Location = new System.Drawing.Point(162, 503);
            this.pictureBox136.Name = "pictureBox136";
            this.pictureBox136.Size = new System.Drawing.Size(58, 48);
            this.pictureBox136.TabIndex = 196;
            this.pictureBox136.TabStop = false;
            this.pictureBox136.Visible = false;
            // 
            // pictureBox137
            // 
            this.pictureBox137.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox137.BackgroundImage")));
            this.pictureBox137.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox137.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox137.InitialImage")));
            this.pictureBox137.Location = new System.Drawing.Point(227, 503);
            this.pictureBox137.Name = "pictureBox137";
            this.pictureBox137.Size = new System.Drawing.Size(58, 48);
            this.pictureBox137.TabIndex = 195;
            this.pictureBox137.TabStop = false;
            this.pictureBox137.Visible = false;
            // 
            // pictureBox138
            // 
            this.pictureBox138.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox138.BackgroundImage")));
            this.pictureBox138.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox138.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox138.InitialImage")));
            this.pictureBox138.Location = new System.Drawing.Point(292, 503);
            this.pictureBox138.Name = "pictureBox138";
            this.pictureBox138.Size = new System.Drawing.Size(58, 48);
            this.pictureBox138.TabIndex = 194;
            this.pictureBox138.TabStop = false;
            this.pictureBox138.Visible = false;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label155.Location = new System.Drawing.Point(309, 521);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(25, 13);
            this.label155.TabIndex = 204;
            this.label155.Text = "500";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label156.Location = new System.Drawing.Point(243, 521);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(25, 13);
            this.label156.TabIndex = 203;
            this.label156.Text = "200";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label157.Location = new System.Drawing.Point(179, 521);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(25, 13);
            this.label157.TabIndex = 202;
            this.label157.Text = "150";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label158.Location = new System.Drawing.Point(113, 521);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(25, 13);
            this.label158.TabIndex = 201;
            this.label158.Text = "100";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label159.Location = new System.Drawing.Point(50, 521);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(19, 13);
            this.label159.TabIndex = 200;
            this.label159.Text = "50";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(293, 487);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(57, 13);
            this.label160.TabIndex = 199;
            this.label160.Text = "Completed";
            this.label160.Visible = false;
            // 
            // pictureBox139
            // 
            this.pictureBox139.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox139.BackgroundImage")));
            this.pictureBox139.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox139.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox139.InitialImage")));
            this.pictureBox139.Location = new System.Drawing.Point(32, 503);
            this.pictureBox139.Name = "pictureBox139";
            this.pictureBox139.Size = new System.Drawing.Size(58, 48);
            this.pictureBox139.TabIndex = 193;
            this.pictureBox139.TabStop = false;
            // 
            // pictureBox140
            // 
            this.pictureBox140.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox140.BackgroundImage")));
            this.pictureBox140.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox140.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox140.InitialImage")));
            this.pictureBox140.Location = new System.Drawing.Point(97, 503);
            this.pictureBox140.Name = "pictureBox140";
            this.pictureBox140.Size = new System.Drawing.Size(58, 48);
            this.pictureBox140.TabIndex = 192;
            this.pictureBox140.TabStop = false;
            // 
            // pictureBox141
            // 
            this.pictureBox141.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox141.BackgroundImage")));
            this.pictureBox141.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox141.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox141.InitialImage")));
            this.pictureBox141.Location = new System.Drawing.Point(162, 503);
            this.pictureBox141.Name = "pictureBox141";
            this.pictureBox141.Size = new System.Drawing.Size(58, 48);
            this.pictureBox141.TabIndex = 191;
            this.pictureBox141.TabStop = false;
            // 
            // pictureBox142
            // 
            this.pictureBox142.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox142.BackgroundImage")));
            this.pictureBox142.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox142.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox142.InitialImage")));
            this.pictureBox142.Location = new System.Drawing.Point(227, 503);
            this.pictureBox142.Name = "pictureBox142";
            this.pictureBox142.Size = new System.Drawing.Size(58, 48);
            this.pictureBox142.TabIndex = 190;
            this.pictureBox142.TabStop = false;
            // 
            // pictureBox143
            // 
            this.pictureBox143.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox143.BackgroundImage")));
            this.pictureBox143.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox143.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox143.InitialImage")));
            this.pictureBox143.Location = new System.Drawing.Point(292, 503);
            this.pictureBox143.Name = "pictureBox143";
            this.pictureBox143.Size = new System.Drawing.Size(58, 48);
            this.pictureBox143.TabIndex = 189;
            this.pictureBox143.TabStop = false;
            // 
            // pictureBox112
            // 
            this.pictureBox112.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox112.BackgroundImage")));
            this.pictureBox112.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox112.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox112.InitialImage")));
            this.pictureBox112.Location = new System.Drawing.Point(32, 424);
            this.pictureBox112.Name = "pictureBox112";
            this.pictureBox112.Size = new System.Drawing.Size(58, 48);
            this.pictureBox112.TabIndex = 181;
            this.pictureBox112.TabStop = false;
            this.pictureBox112.Visible = false;
            // 
            // pictureBox113
            // 
            this.pictureBox113.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox113.BackgroundImage")));
            this.pictureBox113.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox113.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox113.InitialImage")));
            this.pictureBox113.Location = new System.Drawing.Point(97, 424);
            this.pictureBox113.Name = "pictureBox113";
            this.pictureBox113.Size = new System.Drawing.Size(58, 48);
            this.pictureBox113.TabIndex = 180;
            this.pictureBox113.TabStop = false;
            this.pictureBox113.Visible = false;
            // 
            // pictureBox114
            // 
            this.pictureBox114.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox114.BackgroundImage")));
            this.pictureBox114.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox114.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox114.InitialImage")));
            this.pictureBox114.Location = new System.Drawing.Point(162, 424);
            this.pictureBox114.Name = "pictureBox114";
            this.pictureBox114.Size = new System.Drawing.Size(58, 48);
            this.pictureBox114.TabIndex = 179;
            this.pictureBox114.TabStop = false;
            this.pictureBox114.Visible = false;
            // 
            // pictureBox115
            // 
            this.pictureBox115.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox115.BackgroundImage")));
            this.pictureBox115.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox115.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox115.InitialImage")));
            this.pictureBox115.Location = new System.Drawing.Point(227, 424);
            this.pictureBox115.Name = "pictureBox115";
            this.pictureBox115.Size = new System.Drawing.Size(58, 48);
            this.pictureBox115.TabIndex = 178;
            this.pictureBox115.TabStop = false;
            this.pictureBox115.Visible = false;
            // 
            // pictureBox116
            // 
            this.pictureBox116.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox116.BackgroundImage")));
            this.pictureBox116.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox116.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox116.InitialImage")));
            this.pictureBox116.Location = new System.Drawing.Point(292, 424);
            this.pictureBox116.Name = "pictureBox116";
            this.pictureBox116.Size = new System.Drawing.Size(58, 48);
            this.pictureBox116.TabIndex = 177;
            this.pictureBox116.TabStop = false;
            this.pictureBox116.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(306, 442);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 187;
            this.label16.Text = "1000";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label141.Location = new System.Drawing.Point(243, 442);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(25, 13);
            this.label141.TabIndex = 186;
            this.label141.Text = "500";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label142.Location = new System.Drawing.Point(179, 442);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(25, 13);
            this.label142.TabIndex = 185;
            this.label142.Text = "400";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label143.Location = new System.Drawing.Point(113, 442);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(25, 13);
            this.label143.TabIndex = 184;
            this.label143.Text = "200";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label144.Location = new System.Drawing.Point(50, 442);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(25, 13);
            this.label144.TabIndex = 183;
            this.label144.Text = "150";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(293, 408);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(57, 13);
            this.label145.TabIndex = 182;
            this.label145.Text = "Completed";
            this.label145.Visible = false;
            // 
            // pictureBox117
            // 
            this.pictureBox117.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox117.BackgroundImage")));
            this.pictureBox117.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox117.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox117.InitialImage")));
            this.pictureBox117.Location = new System.Drawing.Point(32, 424);
            this.pictureBox117.Name = "pictureBox117";
            this.pictureBox117.Size = new System.Drawing.Size(58, 48);
            this.pictureBox117.TabIndex = 176;
            this.pictureBox117.TabStop = false;
            // 
            // pictureBox118
            // 
            this.pictureBox118.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox118.BackgroundImage")));
            this.pictureBox118.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox118.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox118.InitialImage")));
            this.pictureBox118.Location = new System.Drawing.Point(97, 424);
            this.pictureBox118.Name = "pictureBox118";
            this.pictureBox118.Size = new System.Drawing.Size(58, 48);
            this.pictureBox118.TabIndex = 175;
            this.pictureBox118.TabStop = false;
            // 
            // pictureBox119
            // 
            this.pictureBox119.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox119.BackgroundImage")));
            this.pictureBox119.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox119.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox119.InitialImage")));
            this.pictureBox119.Location = new System.Drawing.Point(162, 424);
            this.pictureBox119.Name = "pictureBox119";
            this.pictureBox119.Size = new System.Drawing.Size(58, 48);
            this.pictureBox119.TabIndex = 174;
            this.pictureBox119.TabStop = false;
            // 
            // pictureBox120
            // 
            this.pictureBox120.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox120.BackgroundImage")));
            this.pictureBox120.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox120.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox120.InitialImage")));
            this.pictureBox120.Location = new System.Drawing.Point(227, 424);
            this.pictureBox120.Name = "pictureBox120";
            this.pictureBox120.Size = new System.Drawing.Size(58, 48);
            this.pictureBox120.TabIndex = 173;
            this.pictureBox120.TabStop = false;
            // 
            // pictureBox121
            // 
            this.pictureBox121.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox121.BackgroundImage")));
            this.pictureBox121.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox121.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox121.InitialImage")));
            this.pictureBox121.Location = new System.Drawing.Point(292, 424);
            this.pictureBox121.Name = "pictureBox121";
            this.pictureBox121.Size = new System.Drawing.Size(58, 48);
            this.pictureBox121.TabIndex = 172;
            this.pictureBox121.TabStop = false;
            // 
            // pictureBox122
            // 
            this.pictureBox122.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox122.BackgroundImage")));
            this.pictureBox122.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox122.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox122.InitialImage")));
            this.pictureBox122.Location = new System.Drawing.Point(32, 345);
            this.pictureBox122.Name = "pictureBox122";
            this.pictureBox122.Size = new System.Drawing.Size(48, 48);
            this.pictureBox122.TabIndex = 164;
            this.pictureBox122.TabStop = false;
            this.pictureBox122.Visible = false;
            // 
            // pictureBox123
            // 
            this.pictureBox123.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox123.BackgroundImage")));
            this.pictureBox123.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox123.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox123.InitialImage")));
            this.pictureBox123.Location = new System.Drawing.Point(86, 345);
            this.pictureBox123.Name = "pictureBox123";
            this.pictureBox123.Size = new System.Drawing.Size(48, 48);
            this.pictureBox123.TabIndex = 163;
            this.pictureBox123.TabStop = false;
            this.pictureBox123.Visible = false;
            // 
            // pictureBox124
            // 
            this.pictureBox124.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox124.BackgroundImage")));
            this.pictureBox124.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox124.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox124.InitialImage")));
            this.pictureBox124.Location = new System.Drawing.Point(140, 345);
            this.pictureBox124.Name = "pictureBox124";
            this.pictureBox124.Size = new System.Drawing.Size(48, 48);
            this.pictureBox124.TabIndex = 162;
            this.pictureBox124.TabStop = false;
            this.pictureBox124.Visible = false;
            // 
            // pictureBox125
            // 
            this.pictureBox125.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox125.BackgroundImage")));
            this.pictureBox125.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox125.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox125.InitialImage")));
            this.pictureBox125.Location = new System.Drawing.Point(194, 345);
            this.pictureBox125.Name = "pictureBox125";
            this.pictureBox125.Size = new System.Drawing.Size(48, 48);
            this.pictureBox125.TabIndex = 161;
            this.pictureBox125.TabStop = false;
            this.pictureBox125.Visible = false;
            // 
            // pictureBox126
            // 
            this.pictureBox126.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox126.BackgroundImage")));
            this.pictureBox126.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox126.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox126.InitialImage")));
            this.pictureBox126.Location = new System.Drawing.Point(248, 345);
            this.pictureBox126.Name = "pictureBox126";
            this.pictureBox126.Size = new System.Drawing.Size(48, 48);
            this.pictureBox126.TabIndex = 160;
            this.pictureBox126.TabStop = false;
            this.pictureBox126.Visible = false;
            // 
            // pictureBox127
            // 
            this.pictureBox127.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox127.BackgroundImage")));
            this.pictureBox127.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox127.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox127.InitialImage")));
            this.pictureBox127.Location = new System.Drawing.Point(302, 345);
            this.pictureBox127.Name = "pictureBox127";
            this.pictureBox127.Size = new System.Drawing.Size(48, 48);
            this.pictureBox127.TabIndex = 153;
            this.pictureBox127.TabStop = false;
            this.pictureBox127.Visible = false;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label147.Location = new System.Drawing.Point(314, 363);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(25, 13);
            this.label147.TabIndex = 170;
            this.label147.Text = "500";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label148.Location = new System.Drawing.Point(260, 363);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(25, 13);
            this.label148.TabIndex = 169;
            this.label148.Text = "200";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label149.Location = new System.Drawing.Point(206, 363);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(25, 13);
            this.label149.TabIndex = 168;
            this.label149.Text = "100";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label150.Location = new System.Drawing.Point(155, 363);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(19, 13);
            this.label150.TabIndex = 167;
            this.label150.Text = "30";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label151.Location = new System.Drawing.Point(104, 362);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(13, 13);
            this.label151.TabIndex = 166;
            this.label151.Text = "1";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label152.Location = new System.Drawing.Point(50, 363);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(13, 13);
            this.label152.TabIndex = 165;
            this.label152.Text = "0";
            // 
            // pictureBox128
            // 
            this.pictureBox128.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox128.BackgroundImage")));
            this.pictureBox128.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox128.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox128.InitialImage")));
            this.pictureBox128.Location = new System.Drawing.Point(32, 345);
            this.pictureBox128.Name = "pictureBox128";
            this.pictureBox128.Size = new System.Drawing.Size(48, 48);
            this.pictureBox128.TabIndex = 159;
            this.pictureBox128.TabStop = false;
            // 
            // pictureBox129
            // 
            this.pictureBox129.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox129.BackgroundImage")));
            this.pictureBox129.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox129.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox129.InitialImage")));
            this.pictureBox129.Location = new System.Drawing.Point(302, 345);
            this.pictureBox129.Name = "pictureBox129";
            this.pictureBox129.Size = new System.Drawing.Size(48, 48);
            this.pictureBox129.TabIndex = 158;
            this.pictureBox129.TabStop = false;
            // 
            // pictureBox130
            // 
            this.pictureBox130.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox130.BackgroundImage")));
            this.pictureBox130.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox130.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox130.InitialImage")));
            this.pictureBox130.Location = new System.Drawing.Point(86, 345);
            this.pictureBox130.Name = "pictureBox130";
            this.pictureBox130.Size = new System.Drawing.Size(48, 48);
            this.pictureBox130.TabIndex = 157;
            this.pictureBox130.TabStop = false;
            // 
            // pictureBox131
            // 
            this.pictureBox131.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox131.BackgroundImage")));
            this.pictureBox131.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox131.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox131.InitialImage")));
            this.pictureBox131.Location = new System.Drawing.Point(140, 345);
            this.pictureBox131.Name = "pictureBox131";
            this.pictureBox131.Size = new System.Drawing.Size(48, 48);
            this.pictureBox131.TabIndex = 156;
            this.pictureBox131.TabStop = false;
            // 
            // pictureBox132
            // 
            this.pictureBox132.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox132.BackgroundImage")));
            this.pictureBox132.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox132.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox132.InitialImage")));
            this.pictureBox132.Location = new System.Drawing.Point(194, 345);
            this.pictureBox132.Name = "pictureBox132";
            this.pictureBox132.Size = new System.Drawing.Size(48, 48);
            this.pictureBox132.TabIndex = 155;
            this.pictureBox132.TabStop = false;
            // 
            // pictureBox133
            // 
            this.pictureBox133.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox133.BackgroundImage")));
            this.pictureBox133.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox133.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox133.InitialImage")));
            this.pictureBox133.Location = new System.Drawing.Point(248, 345);
            this.pictureBox133.Name = "pictureBox133";
            this.pictureBox133.Size = new System.Drawing.Size(48, 48);
            this.pictureBox133.TabIndex = 154;
            this.pictureBox133.TabStop = false;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(293, 329);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(57, 13);
            this.label153.TabIndex = 152;
            this.label153.Text = "Completed";
            this.label153.Visible = false;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.pictureBox144);
            this.tabPage7.Controls.Add(this.pictureBox145);
            this.tabPage7.Controls.Add(this.pictureBox146);
            this.tabPage7.Controls.Add(this.pictureBox147);
            this.tabPage7.Controls.Add(this.pictureBox148);
            this.tabPage7.Controls.Add(this.label162);
            this.tabPage7.Controls.Add(this.label163);
            this.tabPage7.Controls.Add(this.label164);
            this.tabPage7.Controls.Add(this.label165);
            this.tabPage7.Controls.Add(this.label166);
            this.tabPage7.Controls.Add(this.label167);
            this.tabPage7.Controls.Add(this.pictureBox149);
            this.tabPage7.Controls.Add(this.pictureBox150);
            this.tabPage7.Controls.Add(this.pictureBox151);
            this.tabPage7.Controls.Add(this.pictureBox152);
            this.tabPage7.Controls.Add(this.pictureBox153);
            this.tabPage7.Controls.Add(this.label168);
            this.tabPage7.Controls.Add(this.pictureBox154);
            this.tabPage7.Controls.Add(this.pictureBox155);
            this.tabPage7.Controls.Add(this.pictureBox156);
            this.tabPage7.Controls.Add(this.pictureBox157);
            this.tabPage7.Controls.Add(this.pictureBox158);
            this.tabPage7.Controls.Add(this.label169);
            this.tabPage7.Controls.Add(this.label170);
            this.tabPage7.Controls.Add(this.label171);
            this.tabPage7.Controls.Add(this.label172);
            this.tabPage7.Controls.Add(this.label173);
            this.tabPage7.Controls.Add(this.label174);
            this.tabPage7.Controls.Add(this.pictureBox159);
            this.tabPage7.Controls.Add(this.pictureBox160);
            this.tabPage7.Controls.Add(this.pictureBox161);
            this.tabPage7.Controls.Add(this.pictureBox162);
            this.tabPage7.Controls.Add(this.pictureBox163);
            this.tabPage7.Controls.Add(this.label175);
            this.tabPage7.Controls.Add(this.pictureBox164);
            this.tabPage7.Controls.Add(this.pictureBox165);
            this.tabPage7.Controls.Add(this.pictureBox166);
            this.tabPage7.Controls.Add(this.pictureBox167);
            this.tabPage7.Controls.Add(this.pictureBox168);
            this.tabPage7.Controls.Add(this.pictureBox169);
            this.tabPage7.Controls.Add(this.label176);
            this.tabPage7.Controls.Add(this.label177);
            this.tabPage7.Controls.Add(this.label178);
            this.tabPage7.Controls.Add(this.label179);
            this.tabPage7.Controls.Add(this.label180);
            this.tabPage7.Controls.Add(this.label181);
            this.tabPage7.Controls.Add(this.pictureBox170);
            this.tabPage7.Controls.Add(this.pictureBox171);
            this.tabPage7.Controls.Add(this.pictureBox172);
            this.tabPage7.Controls.Add(this.pictureBox173);
            this.tabPage7.Controls.Add(this.pictureBox174);
            this.tabPage7.Controls.Add(this.pictureBox175);
            this.tabPage7.Controls.Add(this.label182);
            this.tabPage7.Controls.Add(this.label183);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(560, 597);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Max";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // pictureBox144
            // 
            this.pictureBox144.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox144.BackgroundImage")));
            this.pictureBox144.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox144.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox144.InitialImage")));
            this.pictureBox144.Location = new System.Drawing.Point(32, 345);
            this.pictureBox144.Name = "pictureBox144";
            this.pictureBox144.Size = new System.Drawing.Size(58, 48);
            this.pictureBox144.TabIndex = 252;
            this.pictureBox144.TabStop = false;
            this.pictureBox144.Visible = false;
            // 
            // pictureBox145
            // 
            this.pictureBox145.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox145.BackgroundImage")));
            this.pictureBox145.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox145.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox145.InitialImage")));
            this.pictureBox145.Location = new System.Drawing.Point(97, 345);
            this.pictureBox145.Name = "pictureBox145";
            this.pictureBox145.Size = new System.Drawing.Size(58, 48);
            this.pictureBox145.TabIndex = 251;
            this.pictureBox145.TabStop = false;
            this.pictureBox145.Visible = false;
            // 
            // pictureBox146
            // 
            this.pictureBox146.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox146.BackgroundImage")));
            this.pictureBox146.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox146.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox146.InitialImage")));
            this.pictureBox146.Location = new System.Drawing.Point(162, 345);
            this.pictureBox146.Name = "pictureBox146";
            this.pictureBox146.Size = new System.Drawing.Size(58, 48);
            this.pictureBox146.TabIndex = 250;
            this.pictureBox146.TabStop = false;
            this.pictureBox146.Visible = false;
            // 
            // pictureBox147
            // 
            this.pictureBox147.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox147.BackgroundImage")));
            this.pictureBox147.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox147.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox147.InitialImage")));
            this.pictureBox147.Location = new System.Drawing.Point(227, 345);
            this.pictureBox147.Name = "pictureBox147";
            this.pictureBox147.Size = new System.Drawing.Size(58, 48);
            this.pictureBox147.TabIndex = 249;
            this.pictureBox147.TabStop = false;
            this.pictureBox147.Visible = false;
            // 
            // pictureBox148
            // 
            this.pictureBox148.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox148.BackgroundImage")));
            this.pictureBox148.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox148.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox148.InitialImage")));
            this.pictureBox148.Location = new System.Drawing.Point(292, 345);
            this.pictureBox148.Name = "pictureBox148";
            this.pictureBox148.Size = new System.Drawing.Size(58, 48);
            this.pictureBox148.TabIndex = 248;
            this.pictureBox148.TabStop = false;
            this.pictureBox148.Visible = false;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label162.Location = new System.Drawing.Point(306, 363);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(31, 13);
            this.label162.TabIndex = 258;
            this.label162.Text = "1000";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label163.Location = new System.Drawing.Point(243, 363);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(25, 13);
            this.label163.TabIndex = 257;
            this.label163.Text = "500";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label164.Location = new System.Drawing.Point(179, 363);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(25, 13);
            this.label164.TabIndex = 256;
            this.label164.Text = "200";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label165.Location = new System.Drawing.Point(113, 363);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(25, 13);
            this.label165.TabIndex = 255;
            this.label165.Text = "150";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label166.Location = new System.Drawing.Point(48, 363);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(25, 13);
            this.label166.TabIndex = 254;
            this.label166.Text = "100";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(293, 329);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(57, 13);
            this.label167.TabIndex = 253;
            this.label167.Text = "Completed";
            this.label167.Visible = false;
            // 
            // pictureBox149
            // 
            this.pictureBox149.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox149.BackgroundImage")));
            this.pictureBox149.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox149.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox149.InitialImage")));
            this.pictureBox149.Location = new System.Drawing.Point(32, 345);
            this.pictureBox149.Name = "pictureBox149";
            this.pictureBox149.Size = new System.Drawing.Size(58, 48);
            this.pictureBox149.TabIndex = 247;
            this.pictureBox149.TabStop = false;
            // 
            // pictureBox150
            // 
            this.pictureBox150.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox150.BackgroundImage")));
            this.pictureBox150.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox150.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox150.InitialImage")));
            this.pictureBox150.Location = new System.Drawing.Point(97, 345);
            this.pictureBox150.Name = "pictureBox150";
            this.pictureBox150.Size = new System.Drawing.Size(58, 48);
            this.pictureBox150.TabIndex = 246;
            this.pictureBox150.TabStop = false;
            // 
            // pictureBox151
            // 
            this.pictureBox151.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox151.BackgroundImage")));
            this.pictureBox151.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox151.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox151.InitialImage")));
            this.pictureBox151.Location = new System.Drawing.Point(162, 345);
            this.pictureBox151.Name = "pictureBox151";
            this.pictureBox151.Size = new System.Drawing.Size(58, 48);
            this.pictureBox151.TabIndex = 245;
            this.pictureBox151.TabStop = false;
            // 
            // pictureBox152
            // 
            this.pictureBox152.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox152.BackgroundImage")));
            this.pictureBox152.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox152.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox152.InitialImage")));
            this.pictureBox152.Location = new System.Drawing.Point(227, 345);
            this.pictureBox152.Name = "pictureBox152";
            this.pictureBox152.Size = new System.Drawing.Size(58, 48);
            this.pictureBox152.TabIndex = 244;
            this.pictureBox152.TabStop = false;
            // 
            // pictureBox153
            // 
            this.pictureBox153.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox153.BackgroundImage")));
            this.pictureBox153.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox153.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox153.InitialImage")));
            this.pictureBox153.Location = new System.Drawing.Point(292, 345);
            this.pictureBox153.Name = "pictureBox153";
            this.pictureBox153.Size = new System.Drawing.Size(58, 48);
            this.pictureBox153.TabIndex = 243;
            this.pictureBox153.TabStop = false;
            // 
            // pictureBox154
            // 
            this.pictureBox154.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox154.BackgroundImage")));
            this.pictureBox154.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox154.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox154.InitialImage")));
            this.pictureBox154.Location = new System.Drawing.Point(32, 424);
            this.pictureBox154.Name = "pictureBox154";
            this.pictureBox154.Size = new System.Drawing.Size(58, 48);
            this.pictureBox154.TabIndex = 235;
            this.pictureBox154.TabStop = false;
            this.pictureBox154.Visible = false;
            // 
            // pictureBox155
            // 
            this.pictureBox155.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox155.BackgroundImage")));
            this.pictureBox155.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox155.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox155.InitialImage")));
            this.pictureBox155.Location = new System.Drawing.Point(97, 424);
            this.pictureBox155.Name = "pictureBox155";
            this.pictureBox155.Size = new System.Drawing.Size(58, 48);
            this.pictureBox155.TabIndex = 234;
            this.pictureBox155.TabStop = false;
            this.pictureBox155.Visible = false;
            // 
            // pictureBox156
            // 
            this.pictureBox156.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox156.BackgroundImage")));
            this.pictureBox156.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox156.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox156.InitialImage")));
            this.pictureBox156.Location = new System.Drawing.Point(162, 424);
            this.pictureBox156.Name = "pictureBox156";
            this.pictureBox156.Size = new System.Drawing.Size(58, 48);
            this.pictureBox156.TabIndex = 233;
            this.pictureBox156.TabStop = false;
            this.pictureBox156.Visible = false;
            // 
            // pictureBox157
            // 
            this.pictureBox157.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox157.BackgroundImage")));
            this.pictureBox157.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox157.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox157.InitialImage")));
            this.pictureBox157.Location = new System.Drawing.Point(227, 424);
            this.pictureBox157.Name = "pictureBox157";
            this.pictureBox157.Size = new System.Drawing.Size(58, 48);
            this.pictureBox157.TabIndex = 232;
            this.pictureBox157.TabStop = false;
            this.pictureBox157.Visible = false;
            // 
            // pictureBox158
            // 
            this.pictureBox158.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox158.BackgroundImage")));
            this.pictureBox158.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox158.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox158.InitialImage")));
            this.pictureBox158.Location = new System.Drawing.Point(292, 424);
            this.pictureBox158.Name = "pictureBox158";
            this.pictureBox158.Size = new System.Drawing.Size(58, 48);
            this.pictureBox158.TabIndex = 231;
            this.pictureBox158.TabStop = false;
            this.pictureBox158.Visible = false;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label169.Location = new System.Drawing.Point(306, 442);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(31, 13);
            this.label169.TabIndex = 241;
            this.label169.Text = "1000";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label170.Location = new System.Drawing.Point(243, 442);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(25, 13);
            this.label170.TabIndex = 240;
            this.label170.Text = "500";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label171.Location = new System.Drawing.Point(179, 442);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(25, 13);
            this.label171.TabIndex = 239;
            this.label171.Text = "400";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label172.Location = new System.Drawing.Point(113, 442);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(25, 13);
            this.label172.TabIndex = 238;
            this.label172.Text = "200";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label173.Location = new System.Drawing.Point(48, 442);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(25, 13);
            this.label173.TabIndex = 237;
            this.label173.Text = "150";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(293, 408);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(57, 13);
            this.label174.TabIndex = 236;
            this.label174.Text = "Completed";
            this.label174.Visible = false;
            // 
            // pictureBox159
            // 
            this.pictureBox159.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox159.BackgroundImage")));
            this.pictureBox159.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox159.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox159.InitialImage")));
            this.pictureBox159.Location = new System.Drawing.Point(32, 424);
            this.pictureBox159.Name = "pictureBox159";
            this.pictureBox159.Size = new System.Drawing.Size(58, 48);
            this.pictureBox159.TabIndex = 230;
            this.pictureBox159.TabStop = false;
            // 
            // pictureBox160
            // 
            this.pictureBox160.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox160.BackgroundImage")));
            this.pictureBox160.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox160.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox160.InitialImage")));
            this.pictureBox160.Location = new System.Drawing.Point(97, 424);
            this.pictureBox160.Name = "pictureBox160";
            this.pictureBox160.Size = new System.Drawing.Size(58, 48);
            this.pictureBox160.TabIndex = 229;
            this.pictureBox160.TabStop = false;
            // 
            // pictureBox161
            // 
            this.pictureBox161.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox161.BackgroundImage")));
            this.pictureBox161.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox161.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox161.InitialImage")));
            this.pictureBox161.Location = new System.Drawing.Point(162, 424);
            this.pictureBox161.Name = "pictureBox161";
            this.pictureBox161.Size = new System.Drawing.Size(58, 48);
            this.pictureBox161.TabIndex = 228;
            this.pictureBox161.TabStop = false;
            // 
            // pictureBox162
            // 
            this.pictureBox162.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox162.BackgroundImage")));
            this.pictureBox162.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox162.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox162.InitialImage")));
            this.pictureBox162.Location = new System.Drawing.Point(227, 424);
            this.pictureBox162.Name = "pictureBox162";
            this.pictureBox162.Size = new System.Drawing.Size(58, 48);
            this.pictureBox162.TabIndex = 227;
            this.pictureBox162.TabStop = false;
            // 
            // pictureBox163
            // 
            this.pictureBox163.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox163.BackgroundImage")));
            this.pictureBox163.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox163.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox163.InitialImage")));
            this.pictureBox163.Location = new System.Drawing.Point(292, 424);
            this.pictureBox163.Name = "pictureBox163";
            this.pictureBox163.Size = new System.Drawing.Size(58, 48);
            this.pictureBox163.TabIndex = 226;
            this.pictureBox163.TabStop = false;
            // 
            // pictureBox164
            // 
            this.pictureBox164.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox164.BackgroundImage")));
            this.pictureBox164.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox164.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox164.InitialImage")));
            this.pictureBox164.Location = new System.Drawing.Point(32, 503);
            this.pictureBox164.Name = "pictureBox164";
            this.pictureBox164.Size = new System.Drawing.Size(48, 48);
            this.pictureBox164.TabIndex = 218;
            this.pictureBox164.TabStop = false;
            this.pictureBox164.Visible = false;
            // 
            // pictureBox165
            // 
            this.pictureBox165.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox165.BackgroundImage")));
            this.pictureBox165.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox165.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox165.InitialImage")));
            this.pictureBox165.Location = new System.Drawing.Point(86, 503);
            this.pictureBox165.Name = "pictureBox165";
            this.pictureBox165.Size = new System.Drawing.Size(48, 48);
            this.pictureBox165.TabIndex = 217;
            this.pictureBox165.TabStop = false;
            this.pictureBox165.Visible = false;
            // 
            // pictureBox166
            // 
            this.pictureBox166.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox166.BackgroundImage")));
            this.pictureBox166.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox166.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox166.InitialImage")));
            this.pictureBox166.Location = new System.Drawing.Point(140, 503);
            this.pictureBox166.Name = "pictureBox166";
            this.pictureBox166.Size = new System.Drawing.Size(48, 48);
            this.pictureBox166.TabIndex = 216;
            this.pictureBox166.TabStop = false;
            this.pictureBox166.Visible = false;
            // 
            // pictureBox167
            // 
            this.pictureBox167.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox167.BackgroundImage")));
            this.pictureBox167.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox167.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox167.InitialImage")));
            this.pictureBox167.Location = new System.Drawing.Point(194, 503);
            this.pictureBox167.Name = "pictureBox167";
            this.pictureBox167.Size = new System.Drawing.Size(48, 48);
            this.pictureBox167.TabIndex = 215;
            this.pictureBox167.TabStop = false;
            this.pictureBox167.Visible = false;
            // 
            // pictureBox168
            // 
            this.pictureBox168.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox168.BackgroundImage")));
            this.pictureBox168.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox168.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox168.InitialImage")));
            this.pictureBox168.Location = new System.Drawing.Point(248, 503);
            this.pictureBox168.Name = "pictureBox168";
            this.pictureBox168.Size = new System.Drawing.Size(48, 48);
            this.pictureBox168.TabIndex = 214;
            this.pictureBox168.TabStop = false;
            this.pictureBox168.Visible = false;
            // 
            // pictureBox169
            // 
            this.pictureBox169.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox169.BackgroundImage")));
            this.pictureBox169.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox169.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox169.InitialImage")));
            this.pictureBox169.Location = new System.Drawing.Point(302, 503);
            this.pictureBox169.Name = "pictureBox169";
            this.pictureBox169.Size = new System.Drawing.Size(48, 48);
            this.pictureBox169.TabIndex = 207;
            this.pictureBox169.TabStop = false;
            this.pictureBox169.Visible = false;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label176.Location = new System.Drawing.Point(311, 521);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(31, 13);
            this.label176.TabIndex = 224;
            this.label176.Text = "1000";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label177.Location = new System.Drawing.Point(260, 521);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(25, 13);
            this.label177.TabIndex = 223;
            this.label177.Text = "500";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label178.Location = new System.Drawing.Point(206, 521);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(25, 13);
            this.label178.TabIndex = 222;
            this.label178.Text = "400";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label179.Location = new System.Drawing.Point(152, 521);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(25, 13);
            this.label179.TabIndex = 221;
            this.label179.Text = "200";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label180.Location = new System.Drawing.Point(104, 520);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(19, 13);
            this.label180.TabIndex = 220;
            this.label180.Text = "50";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label181.Location = new System.Drawing.Point(50, 521);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 219;
            this.label181.Text = "0";
            // 
            // pictureBox170
            // 
            this.pictureBox170.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox170.BackgroundImage")));
            this.pictureBox170.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox170.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox170.InitialImage")));
            this.pictureBox170.Location = new System.Drawing.Point(32, 503);
            this.pictureBox170.Name = "pictureBox170";
            this.pictureBox170.Size = new System.Drawing.Size(48, 48);
            this.pictureBox170.TabIndex = 213;
            this.pictureBox170.TabStop = false;
            // 
            // pictureBox171
            // 
            this.pictureBox171.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox171.BackgroundImage")));
            this.pictureBox171.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox171.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox171.InitialImage")));
            this.pictureBox171.Location = new System.Drawing.Point(302, 503);
            this.pictureBox171.Name = "pictureBox171";
            this.pictureBox171.Size = new System.Drawing.Size(48, 48);
            this.pictureBox171.TabIndex = 212;
            this.pictureBox171.TabStop = false;
            // 
            // pictureBox172
            // 
            this.pictureBox172.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox172.BackgroundImage")));
            this.pictureBox172.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox172.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox172.InitialImage")));
            this.pictureBox172.Location = new System.Drawing.Point(86, 503);
            this.pictureBox172.Name = "pictureBox172";
            this.pictureBox172.Size = new System.Drawing.Size(48, 48);
            this.pictureBox172.TabIndex = 211;
            this.pictureBox172.TabStop = false;
            // 
            // pictureBox173
            // 
            this.pictureBox173.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox173.BackgroundImage")));
            this.pictureBox173.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox173.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox173.InitialImage")));
            this.pictureBox173.Location = new System.Drawing.Point(140, 503);
            this.pictureBox173.Name = "pictureBox173";
            this.pictureBox173.Size = new System.Drawing.Size(48, 48);
            this.pictureBox173.TabIndex = 210;
            this.pictureBox173.TabStop = false;
            // 
            // pictureBox174
            // 
            this.pictureBox174.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox174.BackgroundImage")));
            this.pictureBox174.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox174.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox174.InitialImage")));
            this.pictureBox174.Location = new System.Drawing.Point(194, 503);
            this.pictureBox174.Name = "pictureBox174";
            this.pictureBox174.Size = new System.Drawing.Size(48, 48);
            this.pictureBox174.TabIndex = 209;
            this.pictureBox174.TabStop = false;
            // 
            // pictureBox175
            // 
            this.pictureBox175.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox175.BackgroundImage")));
            this.pictureBox175.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox175.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox175.InitialImage")));
            this.pictureBox175.Location = new System.Drawing.Point(248, 503);
            this.pictureBox175.Name = "pictureBox175";
            this.pictureBox175.Size = new System.Drawing.Size(48, 48);
            this.pictureBox175.TabIndex = 208;
            this.pictureBox175.TabStop = false;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(293, 487);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(57, 13);
            this.label182.TabIndex = 206;
            this.label182.Text = "Completed";
            this.label182.Visible = false;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.label7);
            this.tabPage8.Controls.Add(this.dataGridView2);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(560, 597);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Killboard";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Last 50 entries";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(3, 49);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(554, 545);
            this.dataGridView2.TabIndex = 0;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.linkLabel5);
            this.tabPage9.Controls.Add(this.label5);
            this.tabPage9.Controls.Add(this.checkBox18);
            this.tabPage9.Controls.Add(this.linkLabel4);
            this.tabPage9.Controls.Add(this.checkBox17);
            this.tabPage9.Controls.Add(this.linkLabel3);
            this.tabPage9.Controls.Add(this.checkBox16);
            this.tabPage9.Controls.Add(this.linkLabel2);
            this.tabPage9.Controls.Add(this.checkBox15);
            this.tabPage9.Controls.Add(this.linkLabel1);
            this.tabPage9.Controls.Add(this.checkBox14);
            this.tabPage9.Controls.Add(this.checkBox9);
            this.tabPage9.Controls.Add(this.checkBox13);
            this.tabPage9.Controls.Add(this.checkBox10);
            this.tabPage9.Controls.Add(this.checkBox12);
            this.tabPage9.Controls.Add(this.checkBox11);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(560, 597);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Links";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Location = new System.Drawing.Point(17, 130);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(55, 13);
            this.linkLabel5.TabIndex = 4;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "linkLabel5";
            this.linkLabel5.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(361, 330);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 130;
            this.label5.Text = "Badges";
            this.label5.Visible = false;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(365, 563);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(127, 17);
            this.checkBox18.TabIndex = 129;
            this.checkBox18.Text = "superb galaxy piloting";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.Visible = false;
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(17, 104);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(121, 13);
            this.linkLabel4.TabIndex = 3;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "planetside-universe.com";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(365, 540);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(98, 17);
            this.checkBox17.TabIndex = 128;
            this.checkBox17.Text = "groundpounder";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.Visible = false;
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(17, 75);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(100, 13);
            this.linkLabel3.TabIndex = 2;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "planetside-intel.com";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(365, 517);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(102, 17);
            this.checkBox16.TabIndex = 127;
            this.checkBox16.Text = "dog fighting ace";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.Visible = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(17, 49);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(120, 13);
            this.linkLabel2.TabIndex = 1;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "players.planetside2.com";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(365, 494);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(168, 17);
            this.checkBox15.TabIndex = 126;
            this.checkBox15.Text = "has lead a platoon victoriously";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.Visible = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(17, 20);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(97, 13);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "stats.dasanfall.com";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(365, 471);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(113, 17);
            this.checkBox14.TabIndex = 125;
            this.checkBox14.Text = "has lead a platoon";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.Visible = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(365, 356);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(160, 17);
            this.checkBox9.TabIndex = 120;
            this.checkBox9.Text = "has completed basic training";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Visible = false;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(365, 448);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(162, 17);
            this.checkBox13.TabIndex = 124;
            this.checkBox13.Text = "has lead a squad victoriously";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.Visible = false;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(365, 379);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(183, 17);
            this.checkBox10.TabIndex = 121;
            this.checkBox10.Text = "has completed advanced training";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.Visible = false;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(365, 425);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(107, 17);
            this.checkBox12.TabIndex = 123;
            this.checkBox12.Text = "has lead a squad";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.Visible = false;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(365, 402);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(184, 17);
            this.checkBox11.TabIndex = 122;
            this.checkBox11.Text = "has completed marauders training";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToClipboardToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(172, 26);
            // 
            // copyToClipboardToolStripMenuItem
            // 
            this.copyToClipboardToolStripMenuItem.Name = "copyToClipboardToolStripMenuItem";
            this.copyToClipboardToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.copyToClipboardToolStripMenuItem.Text = "Copy to Clipboard";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.allTheStatsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(704, 24);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pingToolStripMenuItem,
            this.tracerouteToolStripMenuItem,
            this.teamspeakToolStripMenuItem});
            this.toolsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            this.toolsToolStripMenuItem.Click += new System.EventHandler(this.toolsToolStripMenuItem_Click);
            // 
            // pingToolStripMenuItem
            // 
            this.pingToolStripMenuItem.Name = "pingToolStripMenuItem";
            this.pingToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.pingToolStripMenuItem.Text = "Ping";
            this.pingToolStripMenuItem.Visible = false;
            this.pingToolStripMenuItem.Click += new System.EventHandler(this.pingToolStripMenuItem_Click);
            // 
            // tracerouteToolStripMenuItem
            // 
            this.tracerouteToolStripMenuItem.Name = "tracerouteToolStripMenuItem";
            this.tracerouteToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.tracerouteToolStripMenuItem.Text = "Traceroute";
            this.tracerouteToolStripMenuItem.Visible = false;
            // 
            // teamspeakToolStripMenuItem
            // 
            this.teamspeakToolStripMenuItem.Name = "teamspeakToolStripMenuItem";
            this.teamspeakToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.teamspeakToolStripMenuItem.Text = "Teamspeak";
            this.teamspeakToolStripMenuItem.Visible = false;
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // allTheStatsToolStripMenuItem
            // 
            this.allTheStatsToolStripMenuItem.Enabled = false;
            this.allTheStatsToolStripMenuItem.Name = "allTheStatsToolStripMenuItem";
            this.allTheStatsToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.allTheStatsToolStripMenuItem.Text = "All the stats";
            this.allTheStatsToolStripMenuItem.Visible = false;
            this.allTheStatsToolStripMenuItem.Click += new System.EventHandler(this.allTheStatsToolStripMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(155, 65);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(496, 623);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(150, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(205, 31);
            this.label14.TabIndex = 29;
            this.label14.Text = "Members online";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(706, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(247, 39);
            this.label24.TabIndex = 38;
            this.label24.Text = "Member Name";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(654, 57);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(47, 639);
            this.pictureBox4.TabIndex = 31;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(4, 20);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(136, 20);
            this.textBox12.TabIndex = 102;
            this.textBox12.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(8, 37);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(9, 117);
            this.label57.TabIndex = 110;
            this.label57.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n\'";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(14, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(19, 13);
            this.label58.TabIndex = 111;
            this.label58.Text = "----";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(14, 69);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(19, 13);
            this.label59.TabIndex = 112;
            this.label59.Text = "----";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(14, 92);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(19, 13);
            this.label60.TabIndex = 113;
            this.label60.Text = "----";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(14, 115);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(19, 13);
            this.label61.TabIndex = 114;
            this.label61.Text = "----";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(14, 139);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(19, 13);
            this.label62.TabIndex = 115;
            this.label62.Text = "----";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBox1);
            this.groupBox10.Controls.Add(this.checkBox2);
            this.groupBox10.Controls.Add(this.label62);
            this.groupBox10.Controls.Add(this.checkBox3);
            this.groupBox10.Controls.Add(this.label61);
            this.groupBox10.Controls.Add(this.checkBox4);
            this.groupBox10.Controls.Add(this.label60);
            this.groupBox10.Controls.Add(this.checkBox5);
            this.groupBox10.Controls.Add(this.label59);
            this.groupBox10.Controls.Add(this.checkBox6);
            this.groupBox10.Controls.Add(this.label58);
            this.groupBox10.Controls.Add(this.checkBox7);
            this.groupBox10.Controls.Add(this.label57);
            this.groupBox10.Controls.Add(this.checkBox8);
            this.groupBox10.ForeColor = System.Drawing.Color.White;
            this.groupBox10.Location = new System.Drawing.Point(3, 136);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(146, 231);
            this.groupBox10.TabIndex = 116;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Filter";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBox12);
            this.groupBox11.ForeColor = System.Drawing.Color.White;
            this.groupBox11.Location = new System.Drawing.Point(3, 80);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(146, 50);
            this.groupBox11.TabIndex = 117;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Search";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(23, 10);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(96, 90);
            this.pictureBox6.TabIndex = 118;
            this.pictureBox6.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 702);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(704, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 121;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.Visible = false;
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar1.Maximum = 50;
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 16);
            this.toolStripProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(3, 376);
            this.progressBar2.MarqueeAnimationSpeed = 1;
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(146, 23);
            this.progressBar2.TabIndex = 122;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.Color.White;
            this.label72.Location = new System.Drawing.Point(3, 406);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(41, 13);
            this.label72.TabIndex = 123;
            this.label72.Text = "label72";
            this.label72.Visible = false;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(148, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 125;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label2.Location = new System.Drawing.Point(148, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 126;
            this.label2.Text = "label2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label3.Location = new System.Drawing.Point(148, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 127;
            this.label3.Text = "label3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label28.Location = new System.Drawing.Point(106, 132);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(24, 13);
            this.label28.TabIndex = 130;
            this.label28.Text = "VS:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label29.Location = new System.Drawing.Point(106, 116);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(25, 13);
            this.label29.TabIndex = 129;
            this.label29.Text = "NC:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(106, 101);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(25, 13);
            this.label30.TabIndex = 128;
            this.label30.Text = "TR:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label52.Location = new System.Drawing.Point(124, 78);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(43, 16);
            this.label52.TabIndex = 131;
            this.label52.Text = "Indar";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label184.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label184.Location = new System.Drawing.Point(321, 78);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(64, 16);
            this.label184.TabIndex = 138;
            this.label184.Text = "Amerish";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label185.Location = new System.Drawing.Point(303, 132);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(24, 13);
            this.label185.TabIndex = 137;
            this.label185.Text = "VS:";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label186.Location = new System.Drawing.Point(303, 116);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(25, 13);
            this.label186.TabIndex = 136;
            this.label186.Text = "NC:";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.ForeColor = System.Drawing.Color.Red;
            this.label187.Location = new System.Drawing.Point(303, 101);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(25, 13);
            this.label187.TabIndex = 135;
            this.label187.Text = "TR:";
            // 
            // label188
            // 
            this.label188.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label188.Location = new System.Drawing.Point(345, 132);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(40, 13);
            this.label188.TabIndex = 134;
            this.label188.Text = "label188";
            this.label188.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label189
            // 
            this.label189.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label189.Location = new System.Drawing.Point(345, 116);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(40, 13);
            this.label189.TabIndex = 133;
            this.label189.Text = "label189";
            this.label189.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label190
            // 
            this.label190.ForeColor = System.Drawing.Color.Red;
            this.label190.Location = new System.Drawing.Point(345, 101);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(40, 13);
            this.label190.TabIndex = 132;
            this.label190.Text = "label190";
            this.label190.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label191.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label191.Location = new System.Drawing.Point(124, 178);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(56, 16);
            this.label191.TabIndex = 145;
            this.label191.Text = "Esamir";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label192.Location = new System.Drawing.Point(106, 232);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(24, 13);
            this.label192.TabIndex = 144;
            this.label192.Text = "VS:";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label193.Location = new System.Drawing.Point(106, 216);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(25, 13);
            this.label193.TabIndex = 143;
            this.label193.Text = "NC:";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.ForeColor = System.Drawing.Color.Red;
            this.label194.Location = new System.Drawing.Point(106, 201);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(25, 13);
            this.label194.TabIndex = 142;
            this.label194.Text = "TR:";
            // 
            // label195
            // 
            this.label195.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label195.Location = new System.Drawing.Point(148, 232);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(40, 13);
            this.label195.TabIndex = 141;
            this.label195.Text = "label195";
            this.label195.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label196
            // 
            this.label196.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label196.Location = new System.Drawing.Point(148, 216);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(40, 13);
            this.label196.TabIndex = 140;
            this.label196.Text = "label196";
            this.label196.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label197
            // 
            this.label197.ForeColor = System.Drawing.Color.Red;
            this.label197.Location = new System.Drawing.Point(148, 201);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(40, 13);
            this.label197.TabIndex = 139;
            this.label197.Text = "label197";
            this.label197.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label198.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label198.Location = new System.Drawing.Point(321, 178);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(56, 16);
            this.label198.TabIndex = 152;
            this.label198.Text = "Hossin";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label199.Location = new System.Drawing.Point(303, 232);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(24, 13);
            this.label199.TabIndex = 151;
            this.label199.Text = "VS:";
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label200.Location = new System.Drawing.Point(303, 216);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(25, 13);
            this.label200.TabIndex = 150;
            this.label200.Text = "NC:";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.ForeColor = System.Drawing.Color.Red;
            this.label201.Location = new System.Drawing.Point(303, 201);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(25, 13);
            this.label201.TabIndex = 149;
            this.label201.Text = "TR:";
            // 
            // label202
            // 
            this.label202.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(51)))), ((int)(((byte)(255)))));
            this.label202.Location = new System.Drawing.Point(345, 232);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(40, 13);
            this.label202.TabIndex = 148;
            this.label202.Text = "label202";
            this.label202.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label203
            // 
            this.label203.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(140)))), ((int)(((byte)(186)))));
            this.label203.Location = new System.Drawing.Point(345, 216);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(40, 13);
            this.label203.TabIndex = 147;
            this.label203.Text = "label203";
            this.label203.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label204
            // 
            this.label204.ForeColor = System.Drawing.Color.Red;
            this.label204.Location = new System.Drawing.Point(345, 201);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(40, 13);
            this.label204.TabIndex = 146;
            this.label204.Text = "label204";
            this.label204.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label205.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label205.Location = new System.Drawing.Point(137, 30);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(226, 24);
            this.label205.TabIndex = 153;
            this.label205.Text = "Cobalt Territory Control";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.BackColor = System.Drawing.Color.Transparent;
            this.label206.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label206.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label206.Location = new System.Drawing.Point(95, 109);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(108, 31);
            this.label206.TabIndex = 154;
            this.label206.Text = "Locked";
            this.label206.Visible = false;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.BackColor = System.Drawing.Color.Transparent;
            this.label207.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label207.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label207.Location = new System.Drawing.Point(291, 109);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(108, 31);
            this.label207.TabIndex = 155;
            this.label207.Text = "Locked";
            this.label207.Visible = false;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.BackColor = System.Drawing.Color.Transparent;
            this.label208.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label208.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label208.Location = new System.Drawing.Point(95, 208);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(108, 31);
            this.label208.TabIndex = 156;
            this.label208.Text = "Locked";
            this.label208.Visible = false;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.BackColor = System.Drawing.Color.Transparent;
            this.label209.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label209.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label209.Location = new System.Drawing.Point(291, 208);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(108, 31);
            this.label209.TabIndex = 157;
            this.label209.Text = "Locked";
            this.label209.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label206);
            this.groupBox2.Controls.Add(this.label209);
            this.groupBox2.Controls.Add(this.label208);
            this.groupBox2.Controls.Add(this.label207);
            this.groupBox2.Controls.Add(this.label205);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label52);
            this.groupBox2.Controls.Add(this.label190);
            this.groupBox2.Controls.Add(this.label189);
            this.groupBox2.Controls.Add(this.label188);
            this.groupBox2.Controls.Add(this.label187);
            this.groupBox2.Controls.Add(this.label186);
            this.groupBox2.Controls.Add(this.label185);
            this.groupBox2.Controls.Add(this.label184);
            this.groupBox2.Controls.Add(this.label197);
            this.groupBox2.Controls.Add(this.label196);
            this.groupBox2.Controls.Add(this.label195);
            this.groupBox2.Controls.Add(this.label194);
            this.groupBox2.Controls.Add(this.label193);
            this.groupBox2.Controls.Add(this.label198);
            this.groupBox2.Controls.Add(this.label192);
            this.groupBox2.Controls.Add(this.label199);
            this.groupBox2.Controls.Add(this.label191);
            this.groupBox2.Controls.Add(this.label200);
            this.groupBox2.Controls.Add(this.label204);
            this.groupBox2.Controls.Add(this.label201);
            this.groupBox2.Controls.Add(this.label203);
            this.groupBox2.Controls.Add(this.label202);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(155, 414);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(492, 274);
            this.groupBox2.TabIndex = 158;
            this.groupBox2.TabStop = false;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(513, 562);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(47, 13);
            this.label210.TabIndex = 113;
            this.label210.Text = "label210";
            this.label210.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(704, 710);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.linkLabel6);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Outfit Management Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox176)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox177)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox99)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox79)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox133)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox175)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyToClipboardToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem allTheStatsToolStripMenuItem;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.PictureBox pictureBox4;
        //private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tracerouteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teamspeakToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.PictureBox pictureBox62;
        private System.Windows.Forms.PictureBox pictureBox64;
        private System.Windows.Forms.PictureBox pictureBox65;
        private System.Windows.Forms.PictureBox pictureBox66;
        private System.Windows.Forms.PictureBox pictureBox67;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.PictureBox pictureBox68;
        private System.Windows.Forms.PictureBox pictureBox69;
        private System.Windows.Forms.PictureBox pictureBox70;
        private System.Windows.Forms.PictureBox pictureBox71;
        private System.Windows.Forms.PictureBox pictureBox72;
        private System.Windows.Forms.PictureBox pictureBox73;
        private System.Windows.Forms.PictureBox pictureBox74;
        private System.Windows.Forms.PictureBox pictureBox75;
        private System.Windows.Forms.PictureBox pictureBox76;
        private System.Windows.Forms.PictureBox pictureBox77;
        private System.Windows.Forms.PictureBox pictureBox78;
        private System.Windows.Forms.PictureBox pictureBox79;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.PictureBox pictureBox83;
        private System.Windows.Forms.PictureBox pictureBox84;
        private System.Windows.Forms.PictureBox pictureBox85;
        private System.Windows.Forms.PictureBox pictureBox86;
        private System.Windows.Forms.PictureBox pictureBox87;
        private System.Windows.Forms.PictureBox pictureBox88;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.PictureBox pictureBox94;
        private System.Windows.Forms.PictureBox pictureBox95;
        private System.Windows.Forms.PictureBox pictureBox96;
        private System.Windows.Forms.PictureBox pictureBox97;
        private System.Windows.Forms.PictureBox pictureBox98;
        private System.Windows.Forms.PictureBox pictureBox99;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.PictureBox pictureBox102;
        private System.Windows.Forms.PictureBox pictureBox103;
        private System.Windows.Forms.PictureBox pictureBox104;
        private System.Windows.Forms.PictureBox pictureBox105;
        private System.Windows.Forms.PictureBox pictureBox106;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.PictureBox pictureBox107;
        private System.Windows.Forms.PictureBox pictureBox108;
        private System.Windows.Forms.PictureBox pictureBox109;
        private System.Windows.Forms.PictureBox pictureBox110;
        private System.Windows.Forms.PictureBox pictureBox111;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.PictureBox pictureBox61;
        private System.Windows.Forms.PictureBox pictureBox63;
        private System.Windows.Forms.PictureBox pictureBox80;
        private System.Windows.Forms.PictureBox pictureBox81;
        private System.Windows.Forms.PictureBox pictureBox82;
        private System.Windows.Forms.PictureBox pictureBox89;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.PictureBox pictureBox90;
        private System.Windows.Forms.PictureBox pictureBox91;
        private System.Windows.Forms.PictureBox pictureBox92;
        private System.Windows.Forms.PictureBox pictureBox93;
        private System.Windows.Forms.PictureBox pictureBox100;
        private System.Windows.Forms.PictureBox pictureBox101;
        private System.Windows.Forms.PictureBox pictureBox134;
        private System.Windows.Forms.PictureBox pictureBox135;
        private System.Windows.Forms.PictureBox pictureBox136;
        private System.Windows.Forms.PictureBox pictureBox137;
        private System.Windows.Forms.PictureBox pictureBox138;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.PictureBox pictureBox139;
        private System.Windows.Forms.PictureBox pictureBox140;
        private System.Windows.Forms.PictureBox pictureBox141;
        private System.Windows.Forms.PictureBox pictureBox142;
        private System.Windows.Forms.PictureBox pictureBox143;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.PictureBox pictureBox112;
        private System.Windows.Forms.PictureBox pictureBox113;
        private System.Windows.Forms.PictureBox pictureBox114;
        private System.Windows.Forms.PictureBox pictureBox115;
        private System.Windows.Forms.PictureBox pictureBox116;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.PictureBox pictureBox117;
        private System.Windows.Forms.PictureBox pictureBox118;
        private System.Windows.Forms.PictureBox pictureBox119;
        private System.Windows.Forms.PictureBox pictureBox120;
        private System.Windows.Forms.PictureBox pictureBox121;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.PictureBox pictureBox122;
        private System.Windows.Forms.PictureBox pictureBox123;
        private System.Windows.Forms.PictureBox pictureBox124;
        private System.Windows.Forms.PictureBox pictureBox125;
        private System.Windows.Forms.PictureBox pictureBox126;
        private System.Windows.Forms.PictureBox pictureBox127;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.PictureBox pictureBox128;
        private System.Windows.Forms.PictureBox pictureBox129;
        private System.Windows.Forms.PictureBox pictureBox130;
        private System.Windows.Forms.PictureBox pictureBox131;
        private System.Windows.Forms.PictureBox pictureBox132;
        private System.Windows.Forms.PictureBox pictureBox133;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.PictureBox pictureBox144;
        private System.Windows.Forms.PictureBox pictureBox145;
        private System.Windows.Forms.PictureBox pictureBox146;
        private System.Windows.Forms.PictureBox pictureBox147;
        private System.Windows.Forms.PictureBox pictureBox148;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.PictureBox pictureBox149;
        private System.Windows.Forms.PictureBox pictureBox150;
        private System.Windows.Forms.PictureBox pictureBox151;
        private System.Windows.Forms.PictureBox pictureBox152;
        private System.Windows.Forms.PictureBox pictureBox153;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.PictureBox pictureBox154;
        private System.Windows.Forms.PictureBox pictureBox155;
        private System.Windows.Forms.PictureBox pictureBox156;
        private System.Windows.Forms.PictureBox pictureBox157;
        private System.Windows.Forms.PictureBox pictureBox158;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.PictureBox pictureBox159;
        private System.Windows.Forms.PictureBox pictureBox160;
        private System.Windows.Forms.PictureBox pictureBox161;
        private System.Windows.Forms.PictureBox pictureBox162;
        private System.Windows.Forms.PictureBox pictureBox163;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.PictureBox pictureBox164;
        private System.Windows.Forms.PictureBox pictureBox165;
        private System.Windows.Forms.PictureBox pictureBox166;
        private System.Windows.Forms.PictureBox pictureBox167;
        private System.Windows.Forms.PictureBox pictureBox168;
        private System.Windows.Forms.PictureBox pictureBox169;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.PictureBox pictureBox170;
        private System.Windows.Forms.PictureBox pictureBox171;
        private System.Windows.Forms.PictureBox pictureBox172;
        private System.Windows.Forms.PictureBox pictureBox173;
        private System.Windows.Forms.PictureBox pictureBox174;
        private System.Windows.Forms.PictureBox pictureBox175;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.PictureBox pictureBox176;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox177;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label210;
    }
}

